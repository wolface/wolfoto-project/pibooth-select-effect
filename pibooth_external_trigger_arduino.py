"""Pibooth plugin which enables an external trigger connected to an Arduino board to start capture photo."""

import pyfirmata
import pygame
import serial

import pibooth
from pibooth import evts
from pibooth.utils import LOGGER


EVT_PIBOOTH_NEXT = pygame.USEREVENT + 300


@pibooth.hookimpl
def pibooth_configure(cfg):
    cfg.add_option(
        "CONTROLS", "arduino_port", "/dev/ttyUSB0", "Arduino board connection port"
    )
    cfg.add_option(
        "CONTROLS",
        "arduino_external_trigger_pin",
        2,
        "Physical arduino IN pin to trigger capture",
    )
    cfg.add_option(
        "CONTROLS", "arduino_in_idle_level", True, "Arduino digital input idle level"
    )


@pibooth.hookimpl
def pibooth_startup(cfg, app):
    try:
        arduino_port = cfg.get("CONTROLS", "arduino_port")
        external_trigger_pin = cfg.get("CONTROLS", "arduino_external_trigger_pin")
        app.arduino_in_idle_level = cfg.get("CONTROLS", "arduino_in_idle_level")
        app.arduino_check = True  # flag to validate Arduino board use
    except AttributeError:
        LOGGER.debug(
            "WOLFOTO : You use external trigger Arduino plugin but some parameters are missing in pibooth configuration file"
        )
        LOGGER.info("WOLFOTO : Disabling external trigger use.")
        app.arduino_check = False
    if app.arduino_check:
        try:
            app.arduino_board = pyfirmata.Arduino(arduino_port)
            it = pyfirmata.util.Iterator(app.arduino_board)
            it.start()
            app.arduino_external_trigger = app.arduino_board.get_pin(
                f"d:{external_trigger_pin}:i"
            )
            app.arduino_external_trigger_state = app.arduino_external_trigger.read()
        except serial.serialutil.SerialException:
            LOGGER.debug(
                f"WOLFOTO : Check Arduino board. Could not open port {arduino_port}."
            )
            LOGGER.info("WOLFOTO : Disabling external trigger use.")
            app.arduino_check = False


@pibooth.hookimpl(optionalhook=True)
def state_cameramonitor_do(cfg, app, win, events):
    if app.arduino_check:
        try:
            arduino_external_trigger_new_state = app.arduino_external_trigger.read()
            if arduino_external_trigger_new_state != app.arduino_external_trigger_state:
                if arduino_external_trigger_new_state is not app.arduino_in_idle_level:
                    evts.post(EVT_PIBOOTH_NEXT)
                app.arduino_external_trigger_state = arduino_external_trigger_new_state
        except AttributeError:
            LOGGER.debug(
                "WOLFOTO : You use external trigger arduino plugin but some parameters are missing in pibooth configuration file"
            )
            LOGGER.info("WOLFOTO : Disabling external trigger use.")
            app.arduino_check = False
