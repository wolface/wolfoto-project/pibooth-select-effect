import pyfirmata
import serial
import configparser
import os
import time


CONFIGPATH = os.path.expanduser("~/.config/pibooth/pibooth.cfg")
RELAY_PIN = 7  # pin out to control relay
config = configparser.ConfigParser()

config.read(CONFIGPATH)
try:
    arduino_port = config['CONTROLS']['arduino_port']
except KeyError:
    print("ERROR: No port defined for Arduino board in pibooth configuration file (~/.config/pibooth/pibooth.cfg)")
    exit()

try:
    arduino_board = pyfirmata.Arduino(arduino_port)
except serial.serialutil.SerialException:
    print(f"Port {arduino_port} could not be opened, please check Arduino board connection.")
    exit()

arduino_relay = arduino_board.get_pin(f"d:{RELAY_PIN}:o")
arduino_relay.write(0)

