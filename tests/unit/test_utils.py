import datetime
import sys
import pathlib
import pytest
from pibooth.config.parser import PiboothConfigParser
from PIL import Image, ImageOps
import random
import os
import shutil

path = pathlib.Path(__file__).parent.parent.parent.resolve()
sys.path.append(str(path))
import wolf_utils
from pibooth_wolfstart import create_mandatory_items


@pytest.fixture
def shortcodes():
    """Used shortcodes"""

    shortcodes = ["vrb", "rth", "rbj", "yce", "pbx", "hfb", "rxv", "jfr", "tyr", "fdf"]

    return shortcodes


@pytest.fixture
def filenames():
    """Used filenames"""

    filenames = ["filename_1", "filename_2", "filename_3"]

    return filenames


@pytest.fixture
def cfg():
    """Pibooth config"""

    # create configuration file if not exists
    path = pathlib.Path(pathlib.Path(__file__).parent, "data/pibooth.cfg")
    path.parent.mkdir(parents=True, exist_ok=True)
    path.touch()

    cfg = PiboothConfigParser(filename=path, plugin_manager=None)
    return cfg


def test_get_next_time_alarm():

    delta_time = datetime.timedelta()

    alarm_times = []
    time = datetime.time.fromisoformat("09:00:00")
    date_ref = datetime.datetime.combine(datetime.date.today(), time)
    assert wolf_utils.get_next_time_alarm(alarm_times, date_ref, delta_time) is False

    ################
    alarm_times = [("10:00:00", "11:00:00"), ("17:00:00", "17:30:00")]

    time = datetime.time.fromisoformat("09:00:00")
    date_ref = datetime.datetime.combine(datetime.date.today(), time)
    assert wolf_utils.get_next_time_alarm(alarm_times, date_ref, delta_time) == (
        datetime.time.fromisoformat("10:00:00"),
        datetime.time.fromisoformat("11:00:00"),
    )

    time = datetime.time.fromisoformat("10:00:00")
    date_ref = datetime.datetime.combine(datetime.date.today(), time)
    assert wolf_utils.get_next_time_alarm(alarm_times, date_ref, delta_time) == (
        datetime.time.fromisoformat("17:00:00"),
        datetime.time.fromisoformat("17:30:00"),
    )

    time = datetime.time.fromisoformat("16:00:00")
    date_ref = datetime.datetime.combine(datetime.date.today(), time)
    assert wolf_utils.get_next_time_alarm(alarm_times, date_ref, delta_time) == (
        datetime.time.fromisoformat("17:00:00"),
        datetime.time.fromisoformat("17:30:00"),
    )

    time = datetime.time.fromisoformat("17:02:00")
    date_ref = datetime.datetime.combine(datetime.date.today(), time)
    assert wolf_utils.get_next_time_alarm(alarm_times, date_ref, delta_time) == (
        datetime.time.fromisoformat("10:00:00"),
        datetime.time.fromisoformat("11:00:00"),
    )

    ################
    alarm_times = [
        ("00:00:30", "00:00:40"),
        ("10:00:00", "11:00:00"),
        ("01:00:00", "03:00:00"),
    ]

    time = datetime.time.fromisoformat("00:00:30")
    date_ref = datetime.datetime.combine(datetime.date.today(), time)
    assert wolf_utils.get_next_time_alarm(alarm_times, date_ref, delta_time) == (
        datetime.time.fromisoformat("01:00:00"),
        datetime.time.fromisoformat("03:00:00"),
    )

    time = datetime.time.fromisoformat("09:00:00")
    date_ref = datetime.datetime.combine(datetime.date.today(), time)
    assert wolf_utils.get_next_time_alarm(alarm_times, date_ref, delta_time) == (
        datetime.time.fromisoformat("10:00:00"),
        datetime.time.fromisoformat("11:00:00"),
    )

    time = datetime.time.fromisoformat("09:55:00")
    delta_time = datetime.timedelta(minutes=5)
    date_ref = datetime.datetime.combine(datetime.date.today(), time)
    assert wolf_utils.get_next_time_alarm(alarm_times, date_ref, delta_time) == (
        datetime.time.fromisoformat("00:00:30"),
        datetime.time.fromisoformat("00:00:40"),
    )


def test_generate_shortcode(shortcodes):
    """Test generation of shortcodes"""

    shortcode = wolf_utils.generate_shortcode(3, shortcodes)
    assert shortcode not in shortcodes
    assert len(shortcode) == 3


def test_cp_to_local_diffusion_fail(cfg):
    """Use pibooth config file and test copy to local diffusion folder.
    Tests failure cases
    """

    # no existing source
    # should get exit_code == 3
    source = ""
    assert wolf_utils.cp_to_local_diffusion(source, cfg) == 3

    # create source
    source_path = pathlib.Path(pathlib.Path(__file__).parent, "data/img_test")
    if not os.path.exists(source_path):
        color = (
            random.randrange(0, 255),
            random.randrange(0, 255),
            random.randrange(0, 255),
        )
        img = Image.new("RGB", (800, 600), color)
        img = ImageOps.expand(img, 10, 255)
        img.save(source_path, format="JPEG")
    # no "DIFFUSION_KIOSK" in config file
    assert wolf_utils.cp_to_local_diffusion(source_path, cfg) == 4

    # set "DIFFUSION_KIOSK" option and only one parameter
    cfg.set("DIFFUSION_KIOSK", "diffusion_user", "wolfoto")
    # should get exit_code == 4
    assert wolf_utils.cp_to_local_diffusion(source_path, cfg) == 4

    # set all parameters for "DIFFUSION_KIOSK" option but one parameter is empty
    cfg.set("DIFFUSION_KIOSK", "diffusion_server", "")
    cfg.set(
        "DIFFUSION_KIOSK",
        "diffusion_local_folder_path",
        pathlib.Path(pathlib.Path(__file__).parent, "data"),
    )
    # should get exit_code == 4
    assert wolf_utils.cp_to_local_diffusion(source_path, cfg) == 2

    # No obvious tests for exit_code == 1 or 0 using 'scp' command


def test_generate_filename(filenames):
    """Test generation of filename"""

    filename = wolf_utils.generate_filename(filenames)
    assert len(filename) > 0
    assert filename not in filenames


def test_mandatory_items():
    """Test creation of mandatory files and folders"""

    test_dir = pathlib.Path(pathlib.Path(__file__).parent, "data/mandatory")
    create_mandatory_items(test_dir)

    for item in [
        "data/post_waiting_list.yaml",
        "data/shortcodes.yaml",
        "data/filenames.yaml",
        "data/web/",
        "assets/",
        "assets/background.png",
        "assets/logo_place.png",
    ]:
        assert pathlib.Path(test_dir, item).exists()

    # clean
    # remove 'mandatory' test directory
    shutil.rmtree(test_dir)


def test_mandatory_items_but_already_exist():
    """Test creation of mandatory files and folders when they already exist.
    Test that they are not overwritten
    """

    # create folders and files
    test_dir = pathlib.Path(pathlib.Path(__file__).parent, "data/mandatory")
    test_dir.mkdir()
    for d in ["data", "assets", "data/web"]:
        pathlib.Path(test_dir, d).mkdir()
    for f in [
        "data/post_waiting_list.yaml",
        "data/shortcodes.yaml",
        "data/filenames.yaml",
        "assets/background.png",
        "assets/logo_place.png",
    ]:
        pathlib.Path(test_dir, f).touch()
        pathlib.Path(test_dir, f).write_text("Text file contents")

    # create file in 'data/web' folder
    pathlib.Path(test_dir, "data/web/test.txt").touch()
    pathlib.Path(test_dir, "data/web/test.txt").write_text("Text file contents")

    create_mandatory_items(test_dir)

    # check files and folders
    for f in [
        "data/post_waiting_list.yaml",
        "data/shortcodes.yaml",
        "data/filenames.yaml",
        "data/web/test.txt",
        "assets/background.png",
        "assets/logo_place.png",
    ]:
        assert pathlib.Path(test_dir, f).stat().st_size == 18

    # clean
    # remove 'mandatory' test directory
    shutil.rmtree(test_dir)
