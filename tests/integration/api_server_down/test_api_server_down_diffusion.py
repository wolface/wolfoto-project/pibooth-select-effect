import os
import requests
import urllib
import psutil
import pytest
import time
import configparser
import pathlib
import sys

path = pathlib.Path(__file__).parents[3]
sys.path.append(str(path))
import server

DATA_DIR = os.path.expanduser("~/Documents/Pibooth/data/")
IMAGES_DIR = os.path.join(DATA_DIR, "diffusion/images/")
config = configparser.ConfigParser()
config.read(os.path.join(os.environ["HOME"], ".config/pibooth/pibooth.cfg"))
WEB_SERVER_URL = config["SERVER"]["web_server_url"]
SERVER_USERNAME = config["SERVER"]["server_username"]
SERVER_PASSWORD = config["SERVER"]["server_password"]


def test_get_request_with_server_down(wolfoto_web):

    time.sleep(5)  # workaround to get test passed all the time
    success, token_access, token_refresh = server.get_web_token(
        DATA_DIR, WEB_SERVER_URL, SERVER_USERNAME, SERVER_PASSWORD
    )
    assert success is True
    assert token_access is not None

    # kill the server
    for p in psutil.process_iter():
        if (
            p.status() != "zombie"
            and "python" in p.name()
            and "runserver" in p.cmdline()
        ):
            print(f"Killing pid {p.pid} -> {p.cmdline()}")
            p.kill()

    # should raise a ConnectionError exception
    headers = {"Authorization": f"Bearer {token_access}"}
    url_request = urllib.parse.urljoin(WEB_SERVER_URL, "api/gallery")
    with pytest.raises(requests.exceptions.ConnectionError):
        requests.get(url=url_request, headers=headers)
