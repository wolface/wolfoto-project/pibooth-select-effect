import pytest
import socket
from xprocess import ProcessStarter
import psutil
from pathlib import Path
from dotenv import load_dotenv
import os

load_dotenv(dotenv_path=os.path.join(Path(__file__).parent.parent.parent, ".env"))


@pytest.fixture()
def wolfoto_web(xprocess):
    class Starter(ProcessStarter):

        def startup_check(self):
            sock = socket.socket()
            try:
                sock.connect(
                    (
                        os.getenv("WEB_SERVER_URL_IP"),
                        int(os.getenv("WEB_SERVER_URL_PORT")),
                    )
                )
                return True
            except ConnectionRefusedError:
                return False

        # command to start process
        args = [
            "python",
            f"{os.getenv('WOLFOTO')}tests/integration/api/run_wolfoto_web.py",
        ]

    # ensure process is running
    xprocess.ensure("wolfoto_web", Starter)

    yield

    # clean up whole process tree afterwards
    xprocess.getinfo("wolfoto_web").terminate()

    # finish killing wolfoto_web server
    print("\n\nKilling wolfoto_web server")
    for p in psutil.process_iter():
        if (
            p.status() != "zombie"
            and "python" in p.name()
            and "runserver" in p.cmdline()
        ):
            print(f"Killing pid {p.pid} -> {p.cmdline()}")
            p.kill()
