"""Tests while server is not running"""

import pytest
import os
import configparser
import pathlib
import sys
import time
import psutil
from dotenv import load_dotenv

path = pathlib.Path(__file__).parents[3]
sys.path.append(str(path))

import server
import sync_diffusion

load_dotenv(
    dotenv_path=os.path.join(pathlib.Path(__file__).parent.parent.parent, ".env")
)
DATA_DIR = os.path.join(os.path.dirname(__file__), "data")

IMAGES_DIR = os.path.join(
    DATA_DIR, "diffusion/images/"
)  # folder where to store photos for diffusion
LOCAL_DIR = os.path.join(
    DATA_DIR, "diffusion/local/"
)  # folder where to store photos used for diffusion in case server is down

# test folders exist
if not os.path.isdir(IMAGES_DIR):
    os.makedirs(IMAGES_DIR)
if not os.path.isdir(LOCAL_DIR):
    os.makedirs(LOCAL_DIR)


@pytest.fixture
def credentials():
    """Get credentials from pibooth config file"""

    config = configparser.ConfigParser()
    config.read(os.path.join(os.environ["HOME"], ".config/pibooth/pibooth.cfg"))

    web_server_url = config["SERVER"]["web_server_url"]
    server_username = config["SERVER"]["server_username"]
    server_password = config["SERVER"]["server_password"]
    dic = {
        "web_server_url": web_server_url,
        "server_username": server_username,
        "server_password": server_password,
    }
    return dic


@pytest.fixture
def kill_wolfoto_web_server():
    """Be sure that wolfoto_web server is down"""

    # finish killing wolfoto_web server
    print("\n\nKilling wolfoto_web server")
    for p in psutil.process_iter():
        if (
            p.status() != "zombie"
            and "python" in p.name()
            and "runserver" in p.cmdline()
        ):
            print(f"Killing pid {p.pid} -> {p.cmdline()}")
            p.kill()


def test_send_request_post(credentials, kill_wolfoto_web_server):
    """Test basic request without headers"""

    # wait that server is down
    reachable = True
    url = credentials["web_server_url"]
    while reachable:
        reachable, _ = server.send_request_post(url)
        time.sleep(0.2)

    # good url
    url = credentials["web_server_url"]
    reachable, _ = server.send_request_post(url)
    assert reachable is False


def test_send_api_request_gallery(credentials, kill_wolfoto_web_server):

    # wait that server is down
    reachable = True
    url = credentials["web_server_url"]
    while reachable:
        reachable, _ = server.send_request_post(url)
        time.sleep(0.2)

    # test request response
    url_api = "api/gallery/"
    root_dir = os.path.join(os.path.dirname(__file__), "data")
    data = server.send_api_request(
        root_dir,
        credentials["web_server_url"],
        url_api,
        credentials["server_username"],
        credentials["server_password"],
    )

    # should get None
    assert data is None


def test_sync_diffusion_photo_in_local_folder_server_down(credentials):
    """Test sync_diffusion function.
    Photos in local folder (folder where are stored photos that couldn't be sent to web server).
    Server is down so no photos could be downloaded from server and photos in local folder must be
    must be copied from local folder to image dir.
    """

    # create test photos in local dir
    for i in range(5):
        file_name = f"test_photo_{i}.jpg"
        file_path = os.path.join(LOCAL_DIR, file_name)
        with open(file_path, "wb") as f:
            f.write(b"test_image_data")

    # should get 10 photos in 'LOCAL_DIR'
    list_local_dir = os.listdir(LOCAL_DIR)
    assert len(list_local_dir) == 5

    # should get no photo in 'IMAGES_DIR'
    list_dir = os.listdir(IMAGES_DIR)
    # be sure folder is empty
    # remove test files from 'IMAGES_DIR'
    for file in list_dir:
        os.remove(os.path.join(IMAGES_DIR, file))
    list_dir = os.listdir(IMAGES_DIR)
    assert len(list_dir) == 0
    gallery = "test_gallery_0"
    sync_diffusion.sync_diffusion(
        DATA_DIR,
        gallery,
        credentials["web_server_url"],
        credentials["server_username"],
        credentials["server_password"],
    )

    # should get 5 photos in 'IMAGES_DIR'
    list_dir = os.listdir(IMAGES_DIR)
    assert len(list_dir) == 5

    # should get all photos in 'LOCAL_DIR' in 'IMAGES_DIR'
    assert set(list_dir) == set(list_local_dir)

    # should get no photos in local dir
    list_local_dir = os.listdir(LOCAL_DIR)
    assert len(list_local_dir) == 0

    # CLEAN
    # remove test files from 'IMAGES_DIR'
    for file in list_dir:
        os.remove(os.path.join(IMAGES_DIR, file))
