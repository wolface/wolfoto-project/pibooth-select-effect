"""We first need to run wolfoto_web server :
'python manage.py runserver localhost:8000'.
This is done with fixture in conftest.py.
"""

import os
import requests
import urllib
import re
import configparser
import pathlib
import sys

path = pathlib.Path(__file__).parents[3]
sys.path.append(str(path))
import server
import sync_diffusion as sd


DATA_DIR = os.path.expanduser("~/Documents/Pibooth/data/")
IMAGES_DIR = os.path.join(DATA_DIR, "diffusion/images/")
config = configparser.ConfigParser()
config.read(os.path.join(os.environ["HOME"], ".config/pibooth/pibooth.cfg"))
WEB_SERVER_URL = config["SERVER"]["web_server_url"]
SERVER_USERNAME = config["SERVER"]["server_username"]
SERVER_PASSWORD = config["SERVER"]["server_password"]


def test_get_web_token():

    success, token_access, token_refresh = server.get_web_token(
        DATA_DIR, WEB_SERVER_URL, SERVER_USERNAME, SERVER_PASSWORD
    )
    assert success is True
    assert token_access is not None
    assert token_refresh is not None


def test_get_web_token_bad_parameters():

    success, token_access, token_refresh = server.get_web_token(
        DATA_DIR, WEB_SERVER_URL, "bad_username", SERVER_PASSWORD
    )
    assert success is False
    assert token_access is None
    assert token_refresh is None

    success, token_access, token_refresh = server.get_web_token(
        DATA_DIR, WEB_SERVER_URL, SERVER_USERNAME, "bad_password"
    )
    assert success is False
    assert token_access is None
    assert token_refresh is None

    success, token_access, token_refresh = server.get_web_token(
        DATA_DIR, "http://local:8000/", SERVER_USERNAME, SERVER_PASSWORD
    )
    assert success is False
    assert token_access is None
    assert token_refresh is None


def test_refresh_token():

    # get access and refresh tokens
    success, token_access, token_refresh = server.get_web_token(
        DATA_DIR, WEB_SERVER_URL, SERVER_USERNAME, SERVER_PASSWORD
    )
    assert success is True

    # get access token using refresh token
    success = server.refresh_token(DATA_DIR, WEB_SERVER_URL)
    assert success is True


def test_get_photos_to_display_list():
    """Get diffusion list from web server for 'test_gallery_0'"""

    data = sd.get_photos_to_display(
        DATA_DIR, WEB_SERVER_URL, SERVER_USERNAME, SERVER_PASSWORD, "test_gallery_0", 10
    )
    assert data.json() is not None
    # script 'run_wolfoto_web.py' creates 10 local photos
    assert len(data.json()) == 10


def test_send_api_request_gallery():
    """Test send_api_request() for gallery request"""

    data = server.send_api_request(
        DATA_DIR, WEB_SERVER_URL, "api/gallery", SERVER_USERNAME, SERVER_PASSWORD
    )
    assert data.json() is not None
    assert "Test_gallery_0" in [gallery["name"] for gallery in data.json()]


def test_send_api_request_photo():
    """Test send_api_request() for photo request"""

    data = server.send_api_request(
        DATA_DIR, WEB_SERVER_URL, "api/photo", SERVER_USERNAME, SERVER_PASSWORD
    )
    assert data.json() is not None
    test_items = [
        item for item in data.json() if re.match("^Test_gallery_", item["gallery"])
    ]
    assert len(test_items) > 0


def test_get_updates_diffusion_folder():

    old = ["a", "b", "c"]
    new = ["a", "e", "f"]
    items_to_remove, items_to_download = sd.get_updates_diffusion_folder(new, old)
    for c in ["b", "c"]:
        assert c in items_to_remove
    for c in ["e", "f"]:
        assert c in items_to_download

    old = ["a", "b", "c"]
    new = ["a", "b", "c"]
    items_to_remove, items_to_download = sd.get_updates_diffusion_folder(new, old)
    assert len(items_to_remove) == 0
    assert len(items_to_download) == 0

    old = []
    new = ["a", "e", "f"]
    items_to_remove, items_to_download = sd.get_updates_diffusion_folder(new, old)
    for c in ["a", "e", "f"]:
        assert c in items_to_download
    assert len(items_to_remove) == 0

    old = ["a", "b", "c"]
    new = []
    items_to_remove, items_to_download = sd.get_updates_diffusion_folder(new, old)
    for c in ["a", "b", "c"]:
        assert c in items_to_remove
    assert len(items_to_download) == 0


def test_photos_list_from_server():
    """Download photo to be displayed list from web server"""

    # get 10 photos from "test_gallery_0"
    data = sd.get_photos_to_display(
        DATA_DIR, WEB_SERVER_URL, SERVER_USERNAME, SERVER_PASSWORD, "test_gallery_0", 10
    )
    assert len(data.json()) == 10
    # all photos should be marked as 'local'
    for d in data.json():
        assert d["local"] is True


def test_photo_download_and_save():
    """Download photo from web server and save it locally"""

    # get 10 photos from "test_gallery_0"
    data = sd.get_photos_to_display(
        DATA_DIR, WEB_SERVER_URL, SERVER_USERNAME, SERVER_PASSWORD, "test_gallery_0", 10
    )

    # download first photo
    filename = data.json()[0]["filename"]
    # create a test directory to download the photo if it not exists
    test_dir = os.path.join(os.path.dirname(__file__), "test_data/")
    if not os.path.exists(test_dir):
        os.makedirs(test_dir)

    # initial number of items in 'test_dir'
    nb_items = len(os.listdir(test_dir))
    sd.photo_download(
        DATA_DIR, WEB_SERVER_URL, SERVER_USERNAME, SERVER_PASSWORD, test_dir, filename
    )
    # should get one more file
    assert len(os.listdir(test_dir)) == nb_items + 1

    # remove file created for test
    os.remove(os.path.join(test_dir, filename))


def test_get_request_with_bad_access_token():

    headers = {"Authorization": "Bearer bad_token"}
    url_request = urllib.parse.urljoin(WEB_SERVER_URL, "api/gallery")
    response = requests.get(url=url_request, headers=headers)
    assert response.status_code == 401
    assert response.json()["code"] == "token_not_valid"
