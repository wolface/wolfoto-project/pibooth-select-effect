import psutil


# kill the server
for p in psutil.process_iter():
    if p.status() != "zombie" and "python" in p.name() and "runserver" in p.cmdline():
        print(f"Killing pid {p.pid} -> {p.cmdline()}")
        p.kill()
