import subprocess
import os
from pathlib import Path
from dotenv import load_dotenv


load_dotenv(dotenv_path=os.path.join(Path(__file__).parent.parent, ".env"))

subprocess.run(
    [
        "python",
        f"{os.getenv('WOLFOTO')}/tests/api/kill_wolfoto_web.py",
    ],
)

subprocess.Popen(
    [
        os.getenv("WOLFOTO_WEB_PYTHON"),
        f"{os.getenv('WOLFOTO_WEB')}/manage.py",
        "runserver",
        f'{os.getenv("WEB_SERVER_URL_IP")}:{os.getenv("WEB_SERVER_URL_PORT")}',
    ],
)

# generate 10 test photos
subprocess.Popen(
    [
        os.getenv("WOLFOTO_WEB_PYTHON"),
        f"{os.getenv('WOLFOTO_WEB')}/manage.py",
        "generate_test_photos",
        "-p",
        "10",
    ],
)
