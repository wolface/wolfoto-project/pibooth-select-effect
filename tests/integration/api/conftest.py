import pytest
import socket
from xprocess import ProcessStarter
import psutil
from pathlib import Path
from dotenv import load_dotenv
import os
import subprocess

load_dotenv(dotenv_path=os.path.join(Path(__file__).parent.parent.parent, ".env"))


@pytest.hookimpl()
def pytest_sessionstart(session):
    """Action to do before tests"""

    print("START SESSION")
    # kill wolfoto_web server
    print("\n\nKilling wolfoto_web server")
    for p in psutil.process_iter():
        if (
            p.status() != "zombie"
            and "python" in p.name()
            and "runserver" in p.cmdline()
        ):
            print(f"Killing pid {p.pid} -> {p.cmdline()}")
            p.kill()


@pytest.hookimpl()
def pytest_sessionfinish(session):
    """Action to do after tests"""

    print("\nTest session finished!")
    # delete test groups and test users
    subprocess.run(
        [
            f"{os.getenv('WOLFOTO_WEB_PYTHON')}",
            f"{os.getenv('WOLFOTO_WEB')}manage.py",
            "delete_test_groups_users",
        ],
    )


@pytest.fixture(scope="session", autouse=True)
def wolfoto_web(xprocess):
    class Starter(ProcessStarter):

        def startup_check(self):
            sock = socket.socket()
            try:
                sock.connect(
                    (
                        os.getenv("WEB_SERVER_URL_IP"),
                        int(os.getenv("WEB_SERVER_URL_PORT")),
                    )
                )
                return True
            except ConnectionRefusedError:
                return False

        # command to start process
        args = [
            "python",
            f"{os.getenv('WOLFOTO')}tests/integration/api/run_wolfoto_web.py",
        ]

    # ensure process is running
    xprocess.ensure("wolfoto_web", Starter)

    yield

    # clean up whole process tree afterwards
    xprocess.getinfo("wolfoto_web").terminate()

    # finish killing wolfoto_web server
    print("\n\nKilling wolfoto_web server")
    for p in psutil.process_iter():
        if (
            p.status() != "zombie"
            and "python" in p.name()
            and "runserver" in p.cmdline()
        ):
            print(f"Killing pid {p.pid} -> {p.cmdline()}")
            p.kill()
