import os
import pytest
import configparser
import pathlib
from dotenv import load_dotenv
import time
from PIL import Image, ImageOps
import random
import yaml
import sys

path = pathlib.Path(__file__).parents[3]
sys.path.append(str(path))

import server
import sync_diffusion


load_dotenv(
    dotenv_path=os.path.join(pathlib.Path(__file__).parent.parent.parent, ".env")
)
DATA_DIR = os.path.join(os.path.dirname(__file__), "data")

IMAGES_DIR = os.path.join(
    DATA_DIR, "diffusion/images/"
)  # folder where to store photos for diffusion
LOCAL_DIR = os.path.join(
    DATA_DIR, "diffusion/local/"
)  # folder where to store photos used for diffusion in case server is down

# test folders exist
if not os.path.isdir(IMAGES_DIR):
    os.makedirs(IMAGES_DIR)
if not os.path.isdir(LOCAL_DIR):
    os.makedirs(LOCAL_DIR)


@pytest.fixture(scope="session", autouse=True)
def working_directories():
    """Create working directories"""

    os.makedirs(DATA_DIR, exist_ok=True)
    os.makedirs(os.path.join(DATA_DIR, "web"), exist_ok=True)


@pytest.fixture
def credentials():
    """Get credentials from pibooth config file"""

    config = configparser.ConfigParser()
    config.read(os.path.join(os.environ["HOME"], ".config/pibooth/pibooth.cfg"))

    web_server_url = config["SERVER"]["web_server_url"]
    server_username = config["SERVER"]["server_username"]
    server_password = config["SERVER"]["server_password"]
    dic = {
        "web_server_url": web_server_url,
        "server_username": server_username,
        "server_password": server_password,
    }
    return dic


@pytest.fixture
def photo():
    """Create photo and attributes"""

    file_path = os.path.join(DATA_DIR, "img_test")
    if not os.path.exists(file_path):
        color = (
            random.randrange(0, 255),
            random.randrange(0, 255),
            random.randrange(0, 255),
        )
        img = Image.new("RGB", (800, 600), color)
        img = ImageOps.expand(img, 10, 255)

        img.save(file_path, format="JPEG")

    # create dictionnary with photo attributes
    dic = {}
    dic["filename"] = "img_test"
    dic["gallery"] = "test_gallery_0"
    dic["place"] = "Test_place_0"
    dic["datetime"] = "2020-01-01T00:00:00"
    dic["local"] = "1"
    dic["web"] = "1"
    dic["public"] = "0"
    dic["code"] = "Test_code_0"

    return dic


def delete_photo(credentials, filename):
    """Delete photo from web server"""

    url_api = f"api/photo/{filename}/delete_by_filename"
    data = server.send_api_request(
        DATA_DIR,
        credentials["web_server_url"],
        url_api,
        credentials["server_username"],
        credentials["server_password"],
    )
    if data.status_code == 200:
        print(f"Photo <{filename}> has been deleted from server")
    else:
        print(f"Error deleting photo <{filename}> from server: {data.status_code}")


@pytest.fixture
def post_waiting_list():
    """Create post waiting list"""

    # create file to store list of photos to send to server
    file_path = os.path.join(DATA_DIR, "post_waiting_list.yaml")
    if not os.path.exists(file_path):
        with open(file_path, "w") as f:
            f.write("")

    data = {
        "filename": "img_test",
        "gallery": "test_gallery_0",
        "place": "Test_place_0",
        "datetime": "2020-01-01T00:00:00",
        "local": "1",
        "web": "1",
        "public": "0",
        "code": "Test_code_0",
    }

    with open(os.path.join(DATA_DIR, "post_waiting_list.yaml"), "w") as f:
        yaml.dump([data], f)

    # create photo in 'web' folder
    file_path = os.path.join(DATA_DIR, "web/img_test")
    if not os.path.exists(file_path):
        color = (
            random.randrange(0, 255),
            random.randrange(0, 255),
            random.randrange(0, 255),
        )
        img = Image.new("RGB", (800, 600), color)
        img = ImageOps.expand(img, 10, 255)

        img.save(file_path, format="JPEG")


def test_send_api_request_gallery_photo(credentials):
    """Test api gallery request to get list of galleries
    and api photo request to get list of all photos"""

    # wait that server is up
    reachable = False
    url = credentials["web_server_url"]
    while not reachable:
        reachable, _ = server.send_request_post(url)
        time.sleep(0.2)

    # test to get galleries list
    url_api = "api/gallery/"
    data = server.send_api_request(
        DATA_DIR,
        credentials["web_server_url"],
        url_api,
        credentials["server_username"],
        credentials["server_password"],
    )

    # should get 2 galleries: 'wolfoto_gallery' and 'Test_gallery_0'
    assert len(data.json()) == 2
    # should get a gallery named Test_gallery_0
    assert "Test_gallery_0" in [gal["name"] for gal in data.json()]

    # test to get photos list
    url_api = "api/photo/"
    data = server.send_api_request(
        DATA_DIR,
        credentials["web_server_url"],
        url_api,
        credentials["server_username"],
        credentials["server_password"],
    )

    assert data is not None


def test_send_api_request_photo(credentials):
    """Test data response detail"""

    # wait that server is up
    reachable = False
    url = credentials["web_server_url"]
    while not reachable:
        reachable, _ = server.send_request_post(url)
        time.sleep(0.2)

    # test to get photos list
    url_api = "api/photo/"
    data = server.send_api_request(
        DATA_DIR,
        credentials["web_server_url"],
        url_api,
        credentials["server_username"],
        credentials["server_password"],
    )

    data_gallery_test = [
        item for item in data.json() if item["gallery"] == "Test_gallery_0"
    ]
    # should get 10 items as created in run_wolfoto_web.py script
    assert len(data_gallery_test) == 10


def test_send_request_post(credentials):
    """Test basic request without headers"""

    # good url
    url = credentials["web_server_url"]
    reachable, _ = server.send_request_post(url)
    assert reachable is True

    # bad url
    reachable, _ = server.send_request_post("http://localhost:10000")
    assert reachable is False


def test_post_photo(credentials, photo):
    """Send photo to server and test if there is
    a new item in database
    """

    # wait that server is up
    reachable = False
    url = credentials["web_server_url"]
    while not reachable:
        reachable, _ = server.send_request_post(url)
        time.sleep(0.2)

    # post photo to server
    file_path = os.path.join(DATA_DIR, "img_test")
    with open(file_path, "rb") as f:
        byte_img = f.read()

    success = server.post_photo(
        DATA_DIR,
        credentials["web_server_url"],
        credentials["server_username"],
        credentials["server_password"],
        photo,
        byte_img,
    )

    assert success is True

    # get photo list
    url_api = "api/photo/"
    data = server.send_api_request(
        DATA_DIR,
        credentials["web_server_url"],
        url_api,
        credentials["server_username"],
        credentials["server_password"],
    )
    data_gallery_test = [
        item for item in data.json() if item["gallery"] == "Test_gallery_0"
    ]
    # should get 11 items. 10 photos created in run_wolfoto_web.py script + 1 photo sent to server
    assert len(data_gallery_test) == 11

    # get details of photo
    photo = [item for item in data_gallery_test if item["code"] == "Test_code_0"][0]
    assert photo["filename"] == "img_test"

    # delete test photo from server
    delete_photo(credentials, photo["filename"])


def test_post_waiting_photos(credentials, post_waiting_list):
    """Send a photo from the list of photos waiting to be sent to server"""

    # wait that server is up
    reachable = False
    url = credentials["web_server_url"]
    while not reachable:
        reachable, _ = server.send_request_post(url)
        time.sleep(0.2)

    # get initial number of photos in 'Test_gallery_0'
    # get photo list
    url_api = "api/photo/"
    data = server.send_api_request(
        DATA_DIR,
        credentials["web_server_url"],
        url_api,
        credentials["server_username"],
        credentials["server_password"],
    )
    data_gallery_test = [
        item for item in data.json() if item["gallery"] == "Test_gallery_0"
    ]
    initial_nb = len(data_gallery_test)

    # check that post waiting file is not empty
    post_waiting_file = os.path.join(DATA_DIR, "post_waiting_list.yaml")
    assert os.path.getsize(post_waiting_file) != 0
    # send waiting photo
    server.post_waiting_photos(
        DATA_DIR,
        credentials["web_server_url"],
        credentials["server_username"],
        credentials["server_password"],
    )

    # get photo list
    url_api = "api/photo/"
    data = server.send_api_request(
        DATA_DIR,
        credentials["web_server_url"],
        url_api,
        credentials["server_username"],
        credentials["server_password"],
    )
    data_gallery_test = [
        item for item in data.json() if item["gallery"] == "Test_gallery_0"
    ]
    # should get one more item
    assert len(data_gallery_test) == initial_nb + 1

    # get details of photo to be sure that the photo has well been uploaded
    photo = [item for item in data_gallery_test if item["code"] == "Test_code_0"][0]
    assert photo["filename"] == "img_test"

    # if photo has been sent to server, waiting list is empty and photo file is removed
    # test that list of waiting photo is empty
    assert os.path.getsize(post_waiting_file) == 0
    # test that photo in 'web' folder is removed
    file_path = os.path.join(DATA_DIR, "web/img_test")
    assert not os.path.exists(file_path)


def test_get_photos_data_from_server(credentials):
    """Test get shortcodes list from server"""

    # wait that server is up
    reachable = False
    url = credentials["web_server_url"]
    while not reachable:
        reachable, _ = server.send_request_post(url)
        time.sleep(0.2)

    # test to get shortcodes and filenames lists
    shortcodes, filenames = server.get_photos_data_from_server(
        DATA_DIR,
        credentials["web_server_url"],
        credentials["server_username"],
        credentials["server_password"],
    )

    assert shortcodes is not None
    for item in shortcodes:
        assert type(item) is str

    assert filenames is not None
    for item in filenames:
        assert type(item) is str


def test_publish_web_good_parameters(credentials, photo):
    """Test server.publish_web"""

    # wait that server is up
    reachable = False
    url = credentials["web_server_url"]
    while not reachable:
        reachable, _ = server.send_request_post(url)
        time.sleep(0.2)

    # get initial number of photos in 'Test_gallery_0'
    # get photo list
    url_api = "api/photo/"
    data = server.send_api_request(
        DATA_DIR,
        credentials["web_server_url"],
        url_api,
        credentials["server_username"],
        credentials["server_password"],
    )
    data_gallery_test = [
        item for item in data.json() if item["gallery"] == "Test_gallery_0"
    ]
    initial_nb = len(data_gallery_test)

    # get photo's data
    data = photo
    # get image
    file_path = os.path.join(DATA_DIR, "img_test")
    with open(file_path, "rb") as f:
        byte_img = f.read()
    # send photo
    success = server.publish_web(
        DATA_DIR,
        credentials["web_server_url"],
        credentials["server_username"],
        credentials["server_password"],
        data,
        byte_img,
    )

    assert success is True
    # test if the photo has been received by server
    # get photo list
    url_api = "api/photo/"
    data = server.send_api_request(
        DATA_DIR,
        credentials["web_server_url"],
        url_api,
        credentials["server_username"],
        credentials["server_password"],
    )
    data_gallery_test = [
        item for item in data.json() if item["gallery"] == "Test_gallery_0"
    ]
    # should get 11 items. 10 photos created in run_wolfoto_web.py script + 1 photo sent to server
    assert len(data_gallery_test) == initial_nb + 1
    # get details of photo to be sure that the photo has well been uploaded
    photo = [item for item in data_gallery_test if item["code"] == "Test_code_0"][0]
    assert photo["filename"] == "img_test"

    # delete test photo from server
    delete_photo(credentials, photo["filename"])


def test_publish_web_bad_url(credentials, photo):
    """Should return success False when publishing photo,
    copy photo file in 'web' folder and add photo's data in
    'post_waiting_list.yaml' file
    """

    # remove test file from 'web' folder
    try:
        os.remove(os.path.join(DATA_DIR, "web"))
    except OSError:
        pass
    # clear post_waiting_list.yaml file
    post_waiting_file = os.path.join(DATA_DIR, "post_waiting_list.yaml")
    open(post_waiting_file, "w").close()

    # get photo's data
    data = photo
    # get image
    file_path = os.path.join(DATA_DIR, "img_test")
    with open(file_path, "rb") as f:
        byte_img = f.read()
    # send photo
    success = server.publish_web(
        DATA_DIR,
        "http://localhost/bad_url",
        credentials["server_username"],
        credentials["server_password"],
        data,
        byte_img,
    )

    assert success is False
    # photo's data should have been written in post_waiting_list.yaml file
    assert os.path.getsize(post_waiting_file) != 0
    # test data written in post_waiting_list.yaml file
    with open(os.path.join(DATA_DIR, "post_waiting_list.yaml"), "r") as f:
        dic = yaml.safe_load(f)
    for k in dic[0].keys():
        assert dic[0][k] == data[k]
    # photo file should exists
    assert os.path.isfile(os.path.join(DATA_DIR, "web", "img_test"))

    # clean test
    # remove test file from 'web' folder
    try:
        os.remove(os.path.join(DATA_DIR, "web"))
    except OSError:
        pass
    # clear post_waiting_list.yaml file
    post_waiting_file = os.path.join(DATA_DIR, "post_waiting_list.yaml")
    open(post_waiting_file, "w").close()


def test_publish_web_bad_gallery(credentials, photo):
    """Should get no success"""

    # remove test file from 'web' folder
    try:
        os.remove(os.path.join(DATA_DIR, "web"))
    except OSError:
        pass
    # clear post_waiting_list.yaml file
    post_waiting_file = os.path.join(DATA_DIR, "post_waiting_list.yaml")
    open(post_waiting_file, "w").close()

    # get photo's data
    data = photo
    # change gallery
    data["gallery"] = "bad_gallery"
    # get image
    file_path = os.path.join(DATA_DIR, "img_test")
    with open(file_path, "rb") as f:
        byte_img = f.read()
    # send photo
    success = server.publish_web(
        DATA_DIR,
        credentials["web_server_url"],
        credentials["server_username"],
        credentials["server_password"],
        data,
        byte_img,
    )

    assert success is False


def test_publish_web_missing_required_data(credentials, photo):
    """Should get no success if missing data is required by model"""

    # remove test file from 'web' folder
    try:
        os.remove(os.path.join(DATA_DIR, "web"))
    except OSError:
        pass
    # clear post_waiting_list.yaml file
    post_waiting_file = os.path.join(DATA_DIR, "post_waiting_list.yaml")
    open(post_waiting_file, "w").close()

    # get photo's data
    data = photo
    # remove "gallery" key
    del data["gallery"]
    # get image
    file_path = os.path.join(DATA_DIR, "img_test")
    with open(file_path, "rb") as f:
        byte_img = f.read()
    # send photo
    success = server.publish_web(
        DATA_DIR,
        credentials["web_server_url"],
        credentials["server_username"],
        credentials["server_password"],
        data,
        byte_img,
    )

    assert success is False


def test_publish_web_missing_not_required_data(credentials, photo):
    """Should get success if missing data is not required by model.
    'web', 'local', 'public' have default value in Photo model
    """

    # remove test file from 'web' folder
    try:
        os.remove(os.path.join(DATA_DIR, "web"))
    except OSError:
        pass
    # clear post_waiting_list.yaml file
    post_waiting_file = os.path.join(DATA_DIR, "post_waiting_list.yaml")
    open(post_waiting_file, "w").close()

    # get photo's data
    data = photo
    # change gallery
    del data["public"]
    # get image
    file_path = os.path.join(DATA_DIR, "img_test")
    with open(file_path, "rb") as f:
        byte_img = f.read()
    # send photo
    success = server.publish_web(
        DATA_DIR,
        credentials["web_server_url"],
        credentials["server_username"],
        credentials["server_password"],
        data,
        byte_img,
    )

    assert success is True

    # delete test photo from server
    delete_photo(credentials, photo["filename"])


def test_publish_web_missing_image_data(credentials, photo):
    """Should get success if missing data is not required by model.
    'image' field has default value (None) in Photo model.
    """

    # remove test file from 'web' folder
    try:
        os.remove(os.path.join(DATA_DIR, "web"))
    except OSError:
        pass
    # clear post_waiting_list.yaml file
    post_waiting_file = os.path.join(DATA_DIR, "post_waiting_list.yaml")
    open(post_waiting_file, "w").close()

    # get photo's data
    data = photo
    # set no image used
    byte_img = None
    # send photo
    success = server.publish_web(
        DATA_DIR,
        credentials["web_server_url"],
        credentials["server_username"],
        credentials["server_password"],
        data,
        byte_img,
    )

    assert success is True

    # delete test photo from server
    delete_photo(credentials, photo["filename"])


def test_sync_diffusion_no_photo_in_local_folder(credentials):
    """Test sync_diffusion function.
    No photo in local folder (folder where are stored photos that couldn't be sent to web server)
    """

    # should get no photo in 'IMAGES_DIR'
    list_dir = os.listdir(IMAGES_DIR)
    # be sure folder is empty
    # remove test files from 'IMAGES_DIR'
    for file in list_dir:
        os.remove(os.path.join(IMAGES_DIR, file))
    list_dir = os.listdir(IMAGES_DIR)
    assert len(list_dir) == 0
    gallery = "test_gallery_0"
    sync_diffusion.sync_diffusion(
        DATA_DIR,
        gallery,
        credentials["web_server_url"],
        credentials["server_username"],
        credentials["server_password"],
    )

    # should get 10 photos in 'IMAGES_DIR'
    list_dir = os.listdir(IMAGES_DIR)
    assert len(list_dir) == 10
    # remove test files from 'IMAGES_DIR'
    for file in list_dir:
        os.remove(os.path.join(IMAGES_DIR, file))


def test_sync_diffusion_photo_in_local_folder(credentials):
    """Test sync_diffusion function.
    Photo in local folder (folder where are stored photos that couldn't be sent to web server).
    Server is up so photo in local folder must be removed.
    """

    # create test photos in local dir
    for i in range(10):
        file_name = f"test_photo_{i}.jpg"
        file_path = os.path.join(LOCAL_DIR, file_name)
        with open(file_path, "wb") as f:
            f.write(b"test_image_data")

    # should get 10 photos in 'LOCAL_DIR'
    list_local_dir = os.listdir(LOCAL_DIR)
    assert len(list_local_dir) == 10

    # should get no photo in 'IMAGES_DIR'
    list_dir = os.listdir(IMAGES_DIR)
    # be sure folder is empty
    # remove test files from 'IMAGES_DIR'
    for file in list_dir:
        os.remove(os.path.join(IMAGES_DIR, file))
    list_dir = os.listdir(IMAGES_DIR)
    assert len(list_dir) == 0
    gallery = "test_gallery_0"
    sync_diffusion.sync_diffusion(
        DATA_DIR,
        gallery,
        credentials["web_server_url"],
        credentials["server_username"],
        credentials["server_password"],
    )

    # should get 10 photos in 'IMAGES_DIR'
    list_dir = os.listdir(IMAGES_DIR)
    assert len(list_dir) == 10

    # should get no photos in local dir
    list_local_dir = os.listdir(LOCAL_DIR)
    assert len(list_local_dir) == 0

    # CLEAN
    # remove test files from 'IMAGES_DIR'
    for file in list_dir:
        os.remove(os.path.join(IMAGES_DIR, file))
