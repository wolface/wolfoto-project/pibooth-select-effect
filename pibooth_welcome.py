import pygame
import os
import math
import sys

import pibooth
from pibooth.view.pygame.sprites import BasePygameScene, TextSprite, ImageSprite
from pibooth import evts
from pibooth.utils import LOGGER, PollingTimer
from pibooth.language import get_translated_text


EVT_PIBOOTH_NEXT = pygame.USEREVENT + 300
EVT_PIBOOTH_NEXT_CAPTURE = pygame.USEREVENT + 301
EVT_PIBOOTH_EXIT_1 = pygame.USEREVENT + 310
EVT_PIBOOTH_EXIT_2 = pygame.USEREVENT + 311

__version__ = "1.1.0"


class WelcomeScene(BasePygameScene):
    """no monitor camera, just a screen to welcome to capture"""

    def __init__(self):
        super().__init__()
        self.logo_place = ImageSprite(
            self,
            os.path.join(
                os.path.expanduser("~/Documents/Pibooth/"), "assets", "logo_place.png"
            ),
            colorize=False,
        )
        self.logo_photobooth = ImageSprite(
            self,
            os.path.join(os.path.dirname(__file__), "assets", "logo_photobooth.png"),
            colorize=False,
        )
        if os.path.isfile(
            os.path.join(
                os.path.expanduser("~/Documents/Pibooth/"),
                "assets",
                "touch_screen_msg.png",
            )
        ):
            self.welcome = ImageSprite(
                self,
                os.path.join(
                    os.path.expanduser("~/Documents/Pibooth/"),
                    "assets",
                    "touch_screen_msg.png",
                ),
                colorize=False,
            )
        else:
            LOGGER.info("WOLFOTO : Touch screen message not found. Using standard one.")
            self.welcome = ImageSprite(
                self,
                os.path.join(
                    os.path.dirname(__file__), "assets", "touch_screen_msg.png"
                ),
                colorize=False,
            )
        self.welcome.on_pressed = lambda: evts.post(EVT_PIBOOTH_NEXT)
        self.logo_place.on_pressed = lambda: evts.post(EVT_PIBOOTH_EXIT_1)
        self.logo_photobooth.on_pressed = lambda: evts.post(EVT_PIBOOTH_EXIT_2)

    def resize(self, size):
        """Resize text when window is resized."""

        self.status_bar.hide()

        # Logos
        logo_place_size = (self.rect.height * 2 // 7, self.rect.height // 7)
        logo_place_pos = (self.rect.width - logo_place_size[0], 10)
        self.logo_place.set_rect(*logo_place_pos, *logo_place_size)
        logo_photobooth_size = (self.rect.width // 8, self.rect.width // 8)
        logo_photobooth_pos = (
            self.rect.width - logo_photobooth_size[0],
            self.rect.height - logo_photobooth_size[1],
        )
        self.logo_photobooth.set_rect(*logo_photobooth_pos, *logo_photobooth_size)
        # welcome
        welcome_size = (self.rect.width - 50, self.rect.height - 50)
        welcome_pos = (10, 10)
        self.welcome.set_rect(*welcome_pos, *welcome_size)
        self.welcome.rect.centerx = self.rect.width // 2


@pibooth.hookimpl
def pibooth_configure(cfg):
    """Declare the new configuration options"""

    cfg.add_option(
        "GENERAL",
        "welcome_screen",
        False,
        "Use the welcome screen (no camera monitoring)",
    )
    cfg.add_option(
        "GENERAL",
        "welcome_screen_delay",
        20,
        "Delay of inaction before displaying welcome \
                    screen in seconds",
    )


@pibooth.hookimpl
def pibooth_setup_states(machine):
    """Add a post-processing state"""
    machine.add_state("welcome", WelcomeScene())


@pibooth.hookimpl(optionalhook=True)
def state_welcome_enter(cfg, app, win):

    app.camera.stop_preview()


@pibooth.hookimpl(optionalhook=True)
def state_welcome_do(cfg, app, win, events):
    """Give possibility to exit pibooth with keyboard or touchscreen"""

    for event in events:
        if (
            event.type == pygame.KEYDOWN
            and event.key == pygame.K_q
            and pygame.key.get_mods() & pygame.KMOD_CTRL
        ):
            LOGGER.debug("WOLFOTO : [CTRL Q] pressed -> exit")
            sys.exit(0)

    if evts.find_event(events, EVT_PIBOOTH_EXIT_1) and evts.find_event(
        events, EVT_PIBOOTH_EXIT_2
    ):
        LOGGER.debug("WOLFOTO : 'Exit' fingers combination on touchscreen -> exit")
        sys.exit(0)
