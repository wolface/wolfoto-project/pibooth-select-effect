"""Control relay connected to GPIO 24.
Set relay on.
"""

import RPi.GPIO as gpio
from signal import signal, SIGINT
from sys import exit


def handler(signal_received, frame):
    # clean gpio
    print("")
    print("SIGINT or CTRL-C detected. Exiting gracefully")
    gpio.cleanup()
    exit(0)


def main():

    gpio.setmode(gpio.BCM)
    gpio.setup(24, gpio.OUT)

    print("off")
    gpio.output(24, gpio.LOW)


if __name__ == "__main__":
    # catch SIGINT(Ctl C) signal to clean GPIO
    signal(SIGINT, handler)
    main()
