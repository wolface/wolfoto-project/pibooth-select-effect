#!/usr/bin/python3

"""Raspberry pi5
Configure wakeup and shutdown.

Usage:
  python set_wakeup_and_shutdown.py [-h] [-n] [-l LOGFILE] [-c CONFFILE]

options:
  -h, --help            show this help message and exit
  -n, --noshutdown      Just configure pijuice wakeup, no shutdown
  -l LOGFILE, --logfile LOGFILE
                        Log file's path [default=/tmp/wakeup_shutdown.log]
  -c CONFFILE, --conffile CONFFILE
                        Configuration file's path [default=~/.config/pibooth/pibooth.cfg]
"""

import os
import logging
import configparser
import ast
import datetime
import argparse
import pathlib

from wolf_utils import get_next_inactivity_time

__version__ = "0.0.1"


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-l",
        "--logfile",
        help="Log file's path [default=/tmp/wakeup_shutdown.log]",
        default="/tmp/wakeup_shutdown.log",
    )
    parser.add_argument(
        "-c",
        "--conffile",
        help=f"Configuration file's path [default={os.path.join(os.environ['HOME'], '.config/pibooth/pibooth.cfg')}]",
        default=os.path.join(os.environ["HOME"], ".config/pibooth/pibooth.cfg"),
    )

    options, unknown = parser.parse_known_args()

    logging.basicConfig(
        filename=options.logfile,
        level=logging.INFO,
        format="%(asctime)s %(message)s",
        datefmt="%d/%m/%Y %H:%M:%S",
    )

    logging.info("Running shutdown and wakeup setup script")

    config = configparser.ConfigParser()
    config.read(options.conffile)

    try:
        inactivity_times = ast.literal_eval(config.get("GENERAL", "inactivity_times"))
    except (configparser.NoOptionError, configparser.NoSectionError):
        inactivity_times = []

    logging.info(f"Inactivity times: {inactivity_times}")
    if inactivity_times:
        delta_time = datetime.timedelta(minutes=1)  # Get time before shutdown
        next_time = get_next_inactivity_time(
            inactivity_times, datetime.datetime.now(), delta_time
        )
        logging.info(f"Next inactivity time: {next_time}")

        # set wakeup time
        today = datetime.datetime.today()
        if next_time[1] == next_time[0]:
            # bad configuration
            logging.warning("Shutdown time and wakeup time are the same: do nothing.")
        elif next_time[1] > next_time[0]:
            # supposed that times are the same day
            full_d0 = datetime.datetime.combine(today, next_time[0])
            full_d1 = datetime.datetime.combine(today, next_time[1])
        else:
            # supposed that wakeup time is next day
            full_d0 = datetime.datetime.combine(today, next_time[0])
            full_d1 = datetime.datetime.combine(
                today + datetime.timedelta(days=1), next_time[1]
            )
        delta_time = (full_d1 - full_d0).seconds

        cmd = "/usr/bin/bash " + (
            os.path.join(
                pathlib.Path(__file__).parent, "utils/wolfoto_wakeup_shutdown.sh"
            )
            + f" {delta_time}"
        )
        logging.info(f"Command to run: {cmd}")

        # modify crontab
        crontab_new = f"(/usr/bin/crontab -l | /usr/bin/grep -v wolfoto_wakeup_shutdown.sh; echo '{next_time[0].minute} {next_time[0].hour} * * * {cmd}') | /usr/bin/crontab -"
        logging.info(f"Command to modify crontab: {crontab_new}")
        os.system(crontab_new)
        logging.info("crontab has been modified")
    else:
        logging.info("No inactivity times found in configuration")


if __name__ == "__main__":
    main()
