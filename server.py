""" Functions to communicate with
wolfoto server
"""

import requests
import urllib.parse
import yaml
import os
from PIL import Image
import io

from pibooth.utils import LOGGER


def get_web_token(data_folder, url, username, password):
    """Get JWT token for server authentification

    Params:
    data_folder (str): path to folder containing token files
    url (str): url of server
    username (str): username
    password (str):  password

    Returns:
    success (bool): True if token has been received, False otherwise
    """

    url = os.path.join(url, "api/token/")
    data = {"username": username, "password": password}
    try:
        response = requests.post(url=url, data=data)
    except requests.exceptions.ConnectionError:
        LOGGER.info(f"WOLFOTO : Server at url <{url}> not reachable")
        return False, None, None
    if response.status_code >= 200 and response.status_code < 300:
        data = response.json()
        token_access = data["access"]
        ta = open(os.path.join(data_folder, "token_access.txt"), "w")
        ta.write(token_access)
        ta.close()
        token_refresh = data["refresh"]
        tr = open(os.path.join(data_folder, "token_refresh.txt"), "w")
        tr.write(token_refresh)
        tr.close()
        LOGGER.info(f"WOLFOTO : A JWT token has been received: {token_access}")
        success = True
    else:
        LOGGER.info("WOLFOTO : No JWT token received")
        success = False
        token_access = None
        token_refresh = None
    return success, token_access, token_refresh


def refresh_token(data_folder, url):
    """Refresh JWT token

    Params:
    data_folder (str): path to folder containing token files
    url (str): url of server

    Returns:
    success (bool): True if token has been refreshed, False otherwise
    """

    if os.path.exists(os.path.join(data_folder, "token_refresh.txt")):
        url = os.path.join(url, "api/token/refresh/")
        tr = open(os.path.join(data_folder, "token_refresh.txt"), "r")
        refresh_token = tr.readline()
        tr.close()
        data = {"refresh": refresh_token}
        try:
            response = requests.post(url=url, data=data)
        except requests.exceptions.ConnectionError:
            LOGGER.info(f"WOLFOTO : Server at url <{url} not reachable")
            return False
        if response.status_code >= 200 and response.status_code < 300:
            data = response.json()
            token_access = data["access"]
            ta = open(os.path.join(data_folder, "token_access.txt"), "w")
            ta.write(token_access)
            ta.close()
            LOGGER.debug(f"WOLFOTO : JWT token received after refresh: {token_access}")
            success = True
        else:
            LOGGER.info("WOLFOTO : No JWT token received after refresh")
            success = False
    else:
        success = False
    return success


def send_request_post(url, headers={}, data={}, files={}):
    """Send post request to server with logging if a connection exception
    occured

    Params:
    url (str): url of server
    headers (dict): headers of request
    data (dict): data of request
    files (dict): files of request

    Returns:
    reachable (bool): True if server is reachable
    response (obj): response of request
    """

    try:
        response = requests.post(url=url, headers=headers, data=data, files=files)
        reachable = True
    except requests.exceptions.ConnectionError:
        LOGGER.info(f"WOLFOTO : Server at url <{url}> not reachable")
        reachable = False
        response = None
    return reachable, response


def send_api_request(data_folder, url_base, url_api, username, password):
    """Send api request to server managing authorization token.
    If a connection exception occured, try to refresh token.

    Params:
    data_folder (str): path to folder containing token files
    url_base (str): base url of server
    url_api (str): api url
    username (str): server username
    password (str):  server password

    Returns:
    data (list): json response
    """

    token_access_path = os.path.join(data_folder, "token_access.txt")
    url_request = urllib.parse.urljoin(url_base, url_api)
    try_to_send_request = True
    send_request_success = False

    LOGGER.debug(f"WOLFOTO : Trying to send_api_request {url_api}")
    while try_to_send_request:
        LOGGER.debug("WOLFOTO : try_to_send_request loop")
        if os.path.exists(token_access_path):
            with open(token_access_path, "r") as f:
                token_access = f.readline()
            headers = {"Authorization": f"Bearer {token_access}"}
            try:
                LOGGER.debug(
                    f"WOLFOTO : actual request next, for {url_request}, using headers: {headers}"
                )
                response = requests.get(url=url_request, headers=headers)
                LOGGER.debug(f"WOLFOTO : actual request resulted in {response}")
                reachable = True
            except requests.exceptions.ConnectionError:
                LOGGER.error(f"WOLFOTO : Server at url <{url_base}> not reachable")
                reachable = False
                response = None
            if reachable:
                # Not clean enough, explicit list of valid codes should be enumerated
                if response.status_code >= 200 and response.status_code < 300:
                    send_request_success = True
                    try_to_send_request = False
                elif response.status_code != 403:
                    LOGGER.debug("WOLFOTO : Refreshing token")
                    success = refresh_token(data_folder, url_base)
                    LOGGER.debug(f"WOLFOTO : Refreshing token resulted in: {success}")
                    if not success:
                        LOGGER.debug("WOLFOTO : Ask for a new token")
                        success, token_access, token_refresh = get_web_token(
                            data_folder, url_base, username, password
                        )
                    try_to_send_request = success
                else:
                    LOGGER.error(
                        "WOLFOTO : Got an error 403 while accessing the API, we are not authorized"
                    )
                    try_to_send_request = False
            else:
                try_to_send_request = False

        else:
            LOGGER.debug("WOLFOTO : Getting web token")
            success, token_access, token_refresh = get_web_token(
                data_folder, url_base, username, password
            )
            try_to_send_request = success

    if send_request_success:
        data = response
        LOGGER.debug(f"WOLFOTO : send_request_success: {data}")
    else:
        LOGGER.info("WOLFOTO : No server response.")
        data = None
    return data


def create_post_payload(data_folder, data, byte_img):
    """create data to send by post request to server

    Params:
    data_folder (str): path to folder containing token files
    data (dict): data to send to server
    byte_img (bytes): image to send to server

    Returns:
    headers (dict): headers of request
    data (dict): data of request (unchanged)
    files (dict): files of request
    """

    ta = open(os.path.join(data_folder, "token_access.txt"), "r")
    access_token = ta.readline()
    ta.close()
    headers = {"Authorization": f"Bearer {access_token}"}
    files = {
        "image": (
            f"{data['filename']}.jpg",
            byte_img,
            "image/jpg",
        ),
    }

    return headers, files


def post_photo(
    data_folder, web_server_url, server_username, server_password, data, byte_img
):
    """Send photo data to server

    Params:
    data_folder (str): path to folder containing token files
    web_server_url (str): url of server
    server_username (str): server username
    server_password (str):  server password
    data (dict): data to send to server
    byte_img (bytes): image to send to server

    Returns:
    post_send_success (bool): True if photo has been sent, False otherwise
    data (dict): data to send to server
    """

    filename = data["filename"]
    post_send_success = False

    if os.path.exists(os.path.join(data_folder, "token_access.txt")):
        api_photo_url = os.path.join(web_server_url, "api/photo/")
        LOGGER.debug(
            f"WOLFOTO : api photo url is <{api_photo_url}, filename is <{filename}"
        )
        headers, files = create_post_payload(data_folder, data, byte_img)
        reachable, response = send_request_post(api_photo_url, headers, data, files)
        if reachable:
            # server is reachable
            if response.status_code >= 200 and response.status_code < 300:
                post_send_success = True
                LOGGER.info(
                    f"WOLFOTO : Photo <{filename}> has been sent to server, status_code : {response.status_code}"
                )
            else:
                LOGGER.error(
                    f"WOLFOTO : Photo <{filename}> couldn't be sent to server,\nstatus_code : {response.status_code}, trying to refresh JWT token and repost data."
                )
                LOGGER.debug("WOLFOTO : trying to refresh JWT token")
                success = refresh_token(data_folder, web_server_url)
                if success:
                    reachable, response = send_request_post(
                        api_photo_url, headers, data, files
                    )
                    if reachable:
                        if response.status_code >= 200 and response.status_code < 300:
                            post_send_success = True
                        else:
                            post_send_success = False
                    else:
                        post_send_success = False
                else:
                    # couldn't refresh JWT token, ask for a new token
                    success, _, _ = get_web_token(
                        data_folder, web_server_url, server_username, server_password
                    )
                    if success:
                        reachable, response = send_request_post(
                            api_photo_url, headers, data, files
                        )
                        if reachable:
                            if (
                                response.status_code >= 200
                                and response.status_code < 300
                            ):
                                post_send_success = True
                            else:
                                post_send_success = False
                        else:
                            post_send_success = False
        else:
            post_send_success = False

    else:
        # no JWT token stored, ask for new token
        get_web_token(data_folder, web_server_url, server_username, server_password)

    if post_send_success:
        LOGGER.info(f"WOLFOTO : Photo <{filename}> sent to server")
    else:
        LOGGER.error(f"WOLFOTO : Photo <{filename}> couldn't be sent to server")

    return post_send_success


def post_waiting_photos(data_folder, web_server_url, server_username, server_password):
    """Post photos which couln't be sent before because server issue

    Params:
    data_folder (str): path to folder containing data files as list of photos to send to server
    web_server_url (str): url of server
    server_username (str): server username
    server_password (str):  server password
    """

    LOGGER.info("WOLFOTO : Post waiting photos if any")
    with open(os.path.join(data_folder, "post_waiting_list.yaml"), "r") as f:
        post_list = yaml.safe_load(f)

    if post_list:
        for i in range(len(post_list) - 1, -1, -1):
            data = post_list[i]
            with open(os.path.join(data_folder, "web", data["filename"]), "rb") as f:
                byte_img = f.read()
            post_send_success = post_photo(
                data_folder,
                web_server_url,
                server_username,
                server_password,
                data,
                byte_img,
            )
            if post_send_success:
                os.remove(os.path.join(data_folder, "web", data["filename"]))
                LOGGER.info(
                    f"WOLFOTO : File {os.path.join(data_folder, 'web', data['filename'])} sent to server and removed"
                )
                del post_list[i]
            else:
                # we can't reach server, no need to try to send others
                LOGGER.error("WOLFOTO : server couldn't be reached")
                break

    # update post waiting list file
    if post_list:
        with open(os.path.join(data_folder, "post_waiting_list.yaml"), "w") as f:
            yaml.dump(post_list, f)
    else:
        # clear file content
        open(os.path.join(data_folder, "post_waiting_list.yaml"), "w").close()


def get_photos_data_from_server(
    data_folder, web_server_url, server_username, server_password
):
    """Request web server to get list of already used shortcodes and filenames

    Params:
    data_folder (str): folder where are stored authentication tokens files
    web_server_url (str): url base of web server
    server_username (str): login
    server_password (str): password

    Returns:
    shortcodes (list): shortcodes (str)
    filenames (list): filenames (str)
    """

    url_api = "api/photo/"
    data = send_api_request(
        data_folder,
        web_server_url,
        url_api,
        server_username,
        server_password,
    )
    if data is None:
        shortcodes = None
        filenames = None
    else:
        shortcodes = [photo["code"] for photo in data.json()]
        filenames = [photo["filename"] for photo in data.json()]

    return shortcodes, filenames


def publish_web(
    data_folder,
    web_server_url,
    server_username,
    server_password,
    data_to_send,
    byte_img,
):
    """Publish on web_server. Upload resized photo and linked data to web server.
    If photo couldn't be sent to web server, save it on disk to be sent later.

    Params:
    data_folder (str): root folder of application where are stored files used by application
    as used shortcodes file. Absolute path
    web_server_url (str): url base of web server
    server_username (str): login
    server_password (str): password
    data_to_send (dict): data to send to web server
    byte_img (bytes): image to send to web server

    Returns:
    post_photo_success (bool): True if photo was sent to web server, False otherwise
    """

    LOGGER.info("WOLFOTO : publish to web")
    # send image to server
    post_photo_success = post_photo(
        data_folder,
        web_server_url,
        server_username,
        server_password,
        data_to_send,
        byte_img,
    )
    if post_photo_success is False:
        # save photo on disk to be sent later
        img = Image.open(io.BytesIO(byte_img))
        img.save(
            os.path.join(data_folder, "web", data_to_send["filename"]), format="JPEG"
        )

        # store data in a file to send it later
        with open(os.path.join(data_folder, "post_waiting_list.yaml"), "a") as f:
            yaml.dump([data_to_send], f)
            LOGGER.info(
                f"WOLFOTO : Photo <{data_to_send['filename']}> added to waiting list"
            )

    return post_photo_success
