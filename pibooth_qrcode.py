"""Display window with qrcode when 'web' option is checked
"""

import pygame
import os
import qrcode
from PIL import Image

import pibooth
from pibooth.view.pygame.sprites import BasePygameScene, TextSprite, ImageSprite
from pibooth.utils import PollingTimer
from pibooth import evts
from pibooth import fonts

from widgets import ButtonSprite

__version__ = "1.1.0"


EVT_PIBOOTH_NEXT = pygame.USEREVENT + 300


class QrcodeScene(BasePygameScene):
    """Provide a scene to display qrcode to download
    captured photo
    """

    def __init__(self):
        super().__init__()

        self.logo_place = ImageSprite(
            self,
            os.path.join(
                os.path.expanduser("~/Documents/Pibooth/"), "assets", "logo_place.png"
            ),
            colorize=False,
        )
        self.logo_photobooth = ImageSprite(
            self,
            os.path.join(os.path.dirname(__file__), "assets", "logo_photobooth.png"),
            colorize=True,
        )

        self.qrcode_background = ImageSprite(
            self,
            os.path.join(os.path.dirname(__file__), "assets", "qrcode_back.png"),
            colorize=True,
        )
        self.qrcode_background.set_rect(10, 10, 400, 400)
        self.qrcode_img = ImageSprite(self, colorize=False)
        self.qrcode_img.set_rect(20, 20, 400, 400)
        self.qrcode_text = TextSprite(
            self,
            "Pour télécharger la photo, scannez ce qrcode.",
            font_name=os.path.join(
                os.path.dirname(__file__), "assets", "Montserrat-Bold.ttf"
            ),
            layer=2,
        )
        self.qrcode_text.set_rect(20, 450, 400, 400)
        self.qrcode_text.set_align = fonts.ALIGN_TOP_LEFT

        self.close = ButtonSprite(
            self,
            "Fermer",
            (100, 20),
            skin=os.path.join(os.path.dirname(__file__), "assets", "button_back.png"),
            colorize=True,
        )

        self.close.set_on_pressed(lambda: evts.post(EVT_PIBOOTH_NEXT))

    def resize(self, size):

        # Logos
        logo_place_size = (self.rect.height * 2 // 7, self.rect.height // 7)
        logo_place_pos = (self.rect.width - logo_place_size[0], 10)
        self.logo_place.set_rect(*logo_place_pos, *logo_place_size)
        logo_photobooth_size = (self.rect.width // 9, self.rect.width // 9)
        logo_photobooth_pos = (
            self.rect.width - logo_photobooth_size[0],
            self.rect.height - logo_photobooth_size[1],
        )
        self.logo_photobooth.set_rect(*logo_photobooth_pos, *logo_photobooth_size)

        # QRcode
        qrcode_background_size = (self.rect.width * 0.6, self.rect.height * 0.9)
        qrcode_background_pos = (
            self.rect.width // 2 - qrcode_background_size[0] // 2,
            20,
        )
        self.qrcode_background.set_rect(*qrcode_background_pos, *qrcode_background_size)
        qrcode_img_size = (
            min(
                350,
                self.qrcode_background.rect.width * 0.5,
                self.qrcode_background.rect.height * 0.5,
            ),
            min(
                350,
                self.qrcode_background.rect.width * 0.5,
                self.qrcode_background.rect.height * 0.5,
            ),
        )
        qrcode_img_pos = (
            self.qrcode_background.rect.centerx - (qrcode_img_size[0] // 2),
            self.qrcode_background.rect.top + 20,
        )
        self.qrcode_img.set_rect(*qrcode_img_pos, *qrcode_img_size)

        qrcode_text_size = (
            self.qrcode_background.rect.width - 40,
            self.rect.height // 4,
        )
        qrcode_text_pos = (
            self.qrcode_background.rect.left + 20,
            self.qrcode_img.rect.bottom + 20,
        )
        self.qrcode_text.set_rect(*qrcode_text_pos, *qrcode_text_size)

        # Close button
        text_height = self.rect.height // 25

        close_size = (self.rect.width // 7, text_height * 2)
        close_pos = (
            self.qrcode_background.rect.right + 20,
            self.qrcode_background.rect.centery,
        )
        self.close.set_rect(*close_pos, *close_size)
        padding = (
            self.rect.height // 150,
            self.rect.width // 100,
            self.rect.height // 150,
            self.rect.width // 100,
        )
        self.close.set_padding(padding)


@pibooth.hookimpl
def pibooth_setup_states(machine):
    """Add a display qrcode state"""
    machine.add_state("qrcode", QrcodeScene())


@pibooth.hookimpl(optionalhook=True)
def state_qrcode_enter(cfg, app, win):
    """Start a timer during which qrcode is displayed"""

    # set color of photobooth logo
    logo_photobooth_color = cfg.gettuple("WINDOW", "logo_photobooth_color", int)
    win.scene.logo_photobooth.set_color(logo_photobooth_color)

    # set color of qrcode background
    button_background_color = cfg.gettuple("WINDOW", "button_background_color", int)
    win.scene.qrcode_background.set_color(button_background_color)

    # set color of close button
    button_background_color = cfg.gettuple("WINDOW", "button_background_color", int)
    win.scene.close.set_background_color(button_background_color)

    # timer to come back to camera monitoring if no validation
    app.readystate_timer = PollingTimer(30)

    # generate qrcode image
    qr = qrcode.QRCode(
        version=1,
        error_correction=qrcode.constants.ERROR_CORRECT_L,
        box_size=3,
        border=1,
    )
    web_server_url = cfg.get("SERVER", "web_server_url")
    qr.add_data(
        os.path.join(
            web_server_url,
            f"photo/download_user/?filename={app.filename}&code={app.shortcode}",
        )
    )
    qr.make(fit=True)
    qr_img = qr.make_image(fill_color="black", back_color="white")
    img = qr_img.get_image().resize((400, 400), Image.NEAREST).convert("RGB")
    win.scene.qrcode_img.set_skin(img)
    win.scene.qrcode_text.set_text(
        f"Pour télécharger la photo,\nscannez ce qrcode ou allez\nsur {web_server_url}\net utilisez le code '{app.shortcode}'.\nIl est valable 24h."
    )
