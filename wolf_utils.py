"""Some functions to use in pibooth
"""

import datetime
import os
import subprocess
import random
import uuid
import shutil
from PIL import Image
import PIL

from pibooth.utils import LOGGER


def get_next_inactivity_time(inactivity_times, date_ref, delta_time):
    """Get next time in a list compared to date_ref.
    Utility to get next inactivity time to update wakeup and shutdown setup, used for Raspberry pi5.

    Parameters :
    inactivity_times : list of string of iso formated time ("HH:MM:SS")
    date_ref (datetime.datetime) : reference datetime
    delta_time (datetime.timedelta): amount of time to let the system shutdown and be ready to restart

    Returns :
    datetime.time : time to set to wakeup raspberry

    """

    if inactivity_times:
        inactivity_times = [
            (datetime.time.fromisoformat(h[0]), datetime.time.fromisoformat(h[1]))
            for h in inactivity_times
        ]
        inactivity_times.sort(key=lambda x: x[0])

        date = date_ref + delta_time
        time_ref = date.time()

        found = False
        i = 0
        while not found:
            if time_ref >= inactivity_times[i][0]:
                if i == len(inactivity_times) - 1:
                    i = 0
                    found = True
                else:
                    i = i + 1
            else:
                found = True
        return inactivity_times[i]
    else:
        return False


def cp_to_local_diffusion(source, cfg):
    """Copy photo to local diffusion folder

    Params:
    source (str): absolute path of file to copy
    cfg (pibooth.config.parser.PiboothConfigParser): pibooth configuration object

    Returns:
    exit_code (int) :
        0 : success
        1 : exception raised by subprocess running 'scp' command
        2 : missing parameters in pibooth configuration file disabling the copy
        3 : source doesn't exist
    """

    if os.path.exists(source):

        try:
            diffusion_server = cfg.get("DIFFUSION_KIOSK", "diffusion_server")
            diffusion_user = cfg.get("DIFFUSION_KIOSK", "diffusion_user")
            diffusion_local_folder_path = cfg.get(
                "DIFFUSION_KIOSK", "diffusion_local_folder_path"
            )
        except KeyError:
            LOGGER.info(
                "WOLFOTO : Trying to copy captured photo to kiosk diffusion local folder. \nNo 'DIFFUSION_KIOSK' section in pibooth config file."
            )
            exit_code = 4
            return exit_code
        else:
            if diffusion_local_folder_path and diffusion_user and diffusion_server:
                if diffusion_server == "localhost":
                    # copy photo to diffusion kiosk folder using cp
                    try:
                        shutil.copy(source, diffusion_local_folder_path)
                    except Exception as e:
                        LOGGER.info(
                            f"WOLFOTO : an error occured when trying to copy captured photo to local kiosk diffusion folder.\nCheck path of local kiosk diffusion folder.\nError is {e}"
                        )
                        exit_code = 1
                    else:
                        LOGGER.info(
                            "WOLFOTO : Captured photo copied to kiosk diffusion local folder"
                        )
                        exit_code = 0
                else:
                    # copy photo to diffusion kiosk folder using scp
                    try:
                        subprocess.run(
                            [
                                "scp",
                                source,
                                f"{diffusion_user}@{diffusion_server}:{diffusion_local_folder_path}",
                            ],
                            timeout=5,
                            check=True,
                        )
                        # timeout argument allows to exit subprocess if time to execute is too long.
                        # For example, if there is a mistake in script's server argument, subprocess could ask
                        # password for ssh access blocking the application.
                        # A 'subprocess.TimeoutExpired' exception is raised.
                    except Exception as e:
                        LOGGER.info(
                            f"WOLFOTO : an error occured when trying to copy captured photo to local kiosk diffusion folder.\nCheck path of local kiosk diffusion folder.\nError is {e}"
                        )
                        exit_code = 1
                    else:
                        LOGGER.info(
                            "WOLFOTO : captured photo copied to kiosk diffusion local folder"
                        )
                        exit_code = 0
            else:
                LOGGER.info(
                    "WOLFOTO : Trying to copy captured photo to kiosk diffusion local folder. \nNot enough informations in pibooth config file. See section [DIFFUSION_KIOSK]."
                )
                exit_code = 2
    else:
        LOGGER.info(
            f"WOLFOTO : Trying to copy captured photo to kiosk diffusion local folder. \nFile {source} does not exist."
        )
        exit_code = 3
    return exit_code


def generate_shortcode(len, used_shortcodes):
    """Return a random code built of chars choosen in a list.
    Shortcodes are used to facilitate memory to access captured photos.
    This function loads shortcodes list and updates it.

    Params:
    len (int): base length of shortcode
    used_shortcodes (list): shortcodes already used (str)

    Returns:
    shortcode (str): generated shortcode
    """

    chars = ["b", "c", "d", "e", "f", "h", "j", "k", "p", "r", "t", "v", "x", "y"]
    shortcode = ""
    valide = False

    nb_attempt = 0
    while valide is False:
        for i in range(len):
            shortcode += random.choice(chars)
        if shortcode in used_shortcodes:
            valide = False
            shortcode_base = shortcode
            if nb_attempt < 5:
                # 5 attempts to get a 'len' characters shortcode
                # if not possible, shortcode will have 'len' + 1 characters,
                # and so on
                shortcode = shortcode_base
                nb_attempt += 1
            else:
                nb_attempt = 0
                len = 1
        else:
            valide = True

    return shortcode


def generate_filename(used_filenames):
    """Return a filename not already used.

    Params:
    used_filenames (list): already used filenames

    Returns:
    filename (str): new filename
    """

    filename = ""
    while filename in used_filenames or (len(filename) < 1):
        filename = str(uuid.uuid4())

    return filename


def open_image(image_path):
    """Open and check if image is valid

    Parameters:
    image_path (str): Image path

    Returns:
    img: PIL image
    """

    try:
        img = Image.open(image_path, formats=("JPEG", "PNG")).convert(
            "RGB"
        )  # necessary conversion to deal with greyscale image
        LOGGER.debug(f"WOLFOTO : Image <{image_path}> is loaded.")
    except FileNotFoundError:
        LOGGER.error(f"WOLFOTO : Image {image_path} not found.")
        img = None
    except OSError as e:
        LOGGER.error(
            f"WOLFOTO : Error while opening image {image_path}. Error: {str(e)}"
        )
        img = None
    except PIL.UnidentifiedImageError:
        LOGGER.error(f"WOLFOTO : Image {image_path} is not a valid image. Remove image")
        try:
            os.remove(image_path)
        except FileNotFoundError:
            pass
        img = None

    return img
