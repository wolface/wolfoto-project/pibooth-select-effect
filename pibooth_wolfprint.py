import pygame
import os

import pibooth
from pibooth.view.pygame.sprites import BasePygameScene, TextSprite, ImageSprite
from pibooth import evts, fonts
from pibooth.utils import LOGGER, PollingTimer
from pibooth.pictures import get_picture_factory


EVT_PIBOOTH_NEXT = pygame.USEREVENT + 300
EVT_PIBOOTH_PRINT_CHOICE = pygame.USEREVENT + 305

__version__ = "1.0.0"


class WolfPrintScene(BasePygameScene):
    """Print choice"""

    def __init__(self):
        super().__init__()
        self.logo_place = ImageSprite(
            self,
            os.path.join(
                os.path.expanduser("~/Documents/Pibooth/"), "assets", "logo_place.png"
            ),
            colorize=False,
        )
        self.logo_photobooth = ImageSprite(
            self,
            os.path.join(os.path.dirname(__file__), "assets", "logo_photobooth.png"),
            colorize=False,
        )
        self.submit_background = ImageSprite(self, (100, 100, 100))
        self.submit_background.set_rect(10, 10, 400, 400)
        self.submit = TextSprite(self, "Envoyer", layer=2)
        self.submit.set_rect(220, 400, 100, 20)
        self.print_choice = TextSprite(
            self, text="imprimer la photo ? ", size=(450, 50)
        )
        self.print_choice.align = fonts.ALIGN_CENTER_LEFT
        self.print_check = ImageSprite(
            self,
            os.path.join(os.path.dirname(__file__), "assets", "check_no.png"),
            size=(40, 40),
            colorize=False,
        )

        self.submit.on_pressed = lambda: evts.post(EVT_PIBOOTH_NEXT)
        self.print_check.on_pressed = lambda: evts.post(EVT_PIBOOTH_PRINT_CHOICE)

    def resize(self, size):
        """Resize text when window is resized."""

        # Show picture to print
        self.image.set_rect(10, 10, self.rect.width // 2, self.rect.height - 20)

        # Logos
        logo_place_size = (self.rect.height * 2 // 7, self.rect.height // 7)
        logo_place_pos = (self.rect.width - logo_place_size[0], 10)
        self.logo_place.set_rect(*logo_place_pos, *logo_place_size)
        logo_photobooth_size = (self.rect.width // 9, self.rect.width // 9)
        logo_photobooth_pos = (
            self.rect.width - logo_photobooth_size[0],
            self.rect.height - logo_photobooth_size[1],
        )
        self.logo_photobooth.set_rect(*logo_photobooth_pos, *logo_photobooth_size)

        ## print choice
        text_height = self.rect.height // 20
        print_check_pos = (self.image.rect.right + 80, self.rect.height // 3)
        print_check_size = (self.rect.width // 20, text_height * 0.9)
        self.print_check.set_rect(*print_check_pos, *print_check_size)
        print_choice_size = (
            self.rect.width - (self.print_check.rect.right + 10),
            text_height,
        )
        self.print_choice.set_rect(*self.print_check.rect.topright, *print_choice_size)
        self.print_choice.rect.centery = self.print_check.rect.centery - 2
        self.print_choice.rect.left = self.print_check.rect.right + 10

        # submit
        submit_background_size = (self.rect.width // 8, text_height)
        submit_background_pos = (
            self.print_check.rect.left,
            self.print_check.rect.bottom + 50,
        )
        self.submit_background.set_rect(*submit_background_pos, *submit_background_size)
        submit_size = (
            self.submit_background.rect.width - 10,
            self.submit_background.rect.height - 5,
        )
        submit_pos = (
            self.submit_background.rect.left + 5,
            self.submit_background.rect.top + 2,
        )
        self.submit.set_rect(*submit_pos, *submit_size)


@pibooth.hookimpl
def pibooth_setup_states(machine):
    """Add a post-processing state"""
    machine.add_state("wolfprint", WolfPrintScene())


@pibooth.hookimpl(optionalhook=True)
def state_wolfprint_enter(cfg, app, win):

    # cancel all printer tasks to prevent from printing old captures
    # if a problem with the printer occured
    app.printer.cancel_all_tasks()
    LOGGER.debug("WOLFOTO : PRINTER : cancel all previous tasks")

    win.scene.set_image(app.previous_picture)
    # timer to come back to camera monitoring if no validation
    app.readystate_timer = PollingTimer(30)
    # flag reflecting print choice
    cfg.wolf_print = 0
    win.scene.print_check.set_skin(
        os.path.join(os.path.dirname(__file__), "assets", "check_no.png")
    )


@pibooth.hookimpl(optionalhook=True)
def state_wolfprint_do(cfg, app, win, events):

    if evts.find_event(events, EVT_PIBOOTH_PRINT_CHOICE):
        if cfg.wolf_print == 1:
            win.scene.print_check.set_skin(
                os.path.join(os.path.dirname(__file__), "assets", "check_no.png")
            )
            cfg.wolf_print = 0
        else:
            win.scene.print_check.set_skin(
                os.path.join(os.path.dirname(__file__), "assets", "check_yes.png")
            )
            cfg.wolf_print = 1


def bw_filter(image):
    """Dumb cv2 black and white filter"""
    return image.convert("L")


@pibooth.hookimpl
def pibooth_setup_picture_factory(cfg, factory):
    """Replace the picture factory using our own, that will filter images"""
    # Not so brilliant, as captures are already there, we need to read _images
    # which should be considered private.

    # color = cfg.gettuple('PICTURE', 'text_colors', 'color')
    # text_font = cfg.gettuple('PICTURE', 'text_fonts', str)
    # factory.add_text("coucou", text_font[0], color[0])
    if cfg.fx == "b_w":
        filtered_images = [bw_filter(image) for image in factory._images]
        return get_picture_factory(filtered_images, cfg.get("PICTURE", "orientation"))
