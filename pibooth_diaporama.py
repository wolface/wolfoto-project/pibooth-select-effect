import os
import os.path as osp
import pygame
import pathlib
import PIL
from PIL import Image
import datetime

import pibooth
from pibooth import evts
from pibooth.view.pygame.sprites import BasePygameScene, ImageSprite, BaseSprite
from pibooth.utils import LOGGER, PollingTimer
from pibooth import pictures

from sync_diffusion import get_updates_diffusion_folder
from wolf_utils import open_image

EVT_PIBOOTH_NEXT = pygame.USEREVENT + 300
EVT_PIBOOTH_EXIT_1 = pygame.USEREVENT + 310
EVT_PIBOOTH_EXIT_2 = pygame.USEREVENT + 311


__version__ = "1.0.0"


class DiaporamaSprite(BaseSprite):
    """Diaporama image sprite."""

    def __init__(self, parent, skin=None, **kwargs):
        """
        :param parent: sprite to which current sprite is linked
        :type parent: object
        :param skin: image file path, RGB color tuple,  PIL image or Pygame surface
        :type skin: str or tuple or object
        """
        kwargs["layer"] = kwargs.get("layer", BasePygameScene.LAYER_IMAGES)
        super().__init__(parent, **kwargs)
        self._image_orig = None
        self.path = None
        if skin:
            self.set_skin(skin)

    def __repr__(self):
        if self.path:
            elem = f"path='{osp.basename(self.path)}'"
        else:
            elem = f"image={self._image_orig}"
        return f"{self.__class__.__name__}({elem}, rect={tuple(self.rect)})"

    def draw(self):
        """Render image."""
        if self._image_orig is None:
            if self.path:
                self._image_orig = pictures.load_pygame_image(self.path)
            else:
                raise ValueError(f"Path to image is missing for '{self}'")

        if isinstance(self._image_orig, (tuple, list)):
            image = pygame.Surface(self.rect.size, pygame.SRCALPHA, 32)
            image.fill(self._image_orig)
            return image
        elif isinstance(self._image_orig, PIL.Image.Image):
            surface = pygame.image.frombuffer(
                self._image_orig.tobytes(), self._image_orig.size, self._image_orig.mode
            )
            return surface
        else:
            return self._image_orig

    def set_skin(self, skin):
        """Set skin used to fill the sprite. Skin can be:

            - RGB color tuple
            - path to an image
            - PIL image object
            - Pygame surface object

        :param skin: image file path, RGB color tuple,  PIL image or Pygame surface
        :type skin: str or tuple or object
        """
        if isinstance(skin, str):
            if skin != self.path:
                self.path = skin
                self._image_orig = None
                self.set_dirty()
        elif isinstance(skin, (tuple, list)):
            assert len(skin) == 3, "Length of 3 is required for RGB tuple"
            if skin != self._image_orig:
                self.path = None
                self._image_orig = skin
                self.set_dirty()
        else:
            assert isinstance(
                skin, (PIL.Image.Image, pygame.Surface)
            ), "PIL Image or Pygame Surface is required"
            if skin != self._image_orig:
                self.path = None
                self._image_orig = skin
                self.set_dirty()

    def get_skin(self):
        """Return the current skin."""
        return self.path or self._image_orig


class DiaporamaScene(BasePygameScene):
    """Diaporama.
    Can be displayed fullscreen or on same screen than camera_monitor plugin
    """

    def __init__(self):
        super().__init__()

        self.image.set_skin((0, 0, 0))

        if os.path.isfile(
            os.path.expanduser("~/Documents/Pibooth/assets/background.png")
        ):
            self.diaporama = DiaporamaSprite(
                self,
                os.path.expanduser("~/Documents/Pibooth/assets/background.png"),
            )
        else:
            self.diaporama = DiaporamaSprite(
                self,
                os.path.join(os.path.dirname(__file__), "assets", "background.png"),
            )
        self.diaporama.image  # to get rid of exception of _image_cache not initialized, see 'image' property of BaseSprite
        self.diaporama.on_pressed = lambda: evts.post(EVT_PIBOOTH_NEXT)

        # display message if exists. Set layer=7 to display over diaporama(layer=5)
        if os.path.isfile(
            os.path.join(
                os.path.expanduser("~/Documents/Pibooth/"),
                "assets",
                "touch_screen_msg.png",
            )
        ):
            self.message = ImageSprite(
                self,
                os.path.join(
                    os.path.expanduser("~/Documents/Pibooth/"),
                    "assets",
                    "touch_screen_msg.png",
                ),
                colorize=False,
                layer=7,
            )
        else:
            LOGGER.warning(
                "Touch screen message image not found. Please check path and name if needed."
            )

    def resize(self, size):
        """Resize text when window is resized."""

        self.status_bar.hide()
        self.diaporama.set_rect(0, 0, self.rect.width, self.rect.height)

        # resize message if exists
        if hasattr(self, "message"):
            message_size = (
                self.diaporama.rect.width * 0.5,
                self.diaporama.rect.height * 0.5,
            )
            message_pos = (50, 50)
            self.message.set_rect(*message_pos, *message_size)
            self.message.rect.center = self.diaporama.rect.center


@pibooth.hookimpl
def pibooth_setup_states(machine):
    """Add a state"""
    machine.add_state("diaporama", DiaporamaScene())


@pibooth.hookimpl
def pibooth_configure(cfg):
    cfg.add_option(
        "DIFFUSION_KIOSK",
        "diaporama_size",
        (1920, 1080),
        "diaporama: screen size when WINDOW size in config file is fullscreen",
    )


@pibooth.hookimpl(optionalhook=True)
def state_diaporama_enter(cfg, app, win):

    app.camera.stop_preview()

    # create timer for diaporama
    try:
        app.timer_diapo.is_started()
    except Exception:
        app.timer_diapo = PollingTimer(start=False)

    # display message timer if used
    if os.path.isfile(
        os.path.join(
            os.path.expanduser("~/Documents/Pibooth/"),
            "assets",
            "touch_screen_msg.png",
        )
    ):
        app.diapo_msg_tempo = cfg.gettuple("DIFFUSION_KIOSK", "diapo_msg_tempo", int)
        try:
            app.timer_diapo_msg.is_started()

        except Exception:
            app.timer_diapo_msg = PollingTimer()
        app.timer_diapo_msg.start(app.diapo_msg_tempo[0])
        app.timer_diapo_msg_show = False
        win.scene.message.hide()

    win.scene.background.set_skin((0, 0, 0))  # black background for diaporama
    win.scene.diaporama.set_skin((0, 0, 0))  # initialize diaporama image

    # set diaporama parameters
    app.transition_delta_alpha = 1 / (
        cfg.getfloat("DIFFUSION_KIOSK", "transition_duration") * 25
    )  # 25 is the number of frames per second defined in pibooth
    app.diffusion_interval_time = cfg.getfloat("DIFFUSION_KIOSK", "interval_time")
    app.diffusion_interval_time_new = cfg.getfloat(
        "DIFFUSION_KIOSK", "interval_time_new"
    )
    app.transition_alpha = 0
    app.diapo_image_a = None
    app.diapo_image_b = None
    app.diapo_next_image = None
    app.diapo_in_transition = False
    app.diapo_idx = 0
    app.diapo_next_transition_ready = False
    app.diapo_transition_mode = cfg.get("DIFFUSION_KIOSK", "transition_mode")
    app.diapo_new_image = False

    if cfg.getboolean("GENERAL", "debug"):
        # debug loop number
        app.loop = 0
        app.time_prec = datetime.datetime.now()

    app.timer_diapo.start(1)  # to have a full first image


def check_valid_image(images, idx):
    """Check if image is valid"""

    # LOGGER.debug(f"WOLFOTO : check_valid_image(): images <{images}>")

    if len(images) == 0:
        LOGGER.info("WOLFOTO : No image to display found")
        img = Image.open(
            os.path.expanduser("~/Documents/Pibooth/assets/background.png")
        )
    else:
        check = True
        img = None
        idx_initial = int(idx)
        while check:
            try:
                img = Image.open(images[idx], formats=("JPEG", "PNG")).convert(
                    "RGB"
                )  # necessary conversion to deal with greyscale image
                LOGGER.debug(
                    f"WOLFOTO : Image <{images[idx]}> is loaded. Index is <{idx}>"
                )
            except PIL.UnidentifiedImageError:
                LOGGER.error(f"WOLFOTO : Image {images[idx]} is not a valid image")
                if len(images) == 1:
                    LOGGER.info("WOLFOTO : No image to display found")
                    img = Image.open(
                        os.path.expanduser("~/Documents/Pibooth/assets/background.png")
                    )
                    check = False
                else:
                    idx += 1
                    if idx >= len(images):
                        idx = 0
                    if idx == idx_initial:
                        # prevent from eternal loop
                        check = False
            else:
                check = False

    return img, idx


def get_diapo_image(images_dict, ordered_list, idx, background_image=None):
    """Get diaporama image

    Parameters:
    images_dict (dict): Dictionary of images
    ordered_list (list): List of ordered images path
    idx (int): Index of current image
    background_image (PIL image): Background image for the diaporama if no images to display

    Returns:
    img: PIL image
    """

    if len(images_dict) == 0:
        LOGGER.info("WOLFOTO : No image to display found")
        if background_image:
            img = background_image
    else:
        img = images_dict[ordered_list[idx]]
        LOGGER.debug(f"WOLFOTO : get image at index : <{idx}>, image : <{img}")
    return img


def transition(image_a, image_b, alpha, delta_alpha, mode="cut"):
    """Perform transition between two images"""

    # LOGGER.debug(f"WOLFOTO : do transition. alpha : <{alpha}>")

    if mode == "fade":
        # fading transition
        if image_b:
            if alpha <= 1:
                if image_a.size == image_b.size:
                    image = Image.blend(image_a, image_b, alpha)
                    in_transition = True
                    alpha += delta_alpha
                else:
                    in_transition = False
                    image = image_b
                    alpha = 0

            else:
                in_transition = False
                image = image_b
                alpha = 0
        else:
            in_transition = False
            image = image_a
            alpha = 0
    else:
        # cut transition
        in_transition = False
        if image_b:
            image = image_b
        else:
            image = image_a
        alpha = 0

    return image, alpha, in_transition


@pibooth.hookimpl(optionalhook=True)
def state_diaporama_do(cfg, app, win, events):
    """Display diaporama"""

    if cfg.getboolean("GENERAL", "debug"):
        # debug loop number
        app.loop += 1
        now = datetime.datetime.now()
        LOGGER.debug(f"WOLFOTO[{now.strftime('%X %f')}] : loop <{app.loop}>")
        delta = now - app.time_prec
        LOGGER.debug(f"WOLFOTO : delta <{delta.total_seconds()}>")
        app.time_prec = now

    # beginning of diaporama
    if app.diapo_image_a is None:
        if app.timer_diapo.is_timeout():
            # load images
            app.diapo_image_a = get_diapo_image(
                app.diapo_images_dict,
                app.diapo_images_keys,
                app.diapo_idx,
                app.diapo_background_image,
            )
            app.diapo_idx += 1
            if app.diapo_idx >= app.diaporama_length:
                app.diapo_idx = 0

            # center image
            x = (win.scene.rect.width - app.diapo_image_a.width) / 2
            y = (win.scene.rect.height - app.diapo_image_a.height) / 2
            win.scene.diaporama.set_rect(
                x, y, app.diapo_image_a.width, app.diapo_image_a.height
            )

            win.scene.diaporama.set_skin(app.diapo_image_a)

    else:
        if app.diapo_in_transition:
            img, app.transition_alpha, app.diapo_in_transition = transition(
                app.diapo_image_a,
                app.diapo_image_b,
                app.transition_alpha,
                app.transition_delta_alpha,
                mode=app.diapo_transition_mode,
            )
            if not app.diapo_in_transition:
                # transition is finished, prepare next transition and start diaporama timer
                app.diapo_next_transition_ready = False
                if app.diapo_new_image:
                    app.timer_diapo.start(app.diffusion_interval_time_new)
                    app.diapo_new_image = False
                else:
                    app.timer_diapo.start(app.diffusion_interval_time)
            # center image
            x = (win.scene.rect.width - img.width) / 2
            y = (win.scene.rect.height - img.height) / 2
            win.scene.diaporama.set_rect(x, y, img.width, img.height)
            win.scene.diaporama.set_skin(img)

        else:
            # not in transition, we can prepare next transition
            if app.timer_diapo.is_timeout():
                # setup for next transition
                if app.diapo_image_b:
                    app.diapo_image_a = app.diapo_image_b
                app.diapo_image_b = app.diapo_next_image
                app.transition_alpha = 0
                app.diapo_in_transition = True  # transition starts at next loop
            else:
                images_path = [
                    img for img in pathlib.Path(app.diffusion_path).iterdir()
                ]
                if set(images_path) != set(app.diapo_images_dict.keys()):
                    # get differences
                    items_to_remove, items_to_add = get_updates_diffusion_folder(
                        images_path, app.diapo_images_dict.keys()
                    )
                    if items_to_remove:
                        for item in items_to_remove:
                            try:
                                app.diapo_images_dict.pop(item)
                                LOGGER.debug(f"WOLFOTO : image removed: <{item}>")
                            except KeyError:
                                LOGGER.error(
                                    f"WOLFOTO : trying file <{item}> to be removed from images dict raised exception."
                                )

                    if items_to_add:
                        win_w, win_h = app.diaporama_size
                        for item in items_to_add:
                            img = open_image(item)
                            if img:
                                # scale img
                                ratio = img.width / win_w
                                height = img.height / ratio
                                if height > win_h:
                                    height = win_h
                                    ratio = img.height / win_h
                                    width = img.width / ratio
                                else:
                                    width = win_w
                                img = img.resize((round(width), round(height)))
                                app.diapo_images_dict[item] = img
                                LOGGER.debug(f"WOLFOTO : image added: <{item}>")
                        app.diapo_new_image = True

                    # sort keys
                    app.diapo_images_keys = sorted(
                        list(app.diapo_images_dict.keys()),
                        key=os.path.getmtime,
                        reverse=True,
                    )

                    app.diaporama_length = len(app.diapo_images_dict)
                    if len(app.diapo_images_dict) > 0:
                        app.diaporama_last_time = os.path.getmtime(
                            app.diapo_images_keys[0]
                        )
                    else:
                        app.diaporama_last_time = 0
                    app.diapo_idx = 0
                    app.diapo_next_transition_ready = False
                if not app.diapo_next_transition_ready:
                    # load next image
                    app.diapo_next_image = get_diapo_image(
                        app.diapo_images_dict,
                        app.diapo_images_keys,
                        app.diapo_idx,
                        app.diapo_background_image,
                    )
                    app.diapo_next_transition_ready = True
                    app.diapo_idx += 1
                    if app.diapo_idx >= app.diaporama_length:
                        app.diapo_idx = 0

    # display message if used and 'cameramonitor' state is registered
    if hasattr(app, "timer_diapo_msg") and "cameramonitor" in cfg.wolfstates:
        if app.timer_diapo_msg.is_timeout():
            if app.timer_diapo_msg_show:
                win.scene.message.hide()
                app.timer_diapo_msg_show = False
                app.timer_diapo_msg.reset()
                app.timer_diapo_msg.start(app.diapo_msg_tempo[1])
            else:
                win.scene.message.show()
                app.timer_diapo_msg_show = True
                app.timer_diapo_msg.reset()
                app.timer_diapo_msg.start(app.diapo_msg_tempo[0])
