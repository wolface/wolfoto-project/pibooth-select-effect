"""Customized widgets for Wolfoto"""

import pygame
from pibooth.view.pygame.sprites import (
    BasePygameScene,
    ImageSprite,
    BaseSprite,
    TextSprite,
)
from pibooth.pictures import sizing


class BorderSprite(BaseSprite):
    """Border Sprite. Paint a colored rectange around the given
    sprite.
    """

    def __init__(self, parent, color=(255, 0, 0), thick=10):
        """
        :param parent: sprite on which borders are drawn
        :type parent: object
        :param color: RGB color tuple for the border
        :type color: tuple
        :param thick: border thickness
        :type thick: int
        """
        super().__init__(parent, outlines=False, layer=BasePygameScene.LAYER_IMAGES)
        self.thick = thick
        self.parent = parent
        self.set_geometry()
        self.set_color(color)
        self.visible = 1
        self.enabled = True

    def __repr__(self):
        return f"{self.__class__.__name__}({self.parent})"

    def draw(self):
        """Render image."""
        image = pygame.Surface(self.rect.size, pygame.SRCALPHA, 32)
        pygame.draw.rect(
            image,
            self.color,
            (0, 0, self.rect.width, self.rect.height),
            self.thick,
        )
        return image

    def show(self):
        """Show sprite."""
        if not self.visible and self.enabled:
            self.visible = 1

    def hide(self):
        """Hide sprite."""
        if self.visible:
            self.visible = 0

    def add_sprite(self, sprite):
        """Add a sub-sprite to compose the sprite."""
        raise ValueError(
            f"Adding sub-sprite {sprite} to {self.__class__.__name__} not permited"
        )

    def set_pressed(self, state, toggle_timeout=None):
        """Border can not be pressed."""
        del state, toggle_timeout

    def enable(self):
        """Show borders (only if reference sprite is visible)."""
        if not self.enabled:
            self.enabled = True
            if self.parent.visible and not self.visible:
                self.visible = 1

    def disable(self):
        """Hide borders."""
        if self.enabled:
            self.enabled = False
            self.visible = 0

    def set_geometry(self):
        """Based on parent and thick border"""

        if hasattr(self.parent, "_image_orig") and hasattr(
            self.parent._image_orig, "size"
        ):
            # get size of displayed preview
            new_size = sizing.new_size_keep_aspect_ratio(
                self.parent._image_orig.size, self.parent.rect.size
            )
            self.set_rect(
                self.parent.rect.x
                - self.thick
                + 1
                + (self.parent.rect.w - new_size[0]) // 2,
                self.parent.rect.y
                - self.thick
                + 1
                + (self.parent.rect.h - new_size[1]) // 2,
                new_size[0] - 2 + 2 * self.thick,
                new_size[1] - 2 + 2 * self.thick,
            )
        else:
            self.set_rect(
                self.parent.rect.x - self.thick,
                self.parent.rect.y - self.thick,
                self.parent.rect.w + 2 * self.thick,
                self.parent.rect.h + 2 * self.thick,
            )


class ButtonSprite:
    """Button Sprite. Draw a colored button with text.
    Build on ImagesSprite and TextSprite
    """

    def __init__(
        self,
        parent,
        text,
        size,
        skin=(100, 100, 100),
        on_pressed=None,
        padding=(5, 20, 5, 20),
        colorize=False,
        background_color=None,
    ):
        """Initialize button sprite.

        Parameters:
        parent: parent sprite,
        text: button text,
        size: button size,
        skin: button background color or path of image,
        on_pressed: callback function when button is pressed,
        colorize: if True, colorize the button,
        padding: padding around text (top, right, bottom, left)
        """

        self.button_sprite = ImageSprite(
            parent, skin=skin, colorize=colorize, size=size
        )
        self.text_sprite = TextSprite(parent, text)
        self.text_sprite.on_pressed = on_pressed
        self.padding = padding
        self.set_rect(0, 0, *size)

    def set_rect(self, x, y, w, h):
        """Set button geometry."""

        self.button_sprite.set_rect(x, y, w, h)
        text_size = (
            self.button_sprite.rect.w - self.padding[1] - self.padding[3],
            self.button_sprite.rect.h - self.padding[0] - self.padding[2],
        )
        text_pos = (
            self.button_sprite.rect.x + self.padding[3],
            self.button_sprite.rect.y + self.padding[0],
        )
        self.text_sprite.set_rect(*text_pos, *text_size)

    def get_rect(self):
        """Return geometry"""

        return self.button_sprite.rect

    def set_on_pressed(self, on_pressed):
        """Set on_pressed callback."""

        self.text_sprite.on_pressed = on_pressed

    def set_background_color(self, color):
        """Set button background color."""

        self.button_sprite.colorize = True
        self.button_sprite.set_color(color)

    def set_padding(self, padding):
        """Set padding around text.

        Parameters:
        padding: tuple of 4 integers (top, right, bottom, left)
        """

        self.padding = padding
        self.set_rect(
            self.get_rect().x, self.get_rect().y, self.get_rect().w, self.get_rect().h
        )
