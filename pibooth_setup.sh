#!/bin/bash

##########################
# WOLFOTO computer setup #
##########################
# 18/12/2024

# To run after raspberry pi os lite 64 installation
# Raspberry SD card configured using Imager :
#   - name of user (wolfoto)
#   - ssh enabled and authentication mode set
#   - wifi network can be configured
#   - hostname can be set to 'wolfotoxx' ()'xx' are 2 digits)
# To get access to gitlab :
#   - copy referenced ssh keys in Gitlab in folder ~/.ssh/ of Raspberry Pi machine

help() {
    echo "Usage: bash $0 [options]"
    echo "Options:"
    echo "  -b, --base_dir PATH  Path to base directory ending with '/' (default=/home/${USER}/pibooth/)"
    echo "  -p, --printer        Install printer packages (default=false)"
    echo "  -i, --install        Install packages (default=false)"
    echo "  -r, --relay          Install relay service to power on AP (default=false)"
    echo "  -t, --touchscreen    Apply patch to avoid touchscreen issues (default=false)"
    echo "  -h, --help           Display this help message"
    exit 0
}

echo "Wolfoto setup"
echo ---------------

# check ssh keys to access gitlab
if [[ ! -f /home/${USER}/.ssh/id_ed25519 ]]; then
    echo "Missing ssh key to access gitlab. Please copy the keys in folder ~/.ssh/ and try again."
    exit 1
fi

# default values
base_dir="/home/${USER}/pibooth/"
install_printer=false
path_to_pibooth_var="/home/${USER}/Documents/Pibooth/"
install_packages=false
relay=false  # install relay service to power on AP
touchscreen=false # apply patch to avoid touchscreen issues

# options
while [ -n "$1" ]; do
    case "$1" in
        -p|--printer)
            install_printer=true
            ;;
        -b|--base_dir)
            base_dir="$2"
            shift 1
            ;;
        -i|--install)
            install_packages=true
            ;;
        -r|--relay)
            relay=true
            ;;
        -t|--touchscreen)
            touchscreen=true
            ;;
        -h|--help)
            help
            ;;
        *)
            echo "Invalid option: $1"
            help
            ;;
    esac
    shift 1
done

path_to_venv="${base_dir}.venv/"

if [[ -f /proc/device-tree/model ]]; then
    rasp_version=$(cat /proc/device-tree/model | cut -d ' ' -f3)
    platform=raspberry
    echo "Platform: Raspberry Pi"
    echo "Raspberry Pi version: ${rasp_version}"
else
    echo "Platform: Not a Raspberry Pi"
    rasp_version="0"
    platform=other
fi


echo "Project directory : ${base_dir}"
echo "Path to virtual environment: ${path_to_venv}"
echo "Path to pibooth variables: ${path_to_pibooth_var}"
echo "Packages installation: ${install_packages}"
echo "Printer installation: ${install_printer}"
echo "Relay to power on AP enabled: ${relay}"
echo "Touchscreen patch enabled: ${touchscreen}"
echo ---------------

# packages installation
if [[ ${install_packages} = true ]]; then
    echo "System update"
    sudo apt update
    sudo apt -y upgrade
    echo ---------------

    ## Install more packages
    echo "Install more packages"
    sudo apt -y install git python3-pip gphoto2 libgphoto2-dev tmux vim
    if [[ "${platform}" = "raspberry" ]]; then
        sudo apt -y install lightdm openbox feh xterm fim unclutter lynx
    fi
    # install pibooth dependencies
    sudo apt install -y libsdl2-*

    # Following gphoto2 installation instructions (as described in pibooth installation instructions)
    # Doesn't work. Just keeping these for memory.
    # if dpkg -s gphoto2 > /dev/null; then
    #     echo "gphoto2 already installed"
    # else
    #     wget https://raw.githubusercontent.com/gonzalo/gphoto2-updater/master/gphoto2-updater.sh
    #     wget https://raw.githubusercontent.com/gonzalo/gphoto2-updater/master/.env
    #     chmod +x gphoto2-updater.sh
    #     sudo ./gphoto2-updater.sh
    # fi
    echo ---------------

    # Printer
    if [[ "${install_printer}" = "true" ]]; then
        cd || exit 1
        echo "Installing printer"
        
        # uncomment next line if classical printer
        # sudo apt install -y cups libcups2-dev printer-driver-gutenprint

        # If special printer (as Citizen CZ-01 or others, see list here : https://git.shaftnet.org/gitea/slp/selphy_print),
        # see instructions here : https://www.peachyphotos.com/blog/stories/building-modern-gutenprint/.
        # Other informations : https://forums.linuxmint.com/viewtopic.php?t=391775. Instructions are :
        # Remove existing gutenprint packages and ipp-usb
        sudo apt remove *gutenprint* ipp-usb
        # Install necessary development libraries
        sudo apt install libusb-1.0-0-dev libcups2-dev
        # Install CUPS (just in case)
        sudo apt install cups-daemon cups
        # Install git-lfs
        sudo apt install git-lfs
        # Download latest gutenprint snapshot from sourceforge
        GUTENPRINT_VER=5.3.4-2023-06-20T01-00-c236a7f7
        curl -o gutenprint-${GUTENPRINT_VER}.tar.xz "https://master.dl.sourceforge.net/project/gimp-print/snapshots/gutenprint-${GUTENPRINT_VER}.tar.xz?viasf=1"
        # Decompress & Extract
        tar -xJf gutenprint-${GUTENPRINT_VER}.tar.xz
        # Compile gutenprint
        cd gutenprint-${GUTENPRINT_VER} || exit
        ./configure --without-doc
        make -j4
        sudo make install
        cd ..
        # Refresh PPDs
        sudo cups-genppdupdate
        # Restart CUPS
        sudo service cups restart
        # Add lpadmin group to user
        sudo usermod -aG lpadmin "${USER}"

        # add software for Citizen cz-01 printer
        cd || exit 1
        # Get the latest selphy_print code
        git clone https://git.shaftnet.org/gitea/slp/selphy_print.git
        # Compile selphy_print
        cd selphy_print || exit 1
        make -j4 
        sudo make install
        # Set up library include path
        sudo sudo sh -c "echo '/usr/local/lib' > /etc/ld.so.conf.d/usr-local.conf"
        sudo ldconfig
        cd || exit 1
        echo ---------------
    fi
    # END Printer
else
    echo "Packages installation skipped"
    echo ---------------
fi

# project directory
if [[ -d "${base_dir}" ]]; then
    echo "Project directory <${base_dir}> already exists"
else
    mkdir "${base_dir}"
    echo "project directory <${base_dir}> created"
fi
echo ---------------

if [[ ! -d "${path_to_venv}" ]]; then
    # Create virtual environment
    echo "Create virtual environment"
    python3 -m venv "${path_to_venv}"
    echo "Virtual environment created"
    echo ---------------
else
    echo "Virtual environment already exists"
    echo ---------------
fi

# clone pibooth project
cd "${base_dir}" || exit 1
if [[ -d pibooth ]]; then
    echo "Pibooth project already cloned"
    echo ---------------
else
    echo "Clone pibooth project"
    git clone https://github.com/WolfaceIT/pibooth.git && echo "pibooth code downloaded"
    cd pibooth || exit 1
    git checkout -b 3.x origin/3.x && echo "pibooth 3.x branch downloaded"
    # activate virtual environment
    echo "Activate virtual environment"
    source "${path_to_venv}bin/activate"
    # install pibooth as editable
    if [[ "${install_printer}" = "true" ]]; then
        pip3 install -e .[dslr,printer]
        echo "Pibooth installed with [dslr,printer] option"
    else
        pip3 install -e .[dslr]
        echo "Pibooth installed with [dslr] option"
    fi

    # check Pillow version
    version=$(pip list | grep -i Pillow | tr -s ' ' | cut -d ' ' -f2 | cut -d '.' -f1)
    if (( version >= 10 )); then
        echo "La version de Pillow n'est pas adaptée à Pibooth"
        echo "Pillow version ${version} not suitable"
        echo "Uninstall Pillow"
        pip3 uninstall -y Pillow
        echo "Install Pillow 9.3.0"
        pip3 install 'Pillow==9.3.0'
    fi    
    deactivate
    echo ---------------
fi

# clone pibooth plugin
cd "${base_dir}" || exit 1
if [[ -d plugins ]]; then
    echo "pibooth plugins directory already exists"
    echo ---------------
else
    
    echo "clone pibooth plugin"
    echo "create pibooth plugins directory"
    mkdir -p plugins/
    cd plugins/ || exit 1

    echo "clone pibooth wolfoto plugin"
    git clone git@gitlab.com:wolface/wolfoto-project/pibooth-select-effect.git

    cd pibooth-select-effect || exit 1
    # activate virtual environment
    echo "Activate virtual environment"
    source "${path_to_venv}bin/activate"

    # install python modules
    echo "Install python modules required"
    pip3 install -r requirements.txt

    if [[ ${rasp_version} = '5' ]]; then
        echo "Install python modules required by Raspberry Pi 5"
        pip install -r requirements_raspPi5.txt
    fi

    deactivate

    # create folders for wolfoto plugin
    # background for openbox and app
    mkdir -p "${path_to_pibooth_var}"{data/{diffusion/{images,local},web},assets}
    echo "folders for wolfoto plugin created"
    cp "assets/background_dist.png" "/home/${USER}/Documents/Pibooth/assets/background.png"
    echo "background for openbox and app copied"
    echo ---------------
fi

# Configuration to set background for raspberry
if [[ "${platform}" = "raspberry" && ! -d "/home/${USER}/.config/openbox" ]]; then
    cd || exit 1
    echo "Set background screen by creating .config/openbox/autostart file"
    mkdir -p /home/"${USER}"/.config/openbox

cat <<EOF > /home/"${USER}"/.config/openbox/autostart
    ### hide cursor in openbox when 'fullscreen' option in pibooth config file doesn't work
    # unclutter -idle 0 &
    ### set background screen
    feh --bg-scale /home/${USER}/Documents/Pibooth/assets/background.png
    ### app is launched by systemd service 'wolfoto_run_pibooth'
EOF
    echo ---------------
fi

# configure wolfoto_run_pibooth service
if [[ ! -f /etc/systemd/system/wolfoto_run_pibooth.timer ]]; then
    echo "Configure wolfoto_run_pibooth service"
    sudo cp "${base_dir}plugins/pibooth-select-effect/utils/wolfoto_run_pibooth.timer" "/etc/systemd/system/wolfoto_run_pibooth.timer"
    sudo cp "${base_dir}plugins/pibooth-select-effect/utils/wolfoto_run_pibooth.service" "/etc/systemd/system/wolfoto_run_pibooth.service.origin"
    cp "${base_dir}plugins/pibooth-select-effect/utils/run_pibooth.sh" "/home/${USER}/Documents/Pibooth/data/run_pibooth.sh.origin"
    # modify script and service
    cat "/home/${USER}/Documents/Pibooth/data/run_pibooth.sh.origin" | sed "s#PATH/TO/ENVIRONMENT#home/${USER}/pibooth/.venv/bin#" | sed "s#PATH/TO/PIBOOTH/APP#home/${USER}/pibooth/pibooth#" | sed "s#PATH/TO/YOUR/HOME#home/${USER}#" > "/home/${USER}/Documents/Pibooth/data/run_pibooth.sh"
    chmod +x "/home/${USER}/Documents/Pibooth/data/run_pibooth.sh"
    sudo sh -c "cat /etc/systemd/system/wolfoto_run_pibooth.service.origin | sed "s/USERNAME/${USER}/" | sed "s#PATH/TO/SCRIPT#home/${USER}/Documents/Pibooth/data#" > /etc/systemd/system/wolfoto_run_pibooth.service"
    sudo systemctl daemon-reload
    sudo systemctl enable wolfoto_run_pibooth.timer
    echo ---------------
fi

# configure wolfoto_sync_diffusion service
if [[ ! -f /etc/systemd/system/wolfoto_sync_diffusion.timer ]]; then
    echo "Configure wolfoto_sync_diffusion service"
    sudo cp "${base_dir}plugins/pibooth-select-effect/utils/wolfoto_sync_diffusion.timer" "/etc/systemd/system/wolfoto_sync_diffusion.timer"
    sudo cp "${base_dir}plugins/pibooth-select-effect/utils/wolfoto_sync_diffusion.service" "/etc/systemd/system/wolfoto_sync_diffusion.service.origin"
    # modify script and service
    sudo sh -c "cat /etc/systemd/system/wolfoto_sync_diffusion.service.origin | sed "s/USERNAME/${USER}/" | sed "s#/PATH/TO/SCRIPT#/home/${USER}/pibooth#" | sed "s#/PATH/TO/PYTHON#${path_to_venv}bin#" > /etc/systemd/system/wolfoto_sync_diffusion.service"
    sudo systemctl daemon-reload
    sudo systemctl enable wolfoto_sync_diffusion.timer
    echo ---------------
fi

# configure wolfoto_post_waiting_photos service
if [[ ! -f /etc/systemd/system/wolfoto_post_waiting_photos.timer ]]; then
    echo "Configure wolfoto_post_waiting_photos service"
    sudo cp "${base_dir}plugins/pibooth-select-effect/utils/wolfoto_post_waiting_photos.timer" "/etc/systemd/system/wolfoto_post_waiting_photos.timer"
    sudo cp "${base_dir}plugins/pibooth-select-effect/utils/wolfoto_post_waiting_photos.service" "/etc/systemd/system/wolfoto_post_waiting_photos.service.origin"
    # modify script and service
    sudo sh -c "cat /etc/systemd/system/wolfoto_post_waiting_photos.service.origin | sed "s/USERNAME/${USER}/" | sed "s#/PATH/TO/SCRIPT#/home/${USER}/pibooth#" | sed "s#/PATH/TO/PYTHON#${path_to_venv}bin#" > /etc/systemd/system/wolfoto_post_waiting_photos.service"
    sudo systemctl daemon-reload
    sudo systemctl enable wolfoto_post_waiting_photos.timer
    echo ---------------
fi

# enabling relay service to power on AP
if [[ ${relay} = true ]]; then
    echo "Configure wolfoto_relay service on boot"
    sudo cp "${base_dir}plugins/pibooth-select-effect/utils/wolfoto_relay.timer" "/etc/systemd/system/wolfoto_relay.timer"
    sudo cp "${base_dir}plugins/pibooth-select-effect/utils/wolfoto_relay.service" "/etc/systemd/system/wolfoto_relay.service.origin"
    # modify script and service
    sudo sh -c "cat /etc/systemd/system/wolfoto_relay.service.origin | sed "s/USERNAME/${USER}/" | sed "s#/PATH/TO/SCRIPT#/home/${USER}/pibooth#" | sed "s#/PATH/TO/PYTHON#${path_to_venv}bin#" > /etc/systemd/system/wolfoto_relay.service"
    sudo systemctl daemon-reload
    sudo systemctl enable wolfoto_relay.timer
    echo ---------------
fi


# install a configuration file
if [[ ! -d /home/"${USER}"/.config/pibooth/ ]]; then
    echo "Create pibooth configuration directory"
    mkdir -p /home/"${USER}"/.config/pibooth/
    if [[ ! -f /home/"${USER}"/.config/pibooth/pibooth.cfg ]]; then
        echo "Copy pibooth configuration template to ~/.config/pibooth/pibooth.cfg. You have to customize it"
        cp "${base_dir}plugins/pibooth-select-effect/utils/pibooth.cfg" "/home/${USER}/.config/pibooth/"
        # set explicit paths for local user
        sed -i "s#~#/home/${USER}#g" "/home/${USER}/.config/pibooth/pibooth.cfg"
    fi
    echo ---------------
fi

# Copy default touch screen message for diaporama
cp "${base_dir}plugins/pibooth-select-effect/assets/touch_screen_msg.png" "/home/${USER}/Documents/Pibooth/assets/"

# Autologin and boot configuration
if [[ ${rasp_version} = '4' ]]; then
    echo "Configuration for Raspberry Pi4"
    echo "Configure autologin"
    if [ -e /etc/init.d/lightdm ]; then
    sudo systemctl --quiet set-default graphical.target
    sudo sh -c "cat > /etc/systemd/system/getty@tty1.service.d/autologin.conf << EOF
    [Service]
    ExecStart=
    ExecStart=-/sbin/agetty --autologin wolfoto --noclear %I \$TERM
    EOF"
    sudo sed /etc/lightdm/lightdm.conf -i -e "s/^\(#\|\)autologin-user=.*/autologin-user=wolfoto/"
    else
    whiptail --msgbox "Do 'sudo apt-get install lightdm' to allow configuration of boot to desktop" 20 60 2
    fi
    echo ---------------

    # Splash on boot
    echo "Set splash screen on boot"
    args=( splash quiet consoleblank=0 loglevel=1 logo.nologo )
    for arg in "${args[@]}"; do
        grep -q "$arg" /boot/cmdline.txt || sudo sed -i -E "s/^(.*)$/\1 $arg/" /boot/cmdline.txt
    done

    grep -q '^disable_splash=1$' /boot/config.txt || sudo sh -c "cat >> /boot/config.txt << EOF
    disable_splash=1
    EOF"
    echo ---------------

    # If needed, uncomment to hide decoration for pibooth window using openbox
    #grep -q WOLFOTO /home/${USER}/.config/openbox/rc.xml && echo "openbox configuration file already changed" || (sed -i '/<applications>/a <!-- WOLFOTO : no decoration for pibooth window -->\n\t<application class="pibooth">\n\t\t<decor>no</decor>\n\t</application>\n<!-- WOLFOTO END -->' /home/${USER}/.config/openbox/rc.xml && echo "openbox configuration file has been changed")

    # Optimize Raspberry configuration
    echo "Optimize Raspberry configuration"
    ## Disable audio
    sudo sed -i 's/^dtparam=audio=on/#&/' /boot/config.txt
    ## Set gpu memory to 256Mo
    sudo sed -i '/\[all\]/ a gpu_mem=256' /boot/config.txt
    echo ---------------
fi

# Configuration for raspberry pi5
if [[ ${rasp_version} = '5' ]]; then
    echo "Configuration for Raspberry Pi5"
    echo ---------------
    echo "Configure RTC"
    ## Set charging voltage
    grep -q rtc_bbat_vchg /boot/firmware/config.txt
    if [[ $? -eq 1 ]]; then
        sudo sed -i '/\[all\]/ a dtparam=rtc_bbat_vchg=3000000' /boot/firmware/config.txt
    fi
    echo ---------------

    # Optimize Raspberry configuration
    echo "Optimize Raspberry configuration"
    ## Disable audio
    sudo sed -i 's/^dtparam=audio=on/#&/' /boot/firmware/config.txt
    ## Set gpu memory to 256Mo
    grep -q 'gpu_mem=256' /boot/firmware/config.txt
    if [[ $? -eq 1 ]]; then
        sudo sed -i '/\[all\]/ a gpu_mem=256' /boot/firmware/config.txt
    fi
    echo --------------- 

    # configure wolfoto_set_wakeup_and_shutdown service
    if [[ ! -f /etc/systemd/system/wolfoto_set_wakeup_and_shutdown.timer ]]; then
        echo "Configure wolfoto_set_wakeup_and_shutdown service"
        sudo cp "${base_dir}plugins/pibooth-select-effect/utils/wolfoto_set_wakeup_and_shutdown.timer" "/etc/systemd/system/wolfoto_set_wakeup_and_shutdown.timer"
        sudo cp "${base_dir}plugins/pibooth-select-effect/utils/wolfoto_set_wakeup_and_shutdown.service" "/etc/systemd/system/wolfoto_set_wakeup_and_shutdown.service.origin"
        # modify script and service
        sudo sh -c "cat /etc/systemd/system/wolfoto_set_wakeup_and_shutdown.service.origin | sed "s/USERNAME/${USER}/" | sed "s#/PATH/TO/SCRIPT#/home/${USER}/pibooth#" | sed "s#/PATH/TO/PYTHON#${path_to_venv}bin#" > /etc/systemd/system/wolfoto_set_wakeup_and_shutdown.service"
        sudo systemctl daemon-reload
        sudo systemctl enable wolfoto_set_wakeup_and_shutdown.timer
        echo ---------------
    fi

fi

# clean service files
echo "Clearing service files"
sudo rm /etc/systemd/system/wolfoto_*.origin
echo ---------------

# Apply patch on Pibooth core
if [[ ${touchscreen} = true ]]; then
    echo "Applying touchscreen patch on Pibooth core"
    python "${base_dir}plugins/pibooth-select-effect/patchs/touchscreen_disable_menu_call.py" "${base_dir}pibooth/"
    echo ---------------
fi
