import pygame
import os
import math
import sys

import pibooth
from pibooth.view.pygame.sprites import BasePygameScene, ImageSprite, BaseSprite
from pibooth import evts
from pibooth.utils import LOGGER, PollingTimer
from pibooth.language import get_translated_text
from pibooth.pictures import sizing

from widgets import BorderSprite, ButtonSprite


EVT_PIBOOTH_NEXT = pygame.USEREVENT + 300
EVT_PIBOOTH_NEXT_CAPTURE = pygame.USEREVENT + 301
EVT_PIBOOTH_WELCOME = pygame.USEREVENT + 302
EVT_PIBOOTH_EXIT_1 = pygame.USEREVENT + 310
EVT_PIBOOTH_EXIT_2 = pygame.USEREVENT + 311

__version__ = "1.2.0"


class CameraMonitorScene(BasePygameScene):
    """Monitor camera"""

    def __init__(self):
        super().__init__()
        # use customized photo button if exists
        customized_photo_button = os.path.join(
            os.path.expanduser("~/Documents/Pibooth/"),
            "assets",
            "photo_btn.png",
        )
        if os.path.exists(customized_photo_button):
            photo_btn_path = customized_photo_button
        else:
            photo_btn_path = os.path.join(
                os.path.dirname(__file__), "assets", "photo_btn.png"
            )
        self.submit = ImageSprite(self, photo_btn_path, colorize=True)
        self.logo_place = ImageSprite(
            self,
            os.path.join(
                os.path.expanduser("~/Documents/Pibooth/"), "assets", "logo_place.png"
            ),
            colorize=False,
        )
        self.logo_photobooth = ImageSprite(
            self,
            os.path.join(os.path.dirname(__file__), "assets", "logo_photobooth.png"),
            colorize=True,
        )
        self.image_border = BorderSprite(self.image)

        self.submit.on_pressed = lambda: evts.post(EVT_PIBOOTH_NEXT)
        self.logo_place.on_pressed = lambda: evts.post(EVT_PIBOOTH_EXIT_1)
        self.logo_photobooth.on_pressed = lambda: evts.post(EVT_PIBOOTH_EXIT_2)

    def resize(self, size):
        """Resize text when window is resized."""

        self.status_bar.hide()
        # Preview capture
        self.image.set_rect(
            10 + self.image_border.thick,
            10 + self.image_border.thick,
            (self.rect.width * 3 // 4) - 2 * self.image_border.thick,
            self.rect.height - 20 - 2 * self.image_border.thick,
        )
        self.image_border.set_geometry()

        # Logos
        logo_place_size = (self.rect.height * 2 // 7, self.rect.height // 7)
        logo_place_pos = (self.rect.width - logo_place_size[0], 10)
        self.logo_place.set_rect(*logo_place_pos, *logo_place_size)
        logo_photobooth_size = (self.rect.width // 8, self.rect.width // 8)
        logo_photobooth_pos = (
            self.rect.width - logo_photobooth_size[0],
            self.rect.height - logo_photobooth_size[1],
        )
        self.logo_photobooth.set_rect(*logo_photobooth_pos, *logo_photobooth_size)
        # Submit button
        submit_size = (
            min(self.rect.width - self.image.rect.right, self.rect.height // 3),
            min(self.rect.width - self.image.rect.right, self.rect.height // 3),
        )
        submit_pos = (
            self.rect.width - self.rect.width // 8 - submit_size[0] // 2,
            self.rect.height // 2 - submit_size[1] // 2,
        )
        self.submit.set_rect(*submit_pos, *submit_size)
        self.submit.rect.centerx = (
            (self.rect.width - self.image.rect.right) // 2
        ) + self.image.rect.right


@pibooth.hookimpl
def pibooth_setup_states(machine):
    """Add a post-processing state"""
    machine.add_state("cameramonitor", CameraMonitorScene())


@pibooth.hookimpl(optionalhook=True)
def state_cameramonitor_enter(cfg, app, win):

    # set color of photo button
    photo_button_color = cfg.gettuple("WINDOW", "photo_button_color", int)
    color = True
    for value in photo_button_color:
        # if there is one negative value, photo button image is not colorized
        if value < 0 or value > 255:
            color = False
            break
    if color:
        win.scene.submit.set_color(photo_button_color)
    else:
        win.scene.submit.colorize = False

    # set color of photobooth logo
    logo_photobooth_color = cfg.gettuple("WINDOW", "logo_photobooth_color", int)
    win.scene.logo_photobooth.set_color(logo_photobooth_color)

    # clean previous picture recorded, necessary to reset capture.
    app.camera._captures = []
    # reset capture date to be updated on next capture
    app.capture_date = None
    # reset number of captures choice
    if len(app.capture_choices) > 1:
        app.capture_nbr = None

    LOGGER.info("WOLFOTO : Show preview")

    border = 100
    app.camera.preview(win.get_rect(absolute=True).inflate(-border, -border))
    try:
        app.timer.is_started()
    except:
        app.timer = PollingTimer(start=False)

    cfg.fx = "color"

    # using welcome screen or diaporama plugin
    if "welcome" in cfg.wolfstates or "diaporama" in cfg.wolfstates:
        # start timer
        app.timer_inaction.start(cfg.welcome_screen_delay)

    if cfg.getboolean("WINDOW", "image_border"):
        win.scene.image_border.enable()
        win.scene.image_border.set_color(
            cfg.gettuple("WINDOW", "image_border_color", int)
        )
        win.scene.image_border.thick = cfg.getint("WINDOW", "image_border_thick")
    else:
        win.scene.image_border.disable()


@pibooth.hookimpl(optionalhook=True)
def state_cameramonitor_do(cfg, app, win, events):
    """Display preview"""

    for event in events:
        if (
            event.type == pygame.KEYDOWN
            and event.key == pygame.K_q
            and pygame.key.get_mods() & pygame.KMOD_CTRL
        ):
            LOGGER.debug("WOLFOTO : [CTRL Q] pressed -> exit")
            sys.exit(0)

    if evts.find_event(events, EVT_PIBOOTH_EXIT_1) and evts.find_event(
        events, EVT_PIBOOTH_EXIT_2
    ):
        LOGGER.debug("WOLFOTO : 'Exit' fingers combination on touchscreen -> exit")
        sys.exit(0)

    event = evts.find_event(events, evts.EVT_PIBOOTH_CAM_PREVIEW)
    if event:
        win.scene.set_image(event.result)
        if cfg.getboolean("WINDOW", "image_border"):
            win.scene.image_border.set_geometry()
    if (
        cfg.getboolean("WINDOW", "preview_countdown")
        and len(app.capture_choices) == 1
        and app.capture_choices[0] == 1
    ):
        if not app.timer.is_started():
            if evts.find_event(events, EVT_PIBOOTH_NEXT):
                app.timer.start(cfg.getint("WINDOW", "preview_delay"))
                if "welcome" in cfg.wolfstates or "diaporama" in cfg.wolfstates:
                    # disable inaction timer
                    app.timer_inaction.reset()
        else:
            if app.timer.is_timeout():
                evts.post(EVT_PIBOOTH_NEXT_CAPTURE)
                app.timer.reset()
            elif app.timer.remaining() > 0.5:
                app.camera.set_overlay(math.ceil(app.timer.remaining()))
            elif app.timer.remaining() <= 0.5:
                app.camera.set_overlay(get_translated_text("smile"))
    else:
        if evts.find_event(events, EVT_PIBOOTH_NEXT):
            evts.post(EVT_PIBOOTH_NEXT_CAPTURE)
