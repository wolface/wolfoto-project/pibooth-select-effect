"""Synchronize local folder with wolfoto web server.
Download gallery's photos marked as local
"""

import io
from PIL import Image
import urllib.parse
import os
import shutil
from email.utils import parsedate_to_datetime
import configparser
import logging
from pathlib import Path
import argparse


from pibooth.utils import LOGGER, configure_logging
import server as ws


def get_photos_to_display(token_folder, url, username, password, gallery, max=None):
    """Request photos to be displayed list to server"""

    if max is None:
        # use 'max_photos_to_display' attribute of Gallery model
        url_diffusion_list = urllib.parse.urljoin(
            url, f"/api/photos_diffusion/?gallery={gallery}"
        )
    else:
        url_diffusion_list = urllib.parse.urljoin(
            url, f"/api/photos_diffusion/?gallery={gallery}&max={max}"
        )
    LOGGER.debug(f"WOLFOTO : Retrieving {url_diffusion_list} from API")
    data = ws.send_api_request(
        token_folder, url, url_diffusion_list, username, password
    )
    LOGGER.debug("WOLFOTO : Received data: %s", data)

    return data


def photo_download(
    token_folder, url, username, password, images_dir, photo, timestamp=None
):
    """Download photo in 'images_dir' folder.

    :param url: web server url
    :param images_dir: directory where to download photos
    :param photo: tuple (web_server_name,
                         new filename in diffusion kiosk folder which will be
                         more meaningful
                         )
    """

    url_download = f"/api/photo/{photo}/download/"
    data = ws.send_api_request(token_folder, url, url_download, username, password)
    if data:
        LOGGER.debug(
            "WOLFOTO : Headers received from %s: %s", url_download, data.headers
        )
        image = Image.open(io.BytesIO(data.content))
        file_path = os.path.join(images_dir, photo)
        image.save(file_path, "JPEG")

        if "Last-Modified" in data.headers:
            file_date = parsedate_to_datetime(data.headers["Last-Modified"])
            file_ts = file_date.timestamp()
            os.utime(file_path, (file_ts, file_ts))

        success = True
    else:
        success = False

    return success


def cp_from_local_to_diffusion_folder(src_folder, dst_folder):
    """Copy files from local folder to diffusion kiosk folder

    :param src_folder: local folder
    :param dst_folder: diffusion kiosk folder
    """

    file_list = [
        file
        for file in os.listdir(src_folder)
        if os.path.isfile(os.path.join(src_folder, file))
    ]
    for f in file_list:
        try:
            # copy photo in 'local' folder
            shutil.copy2(os.path.join(src_folder, f), dst_folder)
            LOGGER.info(f"WOLFOTO : file <{f}> copied to diffusion folder")
        except Exception as e:
            LOGGER.error(
                f"WOLFOTO : a problem occured when copying file <{f}> in diffusion local folder. Error is : {e}"
            )
        try:
            # remove photo in 'local' folder
            os.remove(os.path.join(src_folder, f))
            LOGGER.debug(f"WOLFOTO : file <{f}> removed from local folder")
        except Exception as e:
            LOGGER.error(
                f"WOLFOTO : a problem occured when removing file <{f}> in diffusion local folder. Error is : {e}"
            )


def get_updates_diffusion_folder(new, old):
    """Update diffusion kiosk folder with new photos or by removing old photos

    Parameters
        new: list of photos to display
        old: list of old photos to remove

    Returns
        items_to_remove: list of old photos to remove
        items_to_download: list of new photos to download
    """

    # remove photos no more present in the new list
    items_to_remove = set(old).difference(set(new))

    # download new items
    items_to_download = set(new).difference(set(old))

    return list(items_to_remove), list(items_to_download)


def sync_diffusion(data_dir, gallery, web_server_url, server_username, server_password):
    """Synchronize local folder with wolfoto web server.

    Parameters:
    data_dir (str): directory where to store data
    gallery (str): gallery name
    web_server_url (str): web server url
    server_username (str): web server username
    server_password (str): web server password
    """

    images_dir = os.path.join(
        data_dir, "diffusion/images/"
    )  # folder where to store photos
    local_dir = os.path.join(
        data_dir, "diffusion/local/"
    )  # folder where to store photos in case server is down

    # test folders exist
    if not os.path.isdir(images_dir):
        os.makedirs(images_dir)
    if not os.path.isdir(local_dir):
        os.makedirs(local_dir)

    # list of photos now in diffusion kiosk directory
    old = []
    old = os.listdir((images_dir))

    data = get_photos_to_display(
        data_dir,
        web_server_url,
        server_username,
        server_password,
        gallery,
    )
    if data is not None:
        new = [photo["filename"] for photo in data.json()]

        items_to_remove, items_to_download = get_updates_diffusion_folder(new, old)
        LOGGER.info(
            "WOLFOTO : We got %d pictures, %d to remove, %d to download",
            len(new),
            len(items_to_remove),
            len(items_to_download),
        )
        LOGGER.debug("WOLFOTO : Whole photo set: %s", new)

        # remove photos no more present in the new list
        for photo_filename in items_to_remove:
            try:
                os.remove(os.path.join(images_dir, photo_filename))
                LOGGER.info(
                    "WOLFOTO : <%s> removed from diffusion kiosk folder.",
                    photo_filename,
                )
            except Exception as e:
                LOGGER.error(
                    "WOLFOTO : Couldn't delete <%s>. Error is %s.", photo_filename, e
                )

        # download new photos
        for photo_filename in items_to_download:
            success = photo_download(
                data_dir,
                web_server_url,
                server_username,
                server_password,
                images_dir,
                photo_filename,
            )
            if success:
                LOGGER.info(
                    "WOLFOTO : <%s> copied to diffusion images directory.",
                    photo_filename,
                )
            else:
                LOGGER.error(
                    "WOLFOTO : Problem occured during download of <%s>.", photo_filename
                )

        # remove all the files in diffusion 'local' folder
        try:
            for f in os.listdir(local_dir):
                if os.path.isfile(os.path.join(local_dir, f)):
                    os.remove(os.path.join(local_dir, f))
        except FileNotFoundError:
            LOGGER.info("WOLFOTO : Path %s did not exist", local_dir)

    else:
        LOGGER.debug(
            "WOLFOTO : Synchronisation could not be done using web server. If photos exist in diffusion local folder, we copy them to be displayed."
        )
        cp_from_local_to_diffusion_folder(local_dir, images_dir)


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Sync photos with web server")

    group = parser.add_mutually_exclusive_group()
    group.add_argument(
        "-v",
        "--verbose",
        dest="logging",
        action="store_const",
        const=logging.DEBUG,
        help="report more information about operations",
        default=logging.INFO,
    )
    group.add_argument(
        "-q",
        "--quiet",
        dest="logging",
        action="store_const",
        const=logging.WARNING,
        help="report only errors and warnings",
        default=logging.INFO,
    )
    options = parser.parse_args()

    # configure logger
    # create writable logging file if not exists
    log_file = "/tmp/pibooth/sync_diffusion.log"
    # test file exists
    if not os.path.isfile(log_file):
        if not os.path.isdir("/tmp/pibooth"):
            os.mkdir("/tmp/pibooth")
        f = Path(log_file)
        os.umask(0o000)
        f.touch()

    msgfmt = "%(asctime)s [%(levelname)-8s] %(message)s"
    datefmt = "%Y-%m-%d %H:%M:%S"
    level = options.logging
    configure_logging(level=level, filename=log_file, msgfmt=msgfmt, datefmt=datefmt)

    # load configuration from pibooth.cfg
    config = configparser.ConfigParser()
    config.read(os.path.join(os.environ["HOME"], ".config/pibooth/pibooth.cfg"))
    web_server_url = config["SERVER"]["web_server_url"]
    server_username = config["SERVER"]["server_username"]
    server_password = config["SERVER"]["server_password"]
    gallery = config["SERVER"]["gallery"]

    # folder where to write data used in the app
    data_dir = os.path.expanduser("~/Documents/Pibooth/data")

    sync_diffusion(data_dir, gallery, web_server_url, server_username, server_password)
