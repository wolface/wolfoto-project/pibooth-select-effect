"""Script to send photos that were stored in local
because server was down.
Can be launched by cron every minute:
* * * * * /path/to/python/in/virtual/environment/python /path/to/script
"""

import os
import server
import configparser
import logging
from pathlib import Path
import argparse

from pibooth.utils import configure_logging


parser = argparse.ArgumentParser(description="Post waiting photos to web server")

group = parser.add_mutually_exclusive_group()
group.add_argument(
    "-v",
    "--verbose",
    dest="logging",
    action="store_const",
    const=logging.DEBUG,
    help="report more information about operations",
    default=logging.INFO,
)
group.add_argument(
    "-q",
    "--quiet",
    dest="logging",
    action="store_const",
    const=logging.WARNING,
    help="report only errors and warnings",
    default=logging.INFO,
)
options = parser.parse_args()

# configure logger
# create writable logging file if not exists
log_file = "/tmp/pibooth/post_waiting_photos.log"
# test file exists
if not os.path.isfile(log_file):
    if not os.path.isdir("/tmp/pibooth"):
        os.mkdir("/tmp/pibooth")
    f = Path(log_file)
    os.umask(0o000)
    f.touch()
msgfmt = "%(asctime)s [%(levelname)-8s] %(message)s"
datefmt = "%Y-%m-%d %H:%M:%S"
level = options.logging
configure_logging(level=level, filename=log_file, msgfmt=msgfmt, datefmt=datefmt)

# load configuration from pibooth.cfg
config = configparser.ConfigParser()
config.read(os.path.join(os.environ["HOME"], ".config/pibooth/pibooth.cfg"))
web_server_url = config["SERVER"]["web_server_url"]
server_username = config["SERVER"]["server_username"]
server_password = config["SERVER"]["server_password"]

# folder where to write data used in the app
data_dir = os.path.expanduser("~/Documents/Pibooth/data/")

server.post_waiting_photos(data_dir, web_server_url, server_username, server_password)
