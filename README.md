pibooth-select-effect
=

These are plugins for pibooth >= v3 to adapt pibooth core to wolfoto. Wolfoto is a photobooth which runs with a web application [wolfoto_web](https://gitlab.com/wolface/wolfoto_web) managing store and diffusion of photos. Diffusion application is developped as [wolfoto](https://gitlab.com/wolface/wolfoto) 'main_diffusion_kiosk.py'.

## wolfstart plugin

Must be used when using one of the wolfoto plugins.  
Manage the main sequences of wolfoto plugins.  
Must be declared at the end of `plugins` parameter in section `[GENERAL]` of pibooth configuration file. For example : 

```ini
[GENERAL]
plugins = ("/path/to/plugins/pibooth-select-effect/pibooth_wolfprint.py", "/path/to/plugins/pibooth-select-effect/pibooth_camera_monitor.py", "/path/to/plugins/pibooth-select-effect/pibooth_select_fx_and_publish_options.py", "/path/to/plugins/pibooth-select-effect/pibooth_qrcode.py", "/path/to/plugins/pibooth-select-effect/pibooth_wolfstart.py")
```

## camera_monitor plugin

**Mandatory except when using only diaporama plugin**  
Used to display camera preview.  
A submit button launches pibooth 'capture' state. This plugin skips 'choose' and 'chosen' state. The number of captures is set to 1.

### Customization

Photo button submit can be customized by storing a button image in `~/Documents/Pibooth/assets/photo_btn.png`. If this file doesn't exist, the image located in `pibooth-select-effect/assets/photo_btn.png` is used.

### Parameters

A border can be added to image preview. Parameters can be set in configuration file:

```ini
[WINDOW]
# image border enable
image_border = False

# image border color
image_border_color = (255, 255, 255)

# image border thick
image_border_thick = 10
```

## select_fx_and_publish_options plugin

Implements a custom state, allowing to choose a filter to apply on each capture, tampering with the Picture Factory and to choose how user wants to publish captured photo ( local and/or web). Name's state is 'chooseoptions'

It uses "global" variables :

- `cfg.fx` to track the fx state choosen by user.
- `cfg.publish_local` to publish in local place.
- `cfg.publish_web` to be able for user to download photo from web server.  

When local or web option is checked, the photo is sent to server with associated data.  
Associated data are :

- `code` : (str), a shortcode to allow user to easily access his photo on web. Shortcodes are generated locally. A file stores already used shortcodes locally. It is updated each time a photo has to be sent to server.
- `datetime` : (str formated as yyyy-mm-ddThh:mm:ss), capture's datetime
- `filename` : (str), an unique filename is locally generated using 'uuid' module.
- `place` : (str), the place where is installed photobooth.
- `gallery` : (str), the name of the gallery associated with this photobooth.
- `local` : ('0'|'1'), indicates if the photo has to be displayed in the place.
- `web` : ('0'|'1'), indicates if user wants to download the captured photo. In this case, a qrcode is generated to allow to download it.
- `public` : ('0'|'1'), not used yet but mandatory for web API. This could later allow  user to choose his photo to be public or private on web.

### user interface

Image 'choose_options_menu_background.png' in assets is used to set background to menu options. This background can be colorized. Some parameters to customize interface in 'pibooth.cfg':

```ini
[WINDOW]

# color of menu options text
menu_options_text_color = (51, 51, 51)

# color of menu options background
menu_options_background_color = (182, 182, 182)
```

### Watermark

A watermark can be added to photo when there is no built picture (photo is alone and `[GENERAL], built_picture` parameter set to False). A watermark image must be stored in `~/Documents/Pibooth/assets/watermark.png` and size must be adapted to captured image size (it is just pasted on captured image). 

### Parameters in pibooth.cfg

Texts of diffusion choice screen can be customised in `~/.config/pibooth.cfg` :

```ini
[CAPTURE_KIOSK]
# 1st part of text to display for diffusion choice
diffusion_text_1 = Voulez-vous :

# 2nd part of text to display for diffusion choice
diffusion_text_local = vous afficher dans ce lieu ?

# 3rd part of text to display for diffusion choice
diffusion_text_web = récupérer la photo sur le web ?
```

If one of these parameters doesn't exist, default one is used (`"Voulez-vous :", "vous afficher dans ce lieu ?", "récupérer la photo sur le web ?"`).  
If one of these parameters is empty (`""`), the option is hidden.

```ini
[CAPTURE_KIOSK]
# default check to select the local diffusion option (int, 1 or 0)
local_check = 1

# default check to select the web diffusion option (int, 1 or 0)
web_check = 0
```

If one of these parameters doesn't exist, default one is used (`local_check = 1; web_check = 0`). 

## external_trigger_arduino plugin

Enables external trigger using an Arduino board. New parameters in configuration file :

```ini
[CONTROLS]

# Arduino board connection port
arduino_port = /dev/ttyACM0

# Physical arduino IN pin to trigger capture
arduino_external_trigger_pin = 2

# Arduino digital input's idle level
arduino_in_idle_level = True
```

An electronic schematic of Arduino connection is in 'hardware' folder.

## wolfprint plugin

Add a screen allowing to choose to print picture or not.  
IMPORTANT : The plugin must be called first in `plugins` pibooth configuration file parameter or, at least, before 'pibooth_select_fx_and_publish_options.py'.

## welcome plugin

When inactive, display a welcome screen without camera monitoring.
This plugin must be declared after camera_monitor plugin :

```ini
[GENERAL]

# Path to custom plugin(s) not installed with pip (list of quoted paths accepted)
plugins = ("/path/to/plugins/pibooth-select-effect/pibooth_wolfprint.py", "/path/to/plugins/pibooth-select-effect/pibooth_camera_monitor.py", "/path/to/plugins/pibooth-select-effect/pibooth_welcome.py")
```

one more parameter in configuration file :

```ini
[GENERAL]

# Delay of inaction before displaying welcome screen in seconds
welcome_screen_delay = 5

```

## customized footer plugin

You can customize the footer of built picture using current date and time and a text whose parameter is set in pibooth config file. The parameters `footer_text1` and `footer_text2` are not used. A new parameter is set :

```ini
[PICTURE]
# Customized footer text displayed
wolf_footer_text = 'Chez Wolfoto'
```

Add path to the plugin to `plugins` parameter in pibooth configuration file : `"/path/to/plugins/pibooth-select-effect/pibooth_customized_footer.py"`.  
To install locale language if necessary: `sudo dpkg-reconfigure locales`. Choose language and reboot. If the language is not updated, check `/etc/default/locale` file. If needed: `sudo bash -c 'echo "LANG=fr_FR.UTF-8" > /etc/default/locale'` for example for french language.

## diaporama plugin

Plugin is named 'pibooth_diaporama.py'
A diaporama can be displayed on capture kiosk screen in fullscreen.  
A message can also be displayed over diaporama to invite to touch the screen and take a capture. Message image must exist and be located at `~/Documents/Pibooth/assets/touch_screen_msg.png`.  

### pibooth configuration file parameters

Parameters:

- `interval_time`: pause time between 2 images (int or float) in seconds
- `interval_time_new` : diaporama: pause time for new image (int or float) in seconds
- `diapo_msg_tempo`: diaporama message display tempo tuple (show_time_in_seconds, hide_time_in_seconds)
- `transition_mode`: you can choose type of transition: `cut` (default) or `fade`. If `fade` mode, you can set the transitions duration.
- `transition_duration`: duration of transition when in `fade` mode.

```ini
[DIFFUSION_KIOSK]
# diffusion kiosk name
# Required by 'select_fx_and_publish_options' and 'diaporama' plugins
# diffusion_server = wolfotodiffusion.local
diffusion_server = localhost  # capture and diffusion are on the same machine

# diffusion kiosk's username
# Required by 'select_fx_and_publish_options' and 'diaporama' plugins
diffusion_user = username

# local folder where to copy photos when web server is down
# Required by 'select_fx_and_publish_options' and 'diaporama' plugins
diffusion_local_folder_path = /home/user/Documents/Pibooth/data/diffusion/local/

# interval_time
# pause time between 2 images (int or float)
interval_time = 1

# diaporama: pause time for new image (int or float)
interval_time_new = 10

# diaporama message display tempo (show_time_in_seconds, hide_time_in_seconds)
diapo_msg_tempo = (3, 5)

# transition mode ['cut' | 'fade'] ('cut' by default)
transition_mode = fade

# transition duration (in seconds) (2 by default)
transition_duration = 0.5
```

Time spent in 'cameramonitor' state can be set using `welcome_screen_delay` parameter ([GENERAL] section).

```ini
[GENERAL]

# Delay of inaction before displaying welcome screen in seconds
welcome_screen_delay = 60
```

### diaporama with no camera or webcam connected

You must, in addition, add 'pibooth_stillimage_camera.py' plugin:

```bash
# diaporama + stillimage (to use when no camera is connected)
plugins = ("~/pibooth/plugins/pibooth-select-effect/pibooth_stillimage_camera.py", "~/pibooth/plugins/pibooth-select-effect/pibooth_diaporama.py", "~/pibooth/plugins/pibooth-select-effect/pibooth_wolfstart.py")
```

and set `disable_camera_setup` to True in configuration file:

```ini
[CAMERA]
# Disable camera setup
disable_camera_setup = True
```

### Images to display folder synchronization

Script `sync_diffusion.py` is used to synchronize diffusion with server's data. It requests server for images to display and synchronize local folder `~/Documents/Pibooth/data/diffusion/images`.  Use cron to launch this script. If you want to run this every 20 seconds, you can edit crontab, `crontab -e` :
```bash
* * * * * /usr/bin/python /your/file/path/sync_diffusion.py
* * * * * sleep 20; /usr/bin/python /your/file/path/sync_diffusion.py
* * * * * sleep 40; /usr/bin/python /your/file/path/sync_diffusion.py
```

When server is down, capure kiosk copy photo image in local folder `~/Documents/Pibooth/data/diffusion/local`. Afterwards, script `sync_diffusion.py` moves image in diffusion folder.

### Using only diaporama function on a Raspberry Pi without attached camera

You can use only diaporama function on a Raspberry without attached camera by creating a dummy camera device. For this, you can use `pibooth_stillimage_camera` plugin. To enable this, you must declare plugins in configuration file and set `disable_camera_setup` true:

```ini
[GENERAL]
plugins = ("/path/to/plugins/pibooth-select-effect/pibooth_stillimage_camera.py", "/path/to/plugins/pibooth-select-effect/pibooth_diaporama.py", "/path/to/plugins/pibooth-select-effect/pibooth_wolfstart.py")

[CAMERA]
# Disable camera setup
disable_camera_setup = True
```
### Troubleshoot in fullscreen mode

To be able to use `fullscreen` mode with `diaporama`plugin, you must set `diaporama_size` parameter.

```ini
[DIFFUSION_KIOSK]
# diaporama: screen size when WINDOW size in config file is fullscreen
diaporama_size = (1920, 1080)
```


## configuration file

Configuration file is located at `~/.config/pibooth/pibooth.cfg`.

### Template

If necessary, a template is given in 'utils' folder. You can copy it in `~/.config/pibooth`. All installation's specific parameters must be updated (web server url and credentials, diffusion parameters, path to plugins...).

### General section

Set paths to plugins :

```ini
[GENERAL]
# Path to custom plugin(s) not installed with pip (list of quoted paths accepted)
plugins = ("/path/to/pibooth-select-effect/pibooth_camera_monitor.py", "/path/to/pibooth-select-effect/pibooth_select_fx_and_publish_options.py", "/path/to/pibooth-select-effect/pibooth_qrcode.py", "/path/to/pibooth-select-effect/pibooth_external_trigger_arduino.py")
```

### Window section 

Better use `assets/Montserrat-Regular.ttf` for text font and black color for text.

```ini
[WINDOW]
# Background RGB color or image path
background = /path/to/pibooth-select-effect/assets/old-wall-background.jpg

# Font name or file path used for app texts
font = /path/to/pibooth-select-effect/assets/Montserrat-Regular.ttf

# Text RGB color
text_color = (0, 0, 0)

# How long is the preview in seconds
preview_delay = 4

# Show a countdown timer during the preview
preview_countdown = True
```

Better remove or limit delays:

```ini
[WINDOW]
# Animate the last taken picture by displaying captures one by one
animate = False

# How long is displayed the capture in seconds before switching to the next one
animate_delay = 0.0

# On 'finish' state: how long is displayed the final picture in seconds (0 if never shown)
finish_picture_delay = 0

# On 'wait' state: how long is displayed the final picture in seconds before being hidden (-1 if never hidden)
wait_picture_delay = 0

# How long is displayed the 'chosen' state:  (0 if never shown)
chosen_delay = 1
```

### Picture section

Multiple captures can be done.

```ini
[PICTURE]
# Possible choice(s) of captures numbers (numbers between 1 to 4)
captures = (1, 4)
```

If `captures` length list is more than one, user can choose the number of captures. If `captures` length list is one, this will be the number of captures to be done.

```ini
[PICTURE]
# Possible choice(s) of captures numbers (numbers between 1 to 4)
captures = (4)  # 4 captures to be done
```

### New parameters : 

To be used with [wolfoto_web](https://gitlab.com/wolface/wolfoto_web) application.

```ini
[SERVER]
# Base URL of server running wolfoto_web application
web_server_url = http://localhost:8000/

# Username to get access to server
server_username = username

# Password to get access to server
server_password = password

# Number of characters of shortcode
shortcode_len = 3

# The place where is installed photobooth
place = place_name

# Gallery's name of this photobooth
gallery = gallery_name
```

## Customisation

You can display special image (for example, the logo of place) at the right top of screen. A transparent image is stored at `~/Documents/Pibooth/assets/logo_place.png`. You can customise this file keeping his geometry.  
The image of the background screen when the application is not running is stored in `~/Documents/Pibooth/assets/background.png`. This image can be customised keeping his geometry.
'photo!' button in camera monitor state can also be customised by storing an image as `~/Documents/Pibooth/assets/photo_btn.png`. By default, this image is colorized. Color can be modified by parameter `photo_button_color` in `[WINDOW]` section of configuration file. if there is one value not in range [0, 255], photo button image is not colorized.

## When server is down

### Synchronisation between capture kiosk and server

When server is down, the captured photos are stored in 'data/web' folder. A script called 'post_waiting_photos.py' must be launched by cron service to update server when it is up. You can edit crontab (`crontab -e`) and add this line :  
`* * * * * /path/to/python/in/virtual/environment/python /path/to/post_waiting_photos.py` . The script will run every minute.

### Copy of captured photo to local folder of diffusion kiosk

When server is down, captured photo in 'data/web' folder is copied to 'diffusion/local' folder of diffusion kiosk to be displayed later. It needs new parameters in pibooth config file :

```ini
[DIFFUSION_KIOSK]
# diffusion kiosk name
diffusion_server = name_in_local_network

# diffusion kiosk's username
diffusion_user = user

# local folder where to copy photos when web server is down
diffusion_local_folder_path = absolute_path_to_local_folder
```

It uses `scp` to copy file, so you must generate ssh key if you haven't one and copy it to diffusion kiosk.

```bash
ssh-keygen -t ed25519
ssh-copy-id user@ip_adress
```

## Troubleshoots

### Pibooth fullscreen mode

Some screens are black at launch of the pibooth application. For touchscreen, to remedy, sometimes you can :

- Touch the screen
- Hit ESC 2 times to enter and exit configuration file editor

An other approach is to modify one line in a pibooth application's file `pibooth/pibooth/view/pygame/window.py` (line 41 at the moment):  

Replace

```python
self.surface = pygame.display.set_mode(self._size, pygame.RESIZABLE)
```

by

```python
self.surface = pygame.display.set_mode(self._size, pygame.NOFRAME)
```

[You can apply the patch 'patchs/noframe_mode.py']  

and set the size of the screen in pibooth configuration file. For example :

```ini
[WINDOW]
# The (width, height) of the display window or 'fullscreen'
size = (1280, 720)
```

### Touchscreen bug

Touching screen with more than 3 fingers opens configuration menu. This was processed by calling the function 'pibooth.evts.is_fingers_event()'. This sometimes crashes the app even when we use just one finger to touch screen. We don't use this feature in wolfoto app. So we can remove the call to this function.

Replace in view/pygame/windows.py, l. 206

```python
elif ((event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE) or evts.is_fingers_event(event, 4))
```

by
```python
elif ((event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE))
```

[You can apply the patch 'patchs/touchscreen_disable_menu_call.py']

### Printers

For support of some dye sublimation photo printers, see here :  
[Supported dye sublimation photo printers](https://git.shaftnet.org/gitea/slp/selphy_print)  
[Instructions](https://www.peachyphotos.com/blog/stories/building-modern-gutenprint/)  
[More informations](https://forums.linuxmint.com/viewtopic.php?t=391775)  
Configure cups at this URL : http://localhost:631/admin .  
Some options (set default option menu) : 
- Media Size : 4*6
- Shrink Page if Necessary to Fit Borders : Shrink (print the whole page)

You can use the lightweigth browser lynx to access print configuration : `sudo apt install lynx`.  
User has to be member of lpadmin group : `sudo usermod -aG lpadmin ${USER}`.  
To access configuration : `lynx localhost:631`.  

For printer Citizen-CZ01, you have to use Citizen-CZ01 driver. No other Citizen driver works. You can look at `/etc/cups/ppd/CITIZEN_SYSTEMS_CZ-01.ppd` and verify lines 
```bash
*Manufacturer:  "Citizen"
*Product:       "(Citizen CZ-01)"
```

### Exit application

In 'camera preview' state, you can exit application with keyboard shortcut `CTRL + q` or using touchscreen : touch both wolfoto logo (bottom right of screen) and place logo (top right of screen).  

## Synchronization wolfoto, wolfoto_web, pibooth and pibooth plugin applications  

| Date | wolfoto app | wolfoto_web app | pibooth | pibooth-select-effect |
|------|-------------|-----------------|---------|-----------------------|
|2023-03-24| v2.1 | v0.1 | | |
|2023-03-27| v2.2 | v0.2 | | |
|2023-07-25| v3.0 | v1.0 | test-camera commit 7aaa0e0 | v1.0 |
|2023-09-04| v3.1 | v1.1 | test-camera commit 7aaa0e0 | v1.1 |
|2024-01-30| v3.2 | v1.1 | test-camera commit 7aaa0e0 | v2.0 |
|2024-06-13| v4.0 | v2.0 | test-camera commit 7aaa0e0 | v3.0 |

## Utilities

### Scripts

#### Post waiting photos

When server is down, photos are stored in '~/Documents/Pibooth/data/web/' and metadata are stored in '~/Documents/Pibooth/data/post_waiting_list.yaml. To send photos to web server : `python post_waiting_photos.py [-v | -q]`. Use `-v` or `-q` options to get more or less verbosity.

#### Synchronize diffusion

When displaying photos, you must use `python sync_diffusion.py [-v | -q]` to get in sync with photos used for diffusion in wolfoto web server. Use `-v` or `-q` options to get more or less verbosity. Photos are downloaded in `~/Documents/Pibooth/data/diffusion/images/`.

#### Run scripts as systemd services

To launch scripts periodically, we can use systemd. For example, to launch post waiting photos every 10 seconds:

- create service `/etc/systemd/system/wolfoto_post_waiting_photos.service`:
```bash
[Unit]
Description=Run wolfoto post waiting photos script

[Service]
Type=simple
Environment="HOME=/home/your_username"
ExecStart=/full/path/to/interpreter/bin/python /full/path/to/script/post_waiting_photos.py

[Install]
WantedBy=graphical.target
```
- create timer `/etc/systemd/system/wolfoto_post_waiting_photos.timer`
```bash
[Unit]
Description=Run wolfoto post waiting photos script every 10 s

[Timer]
OnBootSec=3min
AccuracySec=1s
OnUnitActiveSec=10s

[Install]
WantedBy=timers.target
```
- start timer : `sudo systemctl start wolfoto_post_waiting_photos.timer`
- you can enable timer : `sudo systemctl enable wolfoto_post_waiting_photos.timer` to be started at next session

Templates are given in folder `utils/`. You can copy them in `/etc/systemd/system/`, update installation's specific parameters and enable them.

#### Using RTC capabilities on Raspberry pi5

##### Charge configuration:

To charge the battery at a set voltage, add rtc_bbat_vchg to /boot/firmware/config.txt:

```ini
dtparam=rtc_bbat_vchg=3000000
```

This is processed by pibooth_setup.sh script.

##### Set shutdown and wakeup times

New parameter in `~/.config/pibooth.cfg`:

```ini
# Alarm list, times to shutdown and wakeup raspberry pi5 using rtc (isoformat)
# list of tuples (time_to_shutdown, time_to_wakeup)
inactivity_times = [("23:00:00", "00:10:00"), ("16:00:00", "19:00:00")]
```

The script `set_wakeup_and_shutdown.py` processes time to schedule wakeup, modifies crontab to run bash script which sets wakeup alarm and halts Raspberry Pi. 

###### Launch script using crontab

It can be launched at each boot using crontab:

```bash
@reboot /PATH/TO/PYTHON/python /PATH/TO/SCRIPT/plugins/pibooth-select-effect/set_wakeup_and_shutdown.py
```

###### Launch script using systemd service

Service and timer are given in 'utils' folder. You can customize the service, copy them in '/etc/systemd/system' folder and, finally, enable timer.  See 'Run scripts as systemd services' above for more details.

#### Setup script

`pibooth_setup.sh` script is used to prepare sd card for Raspberry installations.  
First, you need to install 'Raspberry Pi OS Lite 64' on sd card. Debian Bookworm is working. On Ubuntu, you can use 'Raspberry Pi Imager'. Configuration parameters to set:

- General
    - hostname : wolfotoxx
    - username : wolfoto
    - password
    - wifi configuration if known
        - SSID
        - password
        - wifi country
    - local setup
- Services
    - activate ssh. Choose authentication mode.

Boot Raspberry card with the sd card. You can have acces to Raspberry using ssh. Launch `pibooth_setup.sh` script using arguments:

```bash
"Usage: bash pibooth_setup.sh [options]"
"Options:"
"  -b, --base_dir PATH  Path to base directory ending with '/' (default=/home/${USER}/pibooth/)"
"  -p, --printer        Install printer packages (default=false)"
"  -i, --install        Install packages (default=false)"
"  -r, --relay          Install relay service to power on AP (default=false)"
"  -t, --touchscreen    Apply patch to avoid touchscreen issues (default=false)"
"  -h, --help           Display this help message"
```

#### Launch pibooth at boot

Different methods can be used:

##### Systemd service

Use `utils/run_pibooth.sh` script as a base. Adapt it with your configuration. Use systemd to launch the script. Copy scripts `utils/wolfoto_run_pibooth.service` and `utils/wolfoto_run_pibooth.timer` in `/etc/systemd/system/` folder and enable timer : `sudo systemctl enable wolfoto_run_pibooth.timer`.

##### openbox autostart

If you use openbox as your window manager, you can use `.config/openbox/autostart` script. When you don't use python virtual environment, add just `pibooth` at the end of the script.

## Tips

### Using relay to power on AP

Using this type of relay : [5V relay](https://www.kubii.com/fr/modules-relais/1969-module-1-relais-avec-couplage-optique-5v-kubii-3272496008250.html) 

#### Using GPIO of raspberry

- Raspberry Pi 5: install [rpi-lgpio](https://github.com/waveform80/rpi-lgpio) : `pip install rpi-lgpio`.
- Raspberry Pi 4: install [RPi.GPIO] : `pip install RPi.GPIO`.

Connections between Raspberry Pi and relay:

- GND relay = GND pi
- Vcc relay = 5V pi
- cmd relay = GPIO 24 (Pin 18)

Two scripts to control relay: `relay_off_raspPi.py` and `relay_on_raspPi.py`. 
To switch on relay on boot, edit crontab:

```bash
@reboot /home/wolfoto/pibooth/.venv/bin/python /home/wolfoto/pibooth/plugins/pibooth-select-effect/relay_on_raspPi.py
```

#### With a raspberry pi4 and pijuice

Connecting directly the relay to raspberry pinouts.  
Connections :

- GND relay = GND pi
- Vcc relay = 5V pi
- cmd relay = GND relay

220V plug is connected to pins switch open at idle state (when there is no power to GND-5V inputs).

#### Without a raspberry

We're using an Arduino card and script 'relais_AP_arduino.py'. There is a conflict between external trigger and Arduino driven relay for the moment. We must disable external trigger plugin.  
Relay command is connected to pin 7. 220V plug is connected to pins switch open at idle state (when there is no power to GND-5V inputs).  
TODO : using dedicated script to Arduino card and sharing data (socket, OSC, shared memory, ... ?)

### Using an external capture trigger with a Raspberry

'pibooth_external_trigger_raspberry.py' plugin allows to capture photo using a button directly connected to Raspberry.  
Add '/path/to/pibooth_external_trigger_raspberry.py' to `plugins` parameter in pibooth configuration file.  
New parameter in pibooth configuration file (pin is referenced as physical numbering. For example, pin 16 is GPIO23) :

```ini
[CONTROLS] 
# Physical raspberry IN pin to trigger capture
rasp_external_trigger_pin = 16
```

### Delete captures from camera internal memory

It is possible to avoid storing captures in camera internal memory **when not using printer plugin**.

```ini
[CAMERA]
# Delete captures from camera internal memory (when applicable)
delete_internal_memory = True
```

### Remove photo files generated by pibooth no more useful

Launch this script at boot using cron. It removes files in folder `~/Pictures/pibooth/`

```bash
@reboot /usr/bin/bash /path/to/pibooth-select-effect/pibooth_rm_photo_files.sh
```

### Watchdog

You can use `watchdog.sh` script to relaunch pibooth if needed. It launches pibooth a certain amount of trials (3 by default) and, if no success, reboot (maximum number: 2 by default). Using cron, testing every 2 minutes :

```bash
*/2 * * * * /usr/bin/bash /path/to/pibooth-select-effect/watchdog.sh
```

## Tests

Most of the tests use pytest module. Execute `pytest` in root directory.  
Some tests use 'wolfoto_web' server. Paths of wolfoto and wolfoto_web applications, wolfoto_web ip and port and path of python of wolfoto_web virtual environment are set in `tests/.env`.  
To pass tests, 'web_server_url' parameter in pibooth configuration file `~/.config/pibooth/pibooth.cfg` must be set to 'http://localhost:8000/'