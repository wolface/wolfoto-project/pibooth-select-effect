"""Patch 
Workaround to get full screen,
Problem was : fullscreen mode in configuration file doesn't work with all the monitors.
Workaround for these monitors : replace in view/pygame/windows.py, l. 41
'self.surface = pygame.display.set_mode(self._size, pygame.RESIZABLE)'
by
'self.surface = pygame.display.set_mode(self._size, pygame.NOFRAME)'
Option -r to go back to original file.

After application of this patch, you can set the size of the screen in pibooth configuration file. For example :

```ini
[WINDOW]
# The (width, height) of the display window or 'fullscreen'
size = (1280, 720)
```
"""


import argparse
import os


parser = argparse.ArgumentParser()
parser.add_argument("pibooth_root", help="pibooth root folder (where is the README)")
parser.add_argument("-r", "--reverse", action="store_true", help = "reverse patch and return to original file")
args = parser.parse_args()

path = os.path.join(args.pibooth_root, "pibooth/view/pygame/window.py")

with open(path, "r") as f:
    text = f.read()

if not args.reverse:

    count_search = text.count('self.surface = pygame.display.set_mode(self._size, pygame.RESIZABLE)')
    print(f"Number of found expressions to replace : {count_search}")
    if count_search > 2:
        print("Something has changed since last tested patch, there is more than 2 occurences. Get a look at the source code.")
    else:
        print("Replacing the first occurence.")
        new_text = text.replace(
            'self.surface = pygame.display.set_mode(self._size, pygame.RESIZABLE)',
            'self.surface = pygame.display.set_mode(self._size, pygame.NOFRAME)',
            1
            )
        with open(path, "w") as f:
            f.write(new_text)
        print("Done.")
        print("You can now set the size of the screen in pibooth configuration file, section [WINDOW]")

else:
    count_search = text.count('self.surface = pygame.display.set_mode(self._size, pygame.NOFRAME)')
    print(f"Number of found expressions to replace : {count_search}")
    if count_search == 0:
        print("No occurence to replace, please check your code.")
    else:
        print("Reversing to original file.")
        new_text = text.replace(
            'self.surface = pygame.display.set_mode(self._size, pygame.NOFRAME)',
            'self.surface = pygame.display.set_mode(self._size, pygame.RESIZABLE)',
            )
        with open(path, "w") as f:
            f.write(new_text)
        print("Done.") 
        print("You can now set the size of the screen in pibooth configuration file, section [WINDOW]") 
