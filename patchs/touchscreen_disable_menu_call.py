"""Patch
Workaround to rid off call of 'pibooth.evts.is_fingers_event()' which sometimes crashes pibooth app.
Calling this function allowed to open configuration menu when touching screen with more than 3 fingers.
We don't use this feature in wolfoto app. So we can disable it.
Replace in view/pygame/windows.py, l. 206
'elif ((event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE) or evts.is_fingers_event(event, 4))'
by
'elif ((event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE))'.
Option -r to go back to original file.
"""


import argparse
import os


parser = argparse.ArgumentParser()

parser.add_argument("pibooth_root", help="pibooth root folder (where is the README)")
parser.add_argument("-r", "--reverse", action="store_true", help="reverse patch and return to original file")
args = parser.parse_args()

path = os.path.join(args.pibooth_root, "pibooth/view/pygame/window.py")

with open(path, "r") as f:
    text = f.read()

if not args.reverse:

    count_search = text.count('elif ((event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE) or evts.is_fingers_event(event, 4))')
    print(f"Number of found expressions to replace : {count_search}")
    if count_search > 1:
        print("Something has changed since last tested patch, there is more than 1 occurence. Get a look at the source code.")
    else:
        print("Replacing the first occurence.")
        new_text = text.replace(
            'elif ((event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE) or evts.is_fingers_event(event, 4))',
            'elif ((event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE))',
            1
            )

        with open(path, "w") as f:
            f.write(new_text)
        print("Done.")
else:
    count_search = text.count('evts.is_fingers_event(event, 4)')
    if count_search >= 1:
        print("One occurence of text to revert has been found, please check you code.")
    else:
        count_search = text.count('elif ((event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE))')
        print(f"Number of found expressions to replace : {count_search}")
        if count_search > 1:
            print("Something has changed since last tested patch, there is more than 1 occurence. Get a look at the source code.")
        else:
            print("Reversing to original file.")
            new_text = text.replace(
                'elif ((event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE))',
                'elif ((event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE) or evts.is_fingers_event(event, 4))',
                1
                )

            with open(path, "w") as f:
                f.write(new_text)
            print("Done.")
