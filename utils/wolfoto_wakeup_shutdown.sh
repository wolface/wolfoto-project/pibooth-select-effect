# Script to set wakeup for raspberry pi5 and shutdown
# Using script https://forums.raspberrypi.com/viewtopic.php?t=364576#p2192040 from MisterGin

# set wakeup time in seconds
wakeup_time=$1

echo 0 | sudo tee /sys/class/rtc/rtc0/wakealarm >/dev/null
echo "+$wakeup_time" | sudo tee /sys/class/rtc/rtc0/wakealarm >/dev/null
sudo /usr/sbin/halt
