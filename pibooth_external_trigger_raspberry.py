"""Pibooth plugin which enables an external trigger connected to Raspberry board to start capture photo."""

import pygame
from gpiozero import Button

import pibooth
from pibooth import evts
from pibooth.utils import LOGGER


EVT_PIBOOTH_NEXT = pygame.USEREVENT + 300

__version__ = "1.0.0"


@pibooth.hookimpl
def pibooth_configure(cfg):
    cfg.add_option(
        "CONTROLS",
        "rasp_external_trigger_pin",
        16,
        "Physical raspberry IN pin to trigger capture",
    )


@pibooth.hookimpl
def pibooth_startup(cfg, app):
    app.external_trigger_rasp_btn = Button(
        "BOARD" + cfg.get("CONTROLS", "rasp_external_trigger_pin")
    )


@pibooth.hookimpl(optionalhook=True)
def state_cameramonitor_do(cfg, app, win, events):

    def send_capture_signal():
        evts.post(EVT_PIBOOTH_NEXT)
        LOGGER.info("WOLFOTO : Capture triggered by external button")

    app.external_trigger_rasp_btn.when_held = send_capture_signal
