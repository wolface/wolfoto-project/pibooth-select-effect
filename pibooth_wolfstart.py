import pygame
import os
import locale
import pathlib
import shutil
from PIL import Image

import pibooth
from pibooth.utils import LOGGER, PollingTimer
from pibooth import evts
from pibooth.view.pygame.sprites import ImageSprite, TextSprite

from wolf_utils import open_image


locale.setlocale(locale.LC_TIME, "")

__version__ = "1.0.0"

# This list links wolfoto's plugin filename to state name
WOLFSTATES = {
    "pibooth_welcome.py": "welcome",
    "pibooth_camera_monitor.py": "cameramonitor",
    "pibooth_select_fx_and_publish_options.py": "chooseoptions",
    "pibooth_qrcode.py": "qrcode",
    "pibooth_wolfprint.py": "wolfprint",
    "pibooth_diaporama.py": "diaporama",
}

EVT_PIBOOTH_NEXT = pygame.USEREVENT + 300
EVT_PIBOOTH_NEXT_CAPTURE = pygame.USEREVENT + 301
EVT_PIBOOTH_CANCEL = pygame.USEREVENT + 306

APP_DIR = os.path.dirname(__file__)


def print_picture(cfg, app):
    LOGGER.info("WOLFOTO : Send final picture to printer")
    app.printer.print_file(app.previous_picture_file)
    app.count.printed += 1
    app.count.remaining_duplicates -= 1


def create_mandatory_items(base_dir):
    """Create mandatory folders and files for the app

    Params:
    base_dir (str): path to the base directory of the wolfoto photobooth
    """

    # Create directories where pictures are saved
    data_dir = os.path.join(base_dir, "data")
    if not os.path.isdir(data_dir):
        os.makedirs(data_dir)
        os.mkdir(os.path.join(data_dir, "web"))

    # Create mandatory files to store data
    files = ["post_waiting_list.yaml", "shortcodes.yaml", "filenames.yaml"]
    for f in files:
        path = os.path.join(data_dir, f)
        if not os.path.isfile(path):
            open(path, "x").close()

    # Create mandatory assets
    # customizable asserts are copied in 'base_dir/assets' if they don't exist
    # create 'assets' directory
    assets_dir = pathlib.Path(base_dir, "assets")
    if not assets_dir.is_dir():
        assets_dir.mkdir()

    base_assets_dir = pathlib.Path(pathlib.Path(__file__).parent, "assets")
    base_assets = list(base_assets_dir.glob("*_dist.png"))

    for base_asset in base_assets:
        new_name = base_asset.name.removesuffix("_dist.png") + ".png"
        asset = pathlib.Path(assets_dir, new_name)
        if not asset.is_file():
            shutil.copyfile(base_asset, asset)

    # create directory to be used with 'diaporama' plugin
    diffusion_dir = pathlib.Path(data_dir, "diffusion")
    if not diffusion_dir.is_dir():
        diffusion_dir.mkdir()
        pathlib.Path(diffusion_dir, "local").mkdir()
        pathlib.Path(diffusion_dir, "images").mkdir()


@pibooth.hookimpl
def pibooth_configure(cfg):
    """Declare the new configuration options"""

    # web server parameters
    cfg.add_option(
        "SERVER",
        "web_server_url",
        "",
        "Base URL of server running wolfoto_web application",
    )
    cfg.add_option("SERVER", "server_username", "", "Username to get access to server")
    cfg.add_option("SERVER", "server_password", "", "Password to get access to server")
    cfg.add_option("SERVER", "shortcode_len", 3, "Number of characters of shortcode")
    cfg.add_option(
        "SERVER", "place", "wolfoto", "The place where is installed photobooth"
    )
    cfg.add_option(
        "SERVER", "gallery", "wolfoto-gallery", "Gallery's name of this photobooth"
    )

    # diffusion kiosk parameters
    cfg.add_option("DIFFUSION_KIOSK", "diffusion_server", "", "diffusion kiosk name")
    cfg.add_option(
        "DIFFUSION_KIOSK", "diffusion_user", "wolfoto", "diffusion kiosk's username"
    )
    cfg.add_option(
        "DIFFUSION_KIOSK",
        "diffusion_local_folder_path",
        "",
        "local folder where to copy photos when web server is down",
    )
    cfg.add_option(
        "DIFFUSION_KIOSK",
        "interval_time",
        3,
        "diaporama: pause time between 2 images (int or float) in seconds",
    )
    cfg.add_option(
        "DIFFUSION_KIOSK",
        "interval_time_new",
        20,
        "diaporama: pause time for new image (int or float) in seconds",
    )
    cfg.add_option(
        "DIFFUSION_KIOSK",
        "diapo_msg_tempo",
        (3, 20),
        "diaporama message display tempo (show_time, hide_time)",
    )
    cfg.add_option(
        "DIFFUSION_KIOSK",
        "transition_mode",
        "cut",
        "transition mode ('cut' by default)",
    )
    cfg.add_option(
        "DIFFUSION_KIOSK",
        "transition_duration",
        2,
        "transition duration (2 by default)",
    )

    # window parameters
    cfg.add_option(
        "WINDOW",
        "image_border",
        False,
        "image border enable",
    )
    cfg.add_option(
        "WINDOW",
        "image_border_color",
        (255, 255, 255),
        "image border color",
    )
    cfg.add_option(
        "WINDOW",
        "image_border_thick",
        12,
        "image border thick",
    )
    cfg.add_option(
        "WINDOW",
        "photo_button_color",
        (120, 120, 120),
        "color of 'photo' button",
    )
    cfg.add_option(
        "WINDOW",
        "button_background_color",
        (120, 120, 120),
        "background color of button",
    )
    cfg.add_option(
        "WINDOW",
        "logo_photobooth_color",
        (120, 120, 120),
        "color of photobooth logo",
    )


@pibooth.hookimpl
def pibooth_startup(cfg, app):

    # list of wolfoto plugins used
    cfg.wolfstates = []
    for ws in WOLFSTATES:
        if ws in cfg.getpath("GENERAL", "plugins"):
            cfg.wolfstates.append(WOLFSTATES[ws])

    # For now, error not using 'cameramonitor' or 'diaporama' plugin
    if "diaporama" not in cfg.wolfstates and "cameramonitor" not in cfg.wolfstates:
        raise RuntimeError(
            "Error : 'cameramonitor' or 'diaporama' plugin must be used with wolfoto plugins"
        )

    # init timer
    if "welcome" in cfg.wolfstates or "diaporama" in cfg.wolfstates:
        app.timer_inaction = PollingTimer()
        try:
            cfg.welcome_screen_delay = cfg.getint("GENERAL", "welcome_screen_delay")
        except KeyError:
            LOGGER.info(
                "WOLFOTO : [GENERAL,welcome_screen_delay] parameter \
                          unset in config file, default is 120s"
            )
            cfg.welcome_screen_delay = 120

    # test [GENERAL, built_picture] parameter.
    # this parameter allows to use built picture on web server
    try:
        cfg.wolf_built_picture = cfg.getboolean("GENERAL", "built_picture")
    except KeyError:
        LOGGER.info(
            "WOLFOTO : [GENERAL, built_picture] parameter\
            unset in config file, default is False"
        )
        cfg.wolf_built_picture = False

    # create mandatory items
    base_dir = os.path.expanduser("~/Documents/Pibooth/")
    create_mandatory_items(base_dir)

    # init diaporama last time image
    if "diaporama" in cfg.wolfstates:

        # set diaporama size
        if cfg.get("WINDOW", "size") == "fullscreen":
            app.diaporama_size = cfg.gettuple("DIFFUSION_KIOSK", "diaporama_size", int)
        else:
            app.diaporama_size = app._window._size
        app.diffusion_path = os.path.expanduser(
            "~/Documents/Pibooth/data/diffusion/images"
        )
        # get last modified time of photos of photos in diffusion folder
        if not os.path.isdir(app.diffusion_path):
            LOGGER.error("WOLFOTO : No diffusion folder found")
            # create diffusion folder
            os.makedirs(app.diffusion_path)
        app.diapo_images_path = [
            img for img in pathlib.Path(app.diffusion_path).iterdir()
        ]
        app.diapo_images_dict = {}
        win_w, win_h = app.diaporama_size
        if app.diapo_images_path:
            for img_path in app.diapo_images_path:
                img = open_image(img_path)
                if img:
                    # scale img
                    ratio = img.width / win_w
                    height = img.height / ratio
                    if height > win_h:
                        height = win_h
                        ratio = img.height / win_h
                        width = img.width / ratio
                    else:
                        width = win_w
                    img = img.resize((round(width), round(height)))
                    app.diapo_images_dict[img_path] = img
            # sort keys
            app.diapo_images_keys = sorted(
                list(app.diapo_images_dict.keys()),
                key=os.path.getmtime,
                reverse=True,
            )
        else:
            LOGGER.info("WOLFOTO : No images found in diffusion folder")
            app.diaporama_last_time = 0
            app.diapo_images_keys = []

        app.diaporama_length = len(app.diapo_images_dict)
        # load background image
        if os.path.isfile(
            os.path.expanduser("~/Documents/Pibooth/assets/background.png")
        ):
            img = open_image(
                os.path.expanduser("~/Documents/Pibooth/assets/background.png")
            )
        else:
            LOGGER.info("WOLFOTO : diaporama, load default background image")
            img = open_image(
                os.path.join(os.path.dirname(__file__), "assets", "background.png")
            )

        if img:
            # scale img
            ratio = img.width / win_w
            height = img.height / ratio
            if height > win_h:
                height = win_h
                ratio = img.height / win_h
                width = img.width / ratio
            else:
                width = win_w
            app.diapo_background_image = img.resize((int(width), int(height)))
        else:
            LOGGER.error("WOLFOTO : No background image found")
            app.diapo_background_image = None


@pibooth.hookimpl(optionalhook=True)
def state_wait_enter(cfg, app, win):
    """Work around to skip wait state"""

    # customize scene
    win.scene.left_arrow.hide()
    win.scene.right_arrow.hide()
    win.scene.text.hide()
    win.scene.text_print.hide()

    app.timer = PollingTimer()
    app.timer.start(0)


@pibooth.hookimpl(optionalhook=True)
def state_choose_enter(app, win):

    # customize 'choose' scene
    win.scene.left_arrow.set_skin(os.path.join(APP_DIR, "assets/choose.png"))
    win.scene.right_arrow.set_skin(os.path.join(APP_DIR, "assets/other_format.png"))
    win.scene.right_arrow.set_flip(False)
    win.scene.left_arrow.hide()
    win.scene.right_arrow.hide()

    # choose button
    choose_background = ImageSprite(None, (100, 100, 100), size=(100, 100), layer=2)
    size = (win.scene.rect.height * 0.25, win.scene.rect.height * 0.09)
    x = win.scene.rect.left + win.scene.rect.width // 4 - size[0] // 2
    y = win.scene.rect.bottom - size[1] - 10
    choose_background.set_rect(x, y, size[0], size[1])
    win.scene.add_sprite(choose_background)
    choose = TextSprite(None, "Choisir", layer=2)
    choose.set_color((0, 0, 0))
    size = (choose_background.rect.size[0] * 0.8, choose_background.rect.size[1] * 0.8)
    choose.rect.size = (size[0], size[1])
    choose.rect.centerx = choose_background.rect.centerx
    choose.rect.centery = choose_background.rect.centery
    choose.on_pressed = lambda: evts.post(evts.EVT_PIBOOTH_CAPTURE)
    win.scene.add_sprite(choose)

    # other format button
    format_background = ImageSprite(None, (100, 100, 100), size=(100, 100), layer=2)
    size = (win.scene.rect.height * 0.35, win.scene.rect.height * 0.09)
    x = win.scene.rect.right - win.scene.rect.width // 4 - size[0] // 2
    y = win.scene.rect.bottom - size[1] - 10
    format_background.set_rect(x, y, size[0], size[1])
    win.scene.add_sprite(format_background)
    format_t = TextSprite(None, "Autre format", layer=2)
    format_t.set_color((0, 0, 0))
    size = (format_background.rect.size[0] * 0.8, format_background.rect.size[1] * 1)
    format_t.rect.size = (size[0], size[1])
    format_t.rect.centerx = format_background.rect.centerx
    format_t.rect.centery = format_background.rect.centery
    format_t.on_pressed = lambda: evts.post(evts.EVT_PIBOOTH_PRINT)
    win.scene.add_sprite(format_t)


@pibooth.hookimpl(optionalhook=True)
def state_preview_enter(app, win):

    # customize 'preview' scene
    win.scene.left_image.hide()


@pibooth.hookimpl(optionalhook=True)
def state_finish_enter(app, win):
    """Customize 'finish' scene"""

    win.scene.status_bar.hide()
    win.scene.left_image.hide()
    win.scene.right_image.hide()
    win.scene.text.hide()


@pibooth.hookimpl(optionalhook=True)
def state_wait_validate(app, cfg, events):
    """Work around to skip wait state"""

    if app.timer.is_timeout():
        app.timer.reset()
        if "cameramonitor" in cfg.wolfstates:
            LOGGER.info("Skip 'wait' state and go to cameramonitor state")
            return "cameramonitor"
        elif "diaporama" in cfg.wolfstates:
            LOGGER.info("Skip 'wait' state and go to diaporama state")
            return "diaporama"
        else:
            raise RuntimeError(
                "Error : 'cameramonitor' or 'diaporama' plugin must be used with wolfoto plugins"
            )


@pibooth.hookimpl(optionalhook=True)
def state_welcome_validate(app, cfg, events):
    """Go to 'cameramonitor' when
    EVT_PIBOOTH_NEXT event received
    """

    if evts.find_event(events, EVT_PIBOOTH_NEXT):
        LOGGER.info("WOLFOTO : Go to cameramonitor")
        return "cameramonitor"


@pibooth.hookimpl(optionalhook=True)
def state_cameramonitor_validate(app, cfg, events):
    """Go to 'capture' when
    EVT_PIBOOTH_NEXT_CAPTURE event received
    """

    if len(app.capture_choices) > 1:
        if evts.find_event(events, EVT_PIBOOTH_NEXT):
            return "choose"
    else:
        if evts.find_event(events, EVT_PIBOOTH_NEXT_CAPTURE):
            if app.capture_nbr == 1:
                return "capture"
            else:
                # display message that more than one photo will be captured
                return "chosen"

    if "welcome" in cfg.wolfstates:
        # 'welcome' plugin used, priority over 'diaporama' plugin
        # if no action, go to 'welcome' state
        if app.timer_inaction.is_started() and app.timer_inaction.is_timeout():
            LOGGER.info("WOLFOTO : Go to welcome screen")
            return "welcome"
    elif "diaporama" in cfg.wolfstates:
        # 'diaporama' plugin used
        # if no action, go to 'diaporama' state
        if app.timer_inaction.is_started() and app.timer_inaction.is_timeout():
            LOGGER.info("WOLFOTO : Go to diaporama screen")
            return "diaporama"


@pibooth.hookimpl(hookwrapper=True)
def state_capture_validate(cfg):
    """Intercept processing state after capture state"""
    outcome = yield
    next_state = outcome.get_result()
    if not cfg.wolf_built_picture:
        if next_state == "processing":
            if "chooseoptions" in cfg.wolfstates:
                LOGGER.info("WOLFOTO : Overriding processing with chooseoptions")
                outcome.force_result("chooseoptions")


@pibooth.hookimpl(hookwrapper=True)
def state_processing_validate(cfg):
    """Intercept processing state after capture state"""

    outcome = yield
    next_state = outcome.get_result()
    if next_state == "print":
        if cfg.wolf_built_picture:
            if "chooseoptions" in cfg.wolfstates:
                LOGGER.info("WOLFOTO : Go to chooseoptions state")
                outcome.force_result("chooseoptions")
            elif "wolfprint" in cfg.wolfstates:
                LOGGER.info("WOLFOTO : Go to wolfprint state")
                outcome.force_result("wolfprint")
            else:
                LOGGER.info(
                    "WOLFOTO : Go to finish state, [WARNING] No select options and printing states configured !"
                )
                outcome.force_result("finish")
        else:
            if "wolfprint" in cfg.wolfstates:
                LOGGER.info("WOLFOTO : Go to wolfprint state")
                outcome.force_result("wolfprint")
            else:
                LOGGER.info("WOLFOTO : Go to finish state")
                outcome.force_result("finish")


@pibooth.hookimpl(optionalhook=True)
def state_chooseoptions_validate(cfg, app, win, events):
    """[select-effect]"""

    if evts.find_event(events, EVT_PIBOOTH_NEXT):
        if cfg.publish_web == 1:
            LOGGER.info("WOLFOTO : Go to qrcode state")
            return "qrcode"
        else:
            if cfg.wolf_built_picture:
                if "wolfprint" in cfg.wolfstates:
                    LOGGER.info("WOLFOTO : Go to wolfprint state")
                    return "wolfprint"
                else:
                    LOGGER.info("WOLFOTO : Go to 'finish' state")
                    return "finish"
            else:
                LOGGER.info("WOLFOTO : Go to finish state")
                return "finish"
    elif evts.find_event(events, EVT_PIBOOTH_CANCEL):
        cfg.cancelled = True
        LOGGER.info("WOLFOTO : Go to 'finish' state")
        return "finish"

    if app.readystate_timer.is_timeout():
        cfg.cancelled = True
        LOGGER.info("WOLFOTO : Back to 'finish' state")
        return "finish"


@pibooth.hookimpl(optionalhook=True)
def state_qrcode_validate(cfg, app, win, events):
    """[qrcode]"""

    if evts.find_event(events, EVT_PIBOOTH_NEXT) or app.readystate_timer.is_timeout():
        if cfg.wolf_built_picture:
            if "wolfprint" in cfg.wolfstates:
                LOGGER.info("WOLFOTO : Go to wolfprint state")
                return "wolfprint"
            else:
                LOGGER.info("WOLFOTO : Go to 'finish' state")
                return "finish"
        else:
            if "wolfprint" in cfg.wolfstates:
                LOGGER.info("WOLFOTO : Go to processing state")
                return "processing"
            else:
                LOGGER.info("WOLFOTO : Go to 'finish' state")
                return "finish"


@pibooth.hookimpl(optionalhook=True)
def state_wolfprint_validate(app, cfg, events):
    """Go to 'finish'
    [wolfprint]
    """

    if evts.find_event(events, EVT_PIBOOTH_NEXT):
        if cfg.wolf_print:
            LOGGER.info("WOLFOTO : Print photo")
            print_picture(cfg, app)
        LOGGER.info("WOLFOTO : Back to 'finish' state")
        return "finish"

    if app.readystate_timer.is_timeout():
        LOGGER.info("WOLFOTO : Back to 'finish' state")
        return "finish"


@pibooth.hookimpl(optionalhook=True)
def state_diaporama_validate(app, cfg, events):
    """Go to 'cameramonitor'
    [diaporama]
    """

    if "cameramonitor" in cfg.wolfstates and evts.find_event(events, EVT_PIBOOTH_NEXT):
        LOGGER.info("Go to cameramonitor")
        return "cameramonitor"
