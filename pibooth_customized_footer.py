"""Replace footer text by customized one and date
"""

import datetime

import pibooth


__version__ = "1.0.0"


@pibooth.hookimpl
def pibooth_configure(cfg):
    cfg.add_option('PICTURE', 'wolf_footer_text', 'Wolfoto',
                   "Customized footer text displayed")


@pibooth.hookimpl
def pibooth_setup_picture_factory(cfg, factory):
    """Add customized text to built picture,
    text is 'wolf_footer_text' config file parameter and now date
    """

    try:
        text = cfg.get('PICTURE', 'wolf_footer_text').strip('"').strip("'")
    except KeyError:
        text = ''

    # reset pibooth footer text
    cfg.set('PICTURE', 'footer_text1', '')
    cfg.set('PICTURE', 'footer_text2', '')

    color = cfg.gettuple('PICTURE', 'text_colors', 'color')
    text_font = cfg.gettuple('PICTURE', 'text_fonts', str)
    text_date = datetime.datetime.now()
    factory.add_text(text, text_font[0], color[0], 'center')
    factory.add_text(
        text_date.strftime('le %A %d %B %Y à %Hh%M'),
        text_font[0],
        color[0],
        'right'
        )
