#!/bin/bash

##########################################
# WOLFOTO Kiosk capture pibooth watchdog #
##########################################
# 08/11/2023

# Test if pibooth is running
# if not : relaunch pibooth
# after 3 attempts, reboot
# aftre 2 reboots, do nothing
# all actions are logged to /var/log/syslog

# edit crontab (crontab -e)
# to launch watchdog every 5 mn :
# add : */5 * * * * /path/to/pibooth_watchdog.sh

# TODO: use logger ?

nb_launch=0  # number of relaunch of pibooth
max_launch=3  # maximum number of relaunch of pibooth to do
pibooth_state=0  # == 0 if pibooth is down
log_tag=[PIBOOTH]  # tag for logger
log_file=/tmp/pibooth_watchdog.log
nb_reboot_file=~/Documents/Pibooth/data/nb_reboot.txt  # file to store number of reboot
max_reboot=2  # maximum number of reboot to do
format_date="+%F %H:%M:%S"
process="/usr/bin/python3 /usr/local/bin/pibooth"

# logger -t "$log_tag" "pibooth watchdog"

export DISPLAY=:0

while [[ $pibooth_state -eq 0 ]]; do
    pgrep -f "$process"
    if [[ $? -ne 0 ]]; then
        if [[ $nb_launch -lt $max_launch ]]; then
            $process &
            ((nb_launch++))
            echo "$(date "$format_date") $log_tag " "relaunch pibooth : $nb_launch attempt(s)" | tee -a $log_file
            # logger -t $log_tag "relaunch pibooth : $nb_launch attempt"
            sleep 20  # wait 20" before next pibooth process test
        else
            # read number of reboot already done
            if [[ -f "$nb_reboot_file" ]]; then
                read nb_reboot < "$nb_reboot_file"
                if [[ $nb_reboot -lt $max_reboot ]]; then
                    echo "$(date "$format_date") $log_tag " "already rebooted $nb_reboot time(s) -> reboot" | tee -a $log_file
                    # logger -t $log_tag "already rebooted $nb_reboot time(s) -> reboot"
                    ((nb_reboot++))
                    echo $nb_reboot > "$nb_reboot_file"
                    sudo reboot
                    # DEBUG
                    # echo "$(date "$format_date") $log_tag " "launch reboot" | tee -a $log_file
                    # pibooth_state=1
                    # exit 0
                    # END DEBUG
                else
                    echo "$(date "$format_date") $log_tag " "already rebooted $nb_reboot time(s) -> ERROR, do nothing" | tee -a $log_file
                    # logger -t $log_tag "already rebooted $nb_reboot time(s) -> ERROR, do nothing"
                    pibooth_state=1
                fi
            else
                # no file to store number of reboot, first reboot to do
                echo 1 > "$nb_reboot_file"
                sudo reboot
                # DEBUG
                # echo "$(date "$format_date") $log_tag " "launch reboot" | tee -a $log_file
                # pibooth_state=1
                # exit 0
                # END DEBUG
            fi
        fi
    else
        # reset number of reboot
        echo 0 > "$nb_reboot_file"
        pibooth_state=1
    fi
done
echo "$(date "$format_date") $log_tag " "exit watchdog" | tee -a $log_file
