"""Set an effect to be used on all captures of a pictures"""

import os
import pygame
from PIL import Image
import yaml
import io
import datetime
import glob

import pibooth
from pibooth import evts
from pibooth import fonts
from pibooth.utils import PollingTimer
from pibooth.view.pygame.sprites import BasePygameScene, TextSprite, ImageSprite
from pibooth.utils import LOGGER

import server
import wolf_utils
from widgets import BorderSprite, ButtonSprite

EVT_PIBOOTH_NEXT = pygame.USEREVENT + 300
EVT_PIBOOTH_COLOR = pygame.USEREVENT + 301
EVT_PIBOOTH_B_W = pygame.USEREVENT + 302
EVT_PIBOOTH_PUBLISH_LOCAL = pygame.USEREVENT + 303
EVT_PIBOOTH_PUBLISH_WEB = pygame.USEREVENT + 304
EVT_PIBOOTH_CANCEL = pygame.USEREVENT + 306
EVT_PIBOOTH_SEND = pygame.USEREVENT + 307


__version__ = "1.2.0"


class SelectEffectScene(BasePygameScene):
    """Provide a scene to select between available effects and publish options
    before processing
    """

    def __init__(self):
        super().__init__()

        self.logo_place = ImageSprite(
            self,
            os.path.join(
                os.path.expanduser("~/Documents/Pibooth/"), "assets", "logo_place.png"
            ),
            colorize=False,
        )
        self.logo_photobooth = ImageSprite(
            self,
            os.path.join(os.path.dirname(__file__), "assets", "logo_photobooth.png"),
            colorize=True,
        )

        self.submit = ButtonSprite(
            self,
            "Envoyer",
            (100, 20),
            skin=os.path.join(
                os.path.join(os.path.dirname(__file__), "assets", "button_back.png")
            ),
        )
        self.cancel = ButtonSprite(
            self,
            "Annuler",
            (100, 20),
            skin=os.path.join(
                os.path.join(os.path.dirname(__file__), "assets", "button_back.png")
            ),
        )

        self.color_btn = ImageSprite(
            self,
            os.path.join(os.path.dirname(__file__), "assets", "color_btn.png"),
            colorize=False,
        )
        self.color_btn.set_rect(50, 400, 50, 50)
        self.b_w_btn = ImageSprite(
            self,
            os.path.join(os.path.dirname(__file__), "assets", "b_w_btn.png"),
            colorize=False,
        )
        self.b_w_btn.set_rect(150, 400, 50, 50)
        self.captured_image = ImageSprite(
            self,
            os.path.join(os.path.dirname(__file__), "assets", "wolfoto_logo_500.png"),
            colorize=False,
        )
        self.captured_image.set_rect(300, 10, 400, 400)

        self.options_menu_background = ImageSprite(
            self,
            os.path.join(
                os.path.dirname(__file__),
                "assets",
                "choose_options_menu_background.png",
            ),
            colorize=True,
            layer=1,
        )

        self.menu_title = TextSprite(self, text="Voulez-vous :", size=(300, 80))
        self.menu_title.align = fonts.ALIGN_CENTER_LEFT
        self.local_choice = TextSprite(
            self, text="vous afficher dans l'établissement ? ", size=(450, 50)
        )
        self.local_choice.align = fonts.ALIGN_CENTER_LEFT
        self.web_choice = TextSprite(
            self, text="récupérer la photo sur le web ? ", size=(450, 50)
        )
        self.web_choice.align = fonts.ALIGN_CENTER_LEFT
        self.local_check = ImageSprite(
            self,
            os.path.join(os.path.dirname(__file__), "assets", "check_yes.png"),
            size=(40, 40),
            colorize=False,
        )
        self.web_check = ImageSprite(
            self,
            os.path.join(os.path.dirname(__file__), "assets", "check_no.png"),
            size=(40, 40),
            colorize=False,
        )

        self.image_border = BorderSprite(self.captured_image)
        self.wolf_message = ImageSprite(
            self,
            os.path.join(
                os.path.dirname(__file__),
                "assets",
                "msg_envoi_photo_cercle.png",
            ),
            colorize=True,
            layer=9,
        )
        self.wolf_message.set_rect(100, 100, 300, 100)

        self.submit.set_on_pressed(lambda: evts.post(EVT_PIBOOTH_SEND))
        self.cancel.set_on_pressed(lambda: evts.post(EVT_PIBOOTH_CANCEL))
        self.color_btn.on_pressed = lambda: evts.post(EVT_PIBOOTH_COLOR)
        self.b_w_btn.on_pressed = lambda: evts.post(EVT_PIBOOTH_B_W)
        self.local_check.on_pressed = lambda: evts.post(EVT_PIBOOTH_PUBLISH_LOCAL)
        self.web_check.on_pressed = lambda: evts.post(EVT_PIBOOTH_PUBLISH_WEB)

    def resize(self, size):
        """Resize text when window is resized."""

        # captured image
        self.captured_image.set_rect(
            self.rect.width * 0.02,
            self.rect.width * 0.05,
            self.rect.width * 0.6,
            self.rect.height * 0.7,
        )
        self.image_border.set_geometry()

        # options below captured_image
        color_btn_size = (self.rect.width * 0.054, self.rect.width * 0.054)
        color_btn_pos = (
            self.captured_image.rect.centerx
            - self.captured_image.rect.x
            - 1.5 * color_btn_size[0],
            self.captured_image.rect.bottom + self.rect.height * 0.05,
        )
        self.color_btn.set_rect(*color_btn_pos, *color_btn_size)
        b_w_btn_size = color_btn_size
        b_w_btn_pos = (
            self.captured_image.rect.centerx
            - self.captured_image.rect.x
            + 0.5 * color_btn_size[0],
            self.color_btn.rect.top,
        )
        self.b_w_btn.set_rect(*b_w_btn_pos, *b_w_btn_size)

        # Logos
        logo_place_size = (self.rect.height * 2 // 7, self.rect.height // 7)
        logo_place_pos = (self.rect.width - logo_place_size[0], 10)
        self.logo_place.set_rect(*logo_place_pos, *logo_place_size)
        logo_photobooth_size = (self.rect.width // 9, self.rect.width // 9)
        logo_photobooth_pos = (
            self.rect.width - logo_photobooth_size[0],
            self.rect.height - logo_photobooth_size[1],
        )
        self.logo_photobooth.set_rect(*logo_photobooth_pos, *logo_photobooth_size)

        # menu
        menu_pos_topleft = (
            self.captured_image.rect.right + self.rect.width * 0.01,
            self.rect.height * 0.35,
        )

        # publish menu
        text_height = self.rect.height // 25
        ## menu title
        menu_title_pos = (
            menu_pos_topleft[0] + self.rect.width * 0.02,
            menu_pos_topleft[1] + self.rect.height * 0.04,
        )
        menu_title_size = (self.rect.width * 0.51, max(12, text_height * 1.1))
        self.menu_title.set_rect(*menu_title_pos, *menu_title_size)
        ## local choice
        local_check_pos = (
            menu_title_pos[0],
            self.menu_title.rect.bottom + self.rect.height * 0.03,
        )
        local_check_size = (max(12, text_height * 0.9), max(12, text_height * 0.9))
        self.local_check.set_rect(*local_check_pos, *local_check_size)
        local_choice_size = (
            self.rect.width * 0.40,
            max(10, text_height),
        )
        # self.local_choice.set_rect(10, 10, *local_choice_size)
        self.local_choice.set_rect(10, 10, *local_choice_size)
        self.local_choice.rect.centery = self.local_check.rect.centery - 2
        self.local_choice.rect.left = (
            self.local_check.rect.right + self.rect.width * 0.01
        )

        ## web choice
        web_check_pos = (
            menu_title_pos[0],
            self.local_check.rect.bottom + self.rect.height * 0.02,
        )
        web_check_size = local_check_size
        self.web_check.set_rect(*web_check_pos, *web_check_size)
        self.web_choice.set_rect(10, 10, *local_choice_size)
        self.web_choice.rect.centery = self.web_check.rect.centery - 2
        self.web_choice.rect.left = self.web_check.rect.right + self.rect.width * 0.01

        # submit
        padding = (
            self.rect.height // 150,
            self.rect.width // 100,
            self.rect.height // 150,
            self.rect.width // 100,
        )
        submit_size = (self.rect.width * 0.11, text_height * 1.5)
        submit_pos = (
            menu_title_pos[0],
            self.web_check.rect.bottom + self.rect.height * 0.06,
        )
        self.submit.set_rect(*submit_pos, *submit_size)
        self.submit.set_padding(padding)

        # cancel
        cancel_size = (self.rect.width * 0.11, text_height * 1.5)
        cancel_pos = (
            self.submit.get_rect().right + self.rect.width * 0.01,
            self.submit.get_rect().top,
        )
        self.cancel.set_rect(*cancel_pos, *cancel_size)
        self.cancel.set_padding(padding)

        # options menu back
        options_menu_background_size = (
            self.rect.width * 0.37,
            self.rect.width * 0.37 // 1.6,
        )
        # 1.6 is the ratio width/height of options menu back image
        self.options_menu_background.set_rect(
            menu_pos_topleft[0], menu_pos_topleft[1], *options_menu_background_size
        )

        # wolf_message
        wolf_message_size = (
            self.captured_image.rect.height * 0.8,
            self.captured_image.rect.height * 0.8,
        )
        self.wolf_message.set_rect(10, 10, *wolf_message_size)
        self.wolf_message.rect.centerx = self.captured_image.rect.centerx
        self.wolf_message.rect.centery = self.captured_image.rect.centery


@pibooth.hookimpl
def pibooth_setup_states(machine):
    """Add a pre-processing state"""
    machine.add_state("chooseoptions", SelectEffectScene())


@pibooth.hookimpl
def pibooth_configure(cfg):
    """Declare the new configuration options"""

    # capture kiosk parameter
    cfg.add_option(
        "CAPTURE_KIOSK",
        "diffusion_text_1",
        "Voulez-vous :",
        "1st part of text to display for diffusion choice",
    )
    cfg.add_option(
        "CAPTURE_KIOSK",
        "diffusion_text_local",
        "vous afficher dans ce lieu ?",
        "text to display for local diffusion choice",
    )
    cfg.add_option(
        "CAPTURE_KIOSK",
        "diffusion_text_web",
        "récupérer la photo sur le web ?",
        "text to display for web diffusion choice",
    )
    cfg.add_option(
        "CAPTURE_KIOSK",
        "local_check",
        "1",
        "default check to select the local diffusion option (int, 1 or 0)",
    )
    cfg.add_option(
        "CAPTURE_KIOSK",
        "web_check",
        "0",
        "default check to select the local diffusion option (int, 1 or 0)",
    )
    cfg.add_option(
        "WINDOW",
        "menu_options_text_color",
        (51, 51, 51),
        "color of menu options text",
    )
    cfg.add_option(
        "WINDOW",
        "menu_options_background_color",
        (204, 204, 204),
        "color of menu options background",
    )


@pibooth.hookimpl(optionalhook=True)
def state_chooseoptions_enter(cfg, app, win):
    """Start a timer during which text is displayed.
    Update image to display.
    """

    # set color of photobooth logo
    logo_photobooth_color = cfg.gettuple("WINDOW", "logo_photobooth_color", int)
    win.scene.logo_photobooth.set_color(logo_photobooth_color)

    # set color of buttons
    button_background_color = cfg.gettuple("WINDOW", "button_background_color", int)
    win.scene.submit.set_background_color(button_background_color)
    win.scene.cancel.set_background_color(button_background_color)

    # set color text of options menu
    win.scene.menu_title.set_color(
        cfg.gettuple("WINDOW", "menu_options_text_color", int)
    )
    win.scene.local_choice.set_color(
        cfg.gettuple("WINDOW", "menu_options_text_color", int)
    )
    win.scene.web_choice.set_color(
        cfg.gettuple("WINDOW", "menu_options_text_color", int)
    )

    # set color options menu background
    win.scene.options_menu_background.set_color(
        cfg.gettuple("WINDOW", "menu_options_background_color", int)
    )

    # set color of message
    win.scene.wolf_message.set_color(cfg.gettuple("WINDOW", "photo_button_color", int))

    cfg.cancelled = False  # flag to process cancel button
    # load captured image
    if not cfg.wolf_built_picture:
        img = app.camera._process_capture(
            app.camera._captures[0]
        )  # convert app.camera._captures to PIL image
        win.scene.captured_image.set_skin(img)
        win.scene.captured_image.image_color = win.scene.captured_image.get_skin()
        win.scene.captured_image.image_b_w = (
            win.scene.captured_image.get_skin().convert("L")
        )
    else:
        # hide fx options
        win.scene.b_w_btn.hide()
        win.scene.color_btn.hide()
        filename_path = os.path.join(
            cfg.gettuple("GENERAL", "directory", "path")[0], app.picture_filename
        )
        win.scene.captured_image.set_skin(filename_path)

    # set border for captured image
    win.scene.image_border.set_geometry()
    # set color of captured image border
    image_border_color = cfg.gettuple("WINDOW", "image_border_color", int)
    win.scene.image_border.set_color(image_border_color)

    # user's choices are recorded in cfg parameters
    cfg.fx = "color"
    cfg.publish_local = cfg.getint("CAPTURE_KIOSK", "local_check")
    if cfg.publish_local:
        win.scene.local_check.set_skin(
            os.path.join(os.path.dirname(__file__), "assets", "check_yes.png")
        )
    else:
        win.scene.local_check.set_skin(
            os.path.join(os.path.dirname(__file__), "assets", "check_no.png")
        )
    cfg.publish_web = cfg.getint("CAPTURE_KIOSK", "web_check")
    if cfg.publish_web:
        win.scene.web_check.set_skin(
            os.path.join(os.path.dirname(__file__), "assets", "check_yes.png")
        )
    else:
        win.scene.web_check.set_skin(
            os.path.join(os.path.dirname(__file__), "assets", "check_no.png")
        )

    # update diffusion choice text
    win.scene.menu_title.text = cfg.get("CAPTURE_KIOSK", "diffusion_text_1")
    if win.scene.menu_title.text == "":
        win.scene.menu_title.hide()
    win.scene.local_choice.text = cfg.get("CAPTURE_KIOSK", "diffusion_text_local")
    if win.scene.local_choice.text == "":
        # if "diffusion_text_local" is empty, hide option
        win.scene.local_choice.hide()
        win.scene.local_check.hide()
    win.scene.web_choice.text = cfg.get("CAPTURE_KIOSK", "diffusion_text_web")
    if win.scene.web_choice.text == "":
        # if "diffusion_text_web" is empty, hide option
        win.scene.web_choice.hide()
        win.scene.web_check.hide()

    # hide message
    win.scene.wolf_message.hide()

    # start timer to come back to camera monitoring if no validation
    app.readystate_timer = PollingTimer(30)
    # timer to come back to camera monitoring if no validation
    app.readystate_timer = PollingTimer(30)


@pibooth.hookimpl(optionalhook=True)
def state_chooseoptions_do(cfg, app, win, events):

    if evts.find_event(events, EVT_PIBOOTH_B_W):
        win.scene.captured_image.set_skin(
            win.scene.captured_image.image_b_w.convert("RGB")
        )
        cfg.fx = "b_w"
    elif evts.find_event(events, EVT_PIBOOTH_COLOR):
        win.scene.captured_image.set_skin(win.scene.captured_image.image_color)
        cfg.fx = "color"
    elif evts.find_event(events, EVT_PIBOOTH_PUBLISH_LOCAL):
        if cfg.publish_local == 1:
            win.scene.local_check.set_skin(
                os.path.join(os.path.dirname(__file__), "assets", "check_no.png")
            )
            cfg.publish_local = 0
        else:
            win.scene.local_check.set_skin(
                os.path.join(os.path.dirname(__file__), "assets", "check_yes.png")
            )
            cfg.publish_local = 1
    elif evts.find_event(events, EVT_PIBOOTH_PUBLISH_WEB):
        if cfg.publish_web == 1:
            win.scene.web_check.set_skin(
                os.path.join(os.path.dirname(__file__), "assets", "check_no.png")
            )
            cfg.publish_web = 0
        else:
            win.scene.web_check.set_skin(
                os.path.join(os.path.dirname(__file__), "assets", "check_yes.png")
            )
            cfg.publish_web = 1
    elif evts.find_event(events, EVT_PIBOOTH_SEND):
        # show message (sending photo)
        win.scene.wolf_message.show()
        # send events to go next state
        evts.post(EVT_PIBOOTH_NEXT)


@pibooth.hookimpl(optionalhook=True)
def state_chooseoptions_exit(cfg, app, win):
    """do process based on user choice"""

    if not cfg.cancelled:
        # do nothing if cancel button is pressed
        web_server_url = cfg.get("SERVER", "web_server_url")
        server_username = cfg.get("SERVER", "server_username")
        server_password = cfg.get("SERVER", "server_password")
        shortcode_len = int(cfg.get("SERVER", "shortcode_len"))
        gallery = cfg.get("SERVER", "gallery")
        place = cfg.get("SERVER", "place")

        data_dir = os.path.expanduser(
            "~/Documents/Pibooth/data/"
        )  # folder where to write data used in the app

        if cfg.publish_local == 1 or cfg.publish_web == 1:

            LOGGER.info(f"WOLFOTO : date is <{datetime.datetime.now().strftime('%c')}>")
            LOGGER.info(
                f"WOLFOTO : publish photo: local[{cfg.publish_local}], web[{cfg.publish_web}]"
            )

            if cfg.wolf_built_picture:
                img_built_path = os.path.join(
                    cfg.gettuple("GENERAL", "directory", "path")[0],
                    app.picture_filename,
                )
                img = Image.open(img_built_path)
            else:
                if cfg.fx == "b_w":
                    img = win.scene.captured_image.image_b_w
                else:
                    img = win.scene.captured_image.image_color = (
                        win.scene.captured_image.get_skin()
                    )
                # set watermark if exists when image is just one photo (not built)
                try:
                    watermark = Image.open(
                        os.path.join(
                            os.path.expanduser("~/Documents/Pibooth/"),
                            "assets",
                            "watermark.png",
                        )
                    )
                except FileNotFoundError:
                    LOGGER.error(
                        "WOLFOTO : Watermark not found, watermark will not be added."
                    )
                    watermark = None
                else:
                    position = (
                        img.width - watermark.width - 20,
                        img.height - watermark.height - 20,
                    )
                    img.paste(watermark, position, watermark.convert("RGBA"))

            # generate shortcode and filename
            ## 'shortcodes.yaml' is the file storing already used shortcodes
            shortcode_yaml_path = os.path.join(data_dir, "shortcodes.yaml")
            if not os.path.isfile(shortcode_yaml_path):
                LOGGER.debug(f"WOLFOTO : Creating {shortcode_yaml_path}")
                open(shortcode_yaml_path, "w").close()

            ## 'filenames.yaml' is the file storing already used filenames
            filename_yaml_path = os.path.join(data_dir, "filenames.yaml")
            if not os.path.isfile(filename_yaml_path):
                LOGGER.debug(f"WOLFOTO : Creating {filename_yaml_path}")
                open(filename_yaml_path, "w").close()

            ### get shortcodes and filenames lists from server
            LOGGER.debug(
                f"WOLFOTO : Retrieving shortcodes and filenames list from server {web_server_url}"
            )
            shortcodes_on_server, filenames_on_server = (
                server.get_photos_data_from_server(
                    data_dir, web_server_url, server_username, server_password
                )
            )

            ## update 'shortcodes.yaml' and 'filenames.yaml'
            ### update local shortcode file
            if shortcodes_on_server is not None:
                with open(shortcode_yaml_path, "w") as f:
                    LOGGER.debug(
                        f"WOLFOTO : Writing shortcodes from server to {shortcode_yaml_path}"
                    )
                    yaml.dump(shortcodes_on_server, f)
            ### update local filenames file
            if filenames_on_server is not None:
                with open(filename_yaml_path, "w") as f:
                    LOGGER.debug(
                        f"WOLFOTO : Writing filenames from server to {filename_yaml_path}"
                    )
                    yaml.dump(filenames_on_server, f)

            ### load used shortcodes stored in local after update
            with open(shortcode_yaml_path, "r") as f:
                shortcodes = yaml.safe_load(f)
            if shortcodes is None:
                shortcodes = []
            ### generate shortcode
            LOGGER.debug(f"WOLFOTO : Generating shortcode based on {shortcodes}")
            shortcode = wolf_utils.generate_shortcode(shortcode_len, shortcodes)
            ## update used shortcodes file
            shortcodes.append(shortcode)
            with open(shortcode_yaml_path, "w") as f:
                LOGGER.debug(f"WOLFOTO : Saving shortcodes to {shortcode_yaml_path}")
                yaml.dump(shortcodes, f)

            ### load used filenames stored in local after update
            with open(filename_yaml_path, "r") as f:
                filenames = yaml.safe_load(f)
            if filenames is None:
                filenames = []
            ### generate filename
            LOGGER.debug(f"WOLFOTO : Generating filename based on {filenames}")
            filename = wolf_utils.generate_filename(filenames)
            ## update used filenames file
            filenames.append(filename)
            with open(filename_yaml_path, "w") as f:
                LOGGER.debug(f"WOLFOTO : Saving filenames to {filename_yaml_path}")
                yaml.dump(filenames, f)

            # create dictionary for data to send to server
            data_to_send = {}
            # filename = str(uuid.uuid4()) + ".jpg"
            capture_datetime = datetime.datetime.strptime(
                app.capture_date, "%Y-%m-%d-%H-%M-%S"
            )
            # update data dictionary
            buf = io.BytesIO()
            img.save(buf, format="JPEG")
            byte_img = buf.getvalue()
            data_to_send["code"] = shortcode
            data_to_send["filename"] = filename
            data_to_send["datetime"] = capture_datetime.isoformat()
            data_to_send["gallery"] = gallery
            data_to_send["place"] = place
            if cfg.wolf_built_picture:
                data_to_send["local"] = "0"
            else:
                data_to_send["local"] = str(cfg.publish_local)
            data_to_send["web"] = str(cfg.publish_web)
            data_to_send["public"] = "0"

            # send data to server
            LOGGER.debug("WOLFOTO : Publishing photo to Wolfoto Web")
            post_photo_success = server.publish_web(
                data_dir,
                web_server_url,
                server_username,
                server_password,
                data_to_send,
                byte_img,
            )

            if not cfg.wolf_built_picture:
                source = os.path.join(data_dir, "web", filename)
                if post_photo_success is False and cfg.publish_local == 1:
                    # copy photo to diffusion kiosk folder
                    wolf_utils.cp_to_local_diffusion(source, cfg)

            else:
                if cfg.publish_local:
                    # send raw pictures to server
                    # get folder name
                    raw_folder = os.path.splitext(app.picture_filename)[0].split("_")[0]
                    raw_folder_path = os.path.join(
                        cfg.gettuple("GENERAL", "directory", "path")[0],
                        "raw",
                        raw_folder,
                    )
                    i = 0
                    # send each file in folder
                    for file in glob.glob(os.path.join(raw_folder_path, "*.jpg")):
                        img_path = os.path.join(raw_folder_path, file)
                        img = Image.open(img_path)
                        filename_raw = os.path.splitext(filename)[0] + f"_{i}.jpg"
                        buf = io.BytesIO()
                        img.save(buf, format="JPEG")
                        byte_img = buf.getvalue()
                        data_to_send["code"] = shortcode + f"_{i}"
                        data_to_send["filename"] = filename_raw
                        data_to_send["datetime"] = capture_datetime.isoformat()
                        data_to_send["gallery"] = gallery
                        data_to_send["place"] = place
                        data_to_send["local"] = "1"
                        data_to_send["web"] = str(cfg.publish_web)
                        data_to_send["public"] = "0"

                        # send data to server
                        LOGGER.debug("WOLFOTO : Publishing photo to Wolfoto Web")
                        post_photo_success = server.publish_web(
                            data_dir,
                            web_server_url,
                            server_username,
                            server_password,
                            data_to_send,
                            byte_img,
                        )
                        i = i + 1

                        if post_photo_success is False:
                            # copy photo to diffusion kiosk folder using scp
                            source = os.path.join(data_dir, "web", filename_raw)
                            wolf_utils.cp_to_local_diffusion(source, cfg)

            if cfg.publish_web == 1:
                # store data to be used in 'qrcode' state
                app.shortcode = shortcode
                app.filename = filename
